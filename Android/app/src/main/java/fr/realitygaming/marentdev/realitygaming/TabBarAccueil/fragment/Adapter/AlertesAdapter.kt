package fr.realitygaming.marentdev.realitygaming.TabBarAccueil.fragment.Adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import fr.realitygaming.marentdev.realitygaming.Divers.getLinkAvatar
import fr.realitygaming.marentdev.realitygaming.R
import fr.realitygaming.marentdev.realitygaming.TabBarAccueil.fragment.Constructor.AlertesItem
import kotlinx.android.synthetic.main.view_cell_alertes.view.*
import java.net.URI
import java.net.URL

/**
 * Created by Marentdev on 12/03/2018.
 */
class AlertesAdapter: BaseAdapter {
    var context: Context? = null
    var alerteslist = ArrayList<AlertesItem>()

    constructor(context: Context, alertesListe: ArrayList<AlertesItem>) {
        this.context = context
        this.alerteslist = alertesListe
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val alerts = this.alerteslist[position]
        val inflator = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var al = inflator.inflate(R.layout.view_cell_alertes, null)
        al.date_alertes.text = alerts.time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            al.content_alertes.text = Html.fromHtml(alerts.content, Html.FROM_HTML_MODE_COMPACT)
        }else {
            al.content_alertes.text = Html.fromHtml(alerts.content)
        }
        try {
            al.avatar_alertes.setImageBitmap(alerts.img)
        }catch (e:Exception) {
            println("ERRRRORR")
        }
        return al
    }

    override fun getItem(position: Int): Any {
        return alerteslist[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
       return alerteslist.count()
    }
}