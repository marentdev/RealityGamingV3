package fr.realitygaming.marentdev.realitygaming.TabBarAccueil.fragment.Constructor

import android.graphics.Bitmap

/**
 * Created by Marentdev on 12/03/2018.
 */
class AlertesItem(content: String, id: String, time: String, id_author: String, lnk: String, author: String, img: Bitmap?) {
    var content: String? = content
    var id:String? = id
    var time:String? = time
    var id_author: String? = id_author
    var lnk: String? = lnk
    var author: String? = author
    var img: Bitmap? = img
}