package fr.realitygaming.marentdev.realitygaming.ConnexionController

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.beust.klaxon.*
import fr.realitygaming.marentdev.realitygaming.Divers.WebviewCookieHandler
import fr.realitygaming.marentdev.realitygaming.Divers.globalRG
import fr.realitygaming.marentdev.realitygaming.R
import fr.realitygaming.marentdev.realitygaming.TabBarAccueil.TabBarAccueil2Activity
import okhttp3.FormBody
import okhttp3.Request
import org.jsoup.Jsoup
import java.io.StringReader
import java.net.HttpCookie
import java.io.IOException


class LoginActivity : AppCompatActivity() {

    private var goto:Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()
        this.window.statusBarColor = Color.parseColor("#1E8CE0")
        val no_accountL = findViewById<TextView>(R.id.no_accountL)
        no_accountL.setOnClickListener {
            this.finish()
        }
        val have_account = findViewById<Button>(R.id.connection)
        val usernameT = findViewById<TextView>(R.id.pseudoT)
        val passwordT = findViewById<TextView>(R.id.passwordT)
        have_account.setOnClickListener {
            this.startRG(usernameT.text.toString(), passwordT.text.toString())
        }
        WebviewCookieHandler().removeCookies()
        goto = Intent(this, TabBarAccueil2Activity::class.java)
    }

    private fun startRG(username: String, password: String) {
        this.connectionRG(username, password) {
            if (it == 1) {//connecter
                android.widget.Toast.makeText(this, "Connecter", Toast.LENGTH_LONG).show()
                this.startActivity(goto)
            }else if (it == 2) {//two-step
                MaterialDialog.Builder(this).apply {
                    this.title("Quelle est votre methode ?")
                    this.positiveText("Valider")
                    this.negativeText("Cancel")
                    this.items("Email", "Google authentificator")
                    this.itemsCallbackSingleChoice(0, object : MaterialDialog.ListCallbackSingleChoice {
                        override fun onSelection(dialog: MaterialDialog, view: View, which: Int, text: CharSequence): Boolean {
                            println(which)
                            enterCode(if (which == 0) true else false)
                            return true
                        }
                    })

                }.show()
            }else if (it == -2) {//erreur connection
                android.widget.Toast.makeText(this, "Erreur -2", Toast.LENGTH_LONG).show()
            }else if (it == -3) {//erreur inconue
                android.widget.Toast.makeText(this, "Erreur -3", Toast.LENGTH_LONG).show()
            }
        }
    }
    private fun enterCode(is_email:Boolean) {
        MaterialDialog.Builder(this).apply {
            this.title("Quelle est votre code ?")
            this.positiveText("Valider")
            this.negativeText("Cancel")
            this.inputType(android.text.InputType.TYPE_CLASS_NUMBER)
            this.input("Code...", null, object: MaterialDialog.InputCallback {
                override fun onInput(dialog: MaterialDialog, input: CharSequence) {
                    twoStepRG(input.toString(), is_email, { res ->
                        when (res) {
                            1 -> startActivity(goto)
                            -1 -> println("Erreur serveur...")
                            -2 -> println("Erreur Two Step...")
                        }
                    })
                }
            })
        }.show()
    }

    private fun connectionRG(username: String, password: String, closure: (Int) -> Unit) {
        val body = FormBody.Builder().apply {
            this.add("login", username)
            this.add("password", password)
            this.add("remember", "1")
            this.add("_xfToken", "")
        }
        val request = Request.Builder().apply {
            this.url("https://realitygaming.fr/login/login")
            this.post(body.build())
        }
        globalRG.client.newCall(request.build()).execute().use({ response ->
            if (!response.isSuccessful) throw IOException("Unexpected code $response")

            val document = Jsoup.parse(response.body()?.string())
            if (document.getElementsByClass("p-title-value")[0].text() == "RealityGaming") {
                globalRG._xfToken = document.getElementsByAttributeValue("name", "_xfToken")[0].`val`()
                closure(1)
                return@use
            }else if (document.getElementsByClass("p-title-value")[0].text().contains("deux étapes")) {
                globalRG._xfToken = document.getElementsByAttributeValue("name", "_xfToken")[0].`val`()
                closure(2)
                return@use
            }else if (document.getElementsByClass("blockMessage--error").count() > 0) {
                closure(-2)
                return@use
            }else if (document.getElementsByClass("blockMessage").count() > 0) {
                closure(1)
                return@use
            }else{
                closure(-3)
                return@use
            }
        })
    }

    private fun twoStepRG(code:String, email:Boolean, closure: (Int) -> Unit) {
        val what = if (email) "email" else "totp"
        val body = FormBody.Builder().apply {
            this.add("code", code)
            this.add("trust", "1")
            this.add("provider", what)
            this.add("_xfToken", globalRG._xfToken)
            this.add("confirm", "1")
            this.add("save", "Confirmer")
            this.add("_xfResponseType", "json")
        }
        val request = Request.Builder().apply {
            this.url("https://realitygaming.fr/login/two-step")
            this.post(body.build())
        }
        globalRG.client.newCall(request.build()).execute().use({ response ->
            if (!response.isSuccessful) throw IOException("Unexpected code $response")
            val jsonstr = response.body()?.string()
            val json = Parser().parse(StringBuilder(jsonstr)) as JsonObject
            if (json.string("status") == "ok") {
                closure(1)
                return@use
            }else if (json.string("status") == "error") {
                closure(-1)
                return@use
            }else {
                closure(-2)
                return@use
            }
        })
    }
}
