package fr.realitygaming.marentdev.realitygaming.Divers

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.webkit.CookieManager
import okhttp3.OkHttpClient
import java.net.HttpCookie
import android.support.design.internal.BottomNavigationItemView
import java.lang.reflect.AccessibleObject.setAccessible
import java.lang.reflect.Array.setBoolean
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.util.Log


/**
 * Created by mvigne on 3/10/18.
 */
class globalRG {
    companion object {
        @JvmStatic var _xfToken:String = ""
        @JvmStatic var client: OkHttpClient = OkHttpClient().newBuilder().apply {
            this.cookieJar(WebviewCookieHandler())
        }.build()
    }
}
