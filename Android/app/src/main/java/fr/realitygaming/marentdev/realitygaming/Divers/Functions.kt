package fr.realitygaming.marentdev.realitygaming.Divers

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.lookup
import okhttp3.Request
import org.jsoup.Jsoup
import java.io.IOException

/**
 * Created by Marentdev on 13/03/2018.
 */

fun getProfile(pseudo:String? = null, id:String? = null) : Membre {
    val result = Membre()
    if (id == null && pseudo != null) {
        val id = searchId(pseudo)
        if (id.isNotEmpty()) {
            val request = Request.Builder().apply {
                this.url("https://realitygaming.fr/index.php/members/$id/?tooltip=true")
                this.get()
            }.build()
            globalRG.client.newCall(request).execute().use({ response ->
                if (!response.isSuccessful) throw IOException("Unexpected code $response")
                val document = Jsoup.parse(response.body()?.string())
                result.name = document.getElementsByClass("username")[0].text()
                result.messages = document.getElementsByAttributeValueMatching("class", "pairs pairs--rows pairs--rows--centered")[1].child(1).text()
                result.jaimes = document.getElementsByAttributeValueMatching("class", "pairs pairs--rows pairs--rows--centered")[3].child(1).text()
                result.points = document.getElementsByAttributeValueMatching("class", "pairs pairs--rows pairs--rows--centered")[4].child(1).text()
                result.id = id
                result.style = document.getElementsByAttributeValueMatching("class", "username--style")[0].attr("class").replace("username--style", "").replace("username--admin", "").replace("username--staff", "").replace("username--moderator", "").trim()
                return result
            })
        }
    }else if (pseudo == null && id != null) {

    }else {
        return Membre()
    }
    return Membre()
}

fun searchId(pseudo:String) : String {
    val request = Request.Builder().apply {
        this.url("https://realitygaming.fr/index.php/members/find?q=${java.net.URLEncoder.encode(pseudo, "UTF-8")}&_xfToken=${globalRG._xfToken}&_xfResponseType=json")
        this.get()
    }
    globalRG.client.newCall(request.build()).execute().use({ response ->
        if (!response.isSuccessful) throw IOException("Unexpected code $response")
        val jsonstr = response.body()?.string()
        val json = Parser().parse(StringBuilder(jsonstr)) as JsonObject
        if (json.lookup<String>("results")["iconHtml"].count() > 0) {
            val document = Jsoup.parse(json.lookup<String>("results")["iconHtml"][0].toString())
            if (json.lookup<String>("results")["id"][0].toString().toLowerCase() == pseudo.toLowerCase()) {
                return document.getElementsByClass("avatar")[0].attr("data-user-id")
            }
        }
    })
    return ""
}

fun getLinkAvatar(id:String) : String {
    if (id.isEmpty()) { return "" }
    if (id.count() <= 3) {
        return "https://realitygaming.fr/data/avatars/l/0/$id.jpg"
    }else {
        return "https://realitygaming.fr/data/avatars/l/${id.subSequence(0, id.count() - 3)}/$id.jpg"
    }
}