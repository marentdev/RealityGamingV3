package fr.realitygaming.marentdev.realitygaming.TabBarAccueil.fragment

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.GridView
import androidx.util.rangeTo
import androidx.view.doOnPreDraw
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.lookup
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import fr.realitygaming.marentdev.realitygaming.Divers.getLinkAvatar
import fr.realitygaming.marentdev.realitygaming.Divers.globalRG
import fr.realitygaming.marentdev.realitygaming.R
import fr.realitygaming.marentdev.realitygaming.TabBarAccueil.fragment.Adapter.AlertesAdapter
import fr.realitygaming.marentdev.realitygaming.TabBarAccueil.fragment.Constructor.AlertesItem
import kotlinx.android.synthetic.main.activity_tab_bar_accueil2.*
import okhttp3.FormBody
import okhttp3.Request
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.io.IOException


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AlertesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [AlertesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AlertesFragment : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null
    private var v:View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_alertes, container, false)
        v!!.doOnPreDraw {
            this.setupPage()
        }
        return v
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AlertesFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): AlertesFragment {
            val fragment = AlertesFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
    var alertes = ArrayList<AlertesItem>()
    private fun setupPage() {
        val request = Request.Builder().apply {
            this.url("https://realitygaming.fr/compte/alerts?page=1&_xfToken=${globalRG._xfToken}&_xfResponseType=json")
            this.get()
        }
        globalRG.client.newCall(request.build()).execute().use({ response ->
            if (!response.isSuccessful) throw IOException("Unexpected code $response")
            val jsonstr = response.body()?.string()
            val json = Parser().parse(StringBuilder(jsonstr)) as JsonObject
            if (json.string("status") == "ok") {
                //println(json.lookup<String>("html")["content"])
                if (json.lookup<String>("html")["content"].isNotEmpty()) {
                    val document = Jsoup.parse(json.lookup<String>("html")["content"][0] as String)
                    val block_alerts = document.getElementsByAttribute("data-alert-id")
                    block_alerts.forEach { elem ->
                        alertes.add(this.parseAlerte(elem))
                    }
                    println(alertes.count())
                   val grid = v?.findViewById<GridView>(R.id.grid_alertes)
                   grid?.adapter = AlertesAdapter(v!!.context, alertes)
                }else {
                    println("error :(")
                }
                return@use
            }else if (json.string("status") == "error") {
                println("error")
                return@use
            }else {
                println("error")
                return@use
            }
        })
    }

    private fun parseAlerte(alert:Element) : AlertesItem {
        val result = AlertesItem("", "", "","","", "", null)
        result.id = alert.attr("data-alert-id")
        result.id_author = alert.getElementsByClass("contentRow-figure")[0].child(0).attr("data-user-id")
        result.time = alert.getElementsByClass("u-dt")[0].attr("datetime")
        if (alert.getElementsByClass("username").count() > 0) {
            result.author = alert.getElementsByClass("username")[0].text()
        }else {
            result.author = null
        }
        val work_br = alert.getElementsByClass("contentRow-main")[0].getElementsByTag("a")
        if (work_br.count() > 0) {
            //for dir
        }else {
            //no dir
        }
        val parsing = this.parseContent(alert.getElementsByClass("contentRow-main")[0], result)
        if (parsing.count() > 1) {
            result.lnk = parsing[0] as String
            result.content = parsing[1] as String
        }else if (parsing.count() > 0) {
            result.content = parsing[0] as String
        }
        val config = ImageLoaderConfiguration.Builder(context).apply {

        }.build()
        ImageLoader.getInstance().init(config)
        val bmp = ImageLoader.getInstance().loadImageSync(getLinkAvatar(result.id_author!!))
        result.img = bmp
        return result
    }

    private fun parseContent(html: Element, al:AlertesItem) : ArrayList<Any> {
        var result = ArrayList<Any>()
        html.getElementsByClass("contentRow-minor").remove()
        html.getElementsByClass("username").remove()
        val href = html.getElementsByTag("a")
        if (href.count() > 0) {
            val lnk = href[0].text()
            val txt = html.text()
            result.add(0, lnk)
            var range = txt.indexOf(lnk)
            if (range < 0) { range = 0 }
            val content = "<font color=\"#2B2B2B\" face=\"Avenir-Light\" size=\"14px\"><div><font color=\"#F9A601\" face=\"Avenir-Black\">${if (al.author != null) al.author else ""}</font> ${txt.subSequence(0, range)}" +
                    "<font color=\"#509FDB\" face=\"Avenir-Heavy\">$lnk</font>${if (range + lnk.lastIndex == txt.lastIndex) { txt.subSequence(range + (if (lnk.lastIndex < 0) 0 else lnk.lastIndex), txt.lastIndex )} else  txt.subSequence(range + (if (lnk.lastIndex < 0) 0 else lnk.lastIndex) + 1, txt.lastIndex )}</div></font>"
            result.add(1, content)
        }else {
            val content = "<font color=\"#2B2B2B\" face=\"Avenir-Light\" size=\"14px\"><div><font color=\"#F9A601\" face=\"Avenir-Black\">${if (al.author != null) al.author else ""}</font> ${html.text()}</div></font>"
            result.add(0, content)
        }
        return result
    }

}// Required empty public constructor
