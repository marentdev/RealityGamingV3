package fr.realitygaming.marentdev.realitygaming.TabBarAccueil

import android.app.Fragment
import android.app.FragmentManager
import android.app.ListFragment
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import fr.realitygaming.marentdev.realitygaming.R
import fr.realitygaming.marentdev.realitygaming.TabBarAccueil.fragment.*
import kotlinx.android.synthetic.main.activity_tab_bar_accueil2.*

class TabBarAccueil2Activity : AppCompatActivity(), AccueilFragment.OnFragmentInteractionListener, ChatboxFragment.OnFragmentInteractionListener, NewsfeedFragment.OnFragmentInteractionListener,
        ConversationsFragment.OnFragmentInteractionListener, AlertesFragment.OnFragmentInteractionListener {

    private var fragmentManager = supportFragmentManager
    private  var fragment = android.support.v4.app.Fragment()
    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                fragment = AccueilFragment()
            }
            R.id.navigation_chatbox -> {
                fragment = ChatboxFragment()
            }
            R.id.navigation_newsfeed -> {
                fragment = NewsfeedFragment()
            }
            R.id.navigation_conversations -> {
                fragment = ConversationsFragment()
            }
            R.id.navigation_notifications -> {
                fragment = AlertesFragment()
            }
        }
        val fragtrans = fragmentManager.beginTransaction()
        fragtrans.replace(R.id.container,  fragment).commit()
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_bar_accueil2)
        val fragtrans = fragmentManager.beginTransaction()
        fragtrans.replace(R.id.container,  AccueilFragment()).commit()
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.enableItemShiftingMode(false)
        navigation.enableShiftingMode(false)
        navigation.enableAnimation(false)
        navigation.setTextVisibility(false)
        navigation.setIconTintList(2, ColorStateList.valueOf(Color.parseColor("#1E8CE0")))
        window.statusBarColor = Color.parseColor("#2C7CBA")

    }
}
