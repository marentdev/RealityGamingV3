package fr.realitygaming.marentdev.realitygaming.Divers

/**
 * Created by Marentdev on 13/03/2018.
 */
class Membre {
    var name: String? = null
    var id: String? = null
    var style: String? = null
    var messages: String? = null
    var jaimes: String? = null
    var points: String? = null

    constructor(name:String, id:String, style:String, messages:String, jaimes:String, points:String) {
        this.name = name
        this.id = id
        this.style = style
        this.messages = messages
        this.jaimes = jaimes
        this.points = points
    }

    constructor()
}