package fr.realitygaming.marentdev.realitygaming

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import fr.realitygaming.marentdev.realitygaming.ConnexionController.LoginActivity
import fr.realitygaming.marentdev.realitygaming.Divers.globalRG

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportActionBar?.hide()
        this.window.statusBarColor = Color.parseColor("#1E8CE0")
        val btn_account = findViewById<Button>(R.id.have_account)
        btn_account.setOnClickListener {
            this.goToConnexion()
        }
        val label_noaccount = findViewById<TextView>(R.id.no_account)
        label_noaccount.setOnClickListener {
            this.goToCreateAccount()
        }
    }

    fun goToConnexion() {
        val connection = Intent(this, LoginActivity::class.java)
        this.startActivity(connection)
    }

    fun goToCreateAccount() {
        print("No Account !")
    }
}
