//
//  ProfileViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 03/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwifterSwift
import SwiftSoup
import SwiftyJSON
import SwiftMessages
import SideMenu
import ActiveLabel
var user_id:String = ""
class ProfileViewController: ButtonBarPagerTabStripViewController {

    let blueInstagramColor = UIColor(red: 37/255.0, green: 111/255.0, blue: 206/255.0, alpha: 1.0)
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var banner: UIImageView!
    @IBOutlet weak var title_perso: UILabel!
    
    var member_id:String = user_id
    
    override func viewDidLoad() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = blueInstagramColor
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .black
            newCell?.label.textColor = self?.blueInstagramColor
        }
        super.viewDidLoad()
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuShadowRadius = 3
        SideMenuManager.default.menuWidth = 260
        if section_active[2] {
            let btn = UIBarButtonItem(image: UIImage.fontAwesomeIcon(name: "", textColor: .black, size: CGSize(width: 30, height: 35)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openMenu))
            btn.tintColor = .black
            self.navigationItem.leftBarButtonItems = [btn]
        }
        loadImageFromUrl(url: get_banner_link(id: member_id), view: self.banner, isbanner: true)
        loadImageFromUrl(url: get_avatar_link(id: member_id), view: self.avatar)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupPage()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
    }
    
    @objc func openMenu() {
        self.performSegue(withIdentifier: "openmenu", sender: self)
    }
    
    func setupPage() {
        session.request(URL(string: "https://\(domain_name)/members/\(member_id)")!).validate().responseString { (datarep) in
            if datarep.response?.statusCode == 403 {
                let alert =  MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
                alert.configureContent(title: "Erreur", body: "Ce membre a limité l'affichage de son profil.")
                alert.configureTheme(.error)
                alert.button?.isHidden = true
                var config = SwiftMessages.defaultConfig
                config.duration = .seconds(seconds: 3)
                SwiftMessages.show(config: config, view: alert)
                self.navigationController?.popViewController()
                return
            }
            if datarep.result.value != nil {
                do {
                    let document:Document = try SwiftSoup.parse(datarep.result.value!)
                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: getColorRank(rank: Int(try document.getElementsByAttributeValueMatching("class", "username--style(.*?)").eq(0).attr("class").remove(text: ["username--style", " username--staff", "username--admin", "username--moderator", " "]))!)]
                    self.navigationItem.title = try document.getElementsByClass("username").get(0).text()
                    self.title_perso.text = try document.getElementsByClass("memberHeader-blurb").get(0).text()
                } catch Exception.Error(let type, let message) {
                    print(type, message)
                } catch {
                    print("error")
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController, completionHandler: @escaping ([UIViewController]) -> ()) {
        completionHandler([MessageProfileTableViewController(), ActivitesgeneralesTableViewController(), DerniersMessagesTableViewController()])
    }
  

}
