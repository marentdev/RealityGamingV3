//
//  ActivityProfileTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 04/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class ActivityProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var title_act: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
