//
//  MessagesProfileTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 04/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import ActiveLabel
class MessagesProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var pseudo: UILabel!
    @IBOutlet weak var content: ActiveLabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
