//
//  DerniersMessagesTableViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 04/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwifterSwift
import SwiftSoup
import SwiftyJSON
import SwiftMessages
import SideMenu
class DerniersMessagesTableViewController: UITableViewController, IndicatorInfoProvider {

    struct activity {
        var title:String = ""
        var content:String = ""
        var date:String = ""
        var dest:String = ""
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Derniers messages")
    }
    
    var member_id:String = user_id
    var activitys:[activity] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.tableView.estimatedRowHeight = 96
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }

    var first = true
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if first {
            self.showWaitOverlayWithText("Loading...")
            self.setupPage()
            first = false
        }
    }
    
    func setupPage() {
        self.activitys.removeAll()
        session.request(URL(string: "https://\(domain_name)/members/\(member_id)/recent-content")!).validate().responseString { (datarep) in
            if datarep.result.value != nil {
                do {
                    let document:Document = try SwiftSoup.parse(datarep.result.value!)
                    let block_msg = try document.getElementsByClass("block-row")
                    try block_msg.forEach({ (msg) in
                        var new_msg:activity = activity()
                        new_msg.title = try msg.getElementsByClass("contentRow-title").get(0).text()
                        new_msg.content = try msg.getElementsByClass("contentRow-snippet").get(0).text()
                        new_msg.date = try msg.getElementsByClass("contentRow-minor").get(0).text()
                        new_msg.dest = try msg.getElementsByTag("a").get(1).attr("href")
                        
                        self.activitys.append(new_msg)
                    })
                    self.tableView.reloadData()
                    self.removeAllOverlays()
                } catch Exception.Error(let type, let message) {
                    print(type, message)
                } catch {
                    print("error")
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.activitys.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "ActivityProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "act")
        let cell = tableView.dequeueReusableCell(withIdentifier: "act", for: indexPath) as! ActivityProfileTableViewCell
        cell.title_act.text = self.activitys[indexPath.row].title
        cell.content.text = self.activitys[indexPath.row].content
        cell.date.text = self.activitys[indexPath.row].date
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let desti = parse_dir(path: self.activitys[indexPath.row].dest)
        if desti.isMember(of: UIViewController.self)  { return }
        self.navigationController?.pushViewController(desti)
    }

}
