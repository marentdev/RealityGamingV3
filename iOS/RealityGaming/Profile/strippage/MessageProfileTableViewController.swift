//
//  MessageProfileTableViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 04/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwifterSwift
import SwiftSoup
import SwiftyJSON
import SwiftMessages
import SideMenu
import FCAlertView
class MessageProfileTableViewController: UITableViewController, IndicatorInfoProvider {
    
    struct message {
        var author_attr:NSAttributedString?
        var author:String = ""
        var date:String = ""
        var content:String = ""
        var msg_likes:NSAttributedString?
        var styleid:String = ""
        var id:String = "'"
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Messages de profil")
    }
    
    var member_id:String = user_id
    var messages:[message] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressCell))
        self.tableView.addGestureRecognizer(longPressRecognizer)
        self.tableView.estimatedRowHeight = 96
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    var first = true
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if first {
            self.showWaitOverlayWithText("Loading...")
            self.setupPage()
            first = false
        }
    }
    @objc func longPressCell(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            let touchPoint = longPressGestureRecognizer.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
            let alert = UIAlertController(style: .actionSheet, source: self.view, title: "Message de \(self.messages[indexPath.row].author)", message: "", tintColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
           /* if self.messages[indexPath.row].author != Profile.pseudo {
                alert.addAction(UIAlertAction(title: "Like", style: .default, handler: { (aler) in
                    //Like
                }))
            }*/
           // alert.addAction(UIAlertAction(title: "Commenter", style: .default, handler: { (aler) in
                //commenter
           // }))
            alert.addAction(UIAlertAction(title: "Signaler", style: .default, handler: { (aler) in
                let alert = FCAlertView()
                let textfield:UITextField = UITextField()
                alert.addTextField(withCustomTextField: textfield, andPlaceholder: "Message", andTextReturn: { str in
                    textfield.text = str
                })
                alert.addButton("Signaler", withActionBlock: {
                    session.request(URL(string: "https://\(domain_name)/profile-posts/\(self.messages[indexPath.row].id)/report")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType":"json", "message": textfield.text!]).responseJSON(completionHandler: { (datarep) in
                            let json:JSON = JSON(datarep.result.value!)
                            if json["message"] != JSON.null {
                            }else if json["status"] != JSON.null {
                                let alert =  MessageView.viewFromNib(layout: .cardView)
                                alert.configureContent(title: "Erreur", body: json["errors"][0].string!)
                                alert.configureTheme(.error)
                                alert.button?.isHidden = true
                                var config = SwiftMessages.defaultConfig
                                config.duration = .seconds(seconds: 3)
                                SwiftMessages.show(config: config, view: alert)
                            }
                    })
                })
                alert.showAlert(withTitle: "Signaler le message", withSubtitle: "", withCustomImage: nil, withDoneButtonTitle: "Cancel", andButtons: nil)
            }))
            alert.addAction(UIAlertAction(title: "Annuler", style: .destructive, handler: { (aler) in
                //annuler
            }))
            self.present(alert, animated: true, completion: nil)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.messages.count
    }

    func setupPage() {
        self.messages.removeAll()
        session.request(URL(string: "https://\(domain_name)/members/\(member_id)")!).validate().responseString { (datarep) in
            if datarep.result.value != nil {
                do {
                    let document:Document = try SwiftSoup.parse(datarep.result.value!)
                    let block_msg = try document.getElementsByClass("message")
                    try block_msg.forEach({ (msg) in
                        if msg.getAttributes()?.get(key: "method") == "" {
                            var new_msg:message = message()
                            new_msg.content = try msg.getElementsByClass("message-body").get(0).text()
                            new_msg.author = try msg.getElementsByClass("attribution").get(0).text()
                            new_msg.date = try msg.getElementsByClass("u-dt").get(0).text()
                            new_msg.styleid = try msg.getElementsByAttributeValueMatching("class", "username--style(.*?)").eq(0).attr("class").remove(text: ["username--style", "username--staff", "username--admin", "username--moderator", " "])
                            if try msg.getElementsByClass("message-responseRow--likes").get(0).getElementsByTag("a").array().count > 0 {
                                new_msg.msg_likes = stringFromHtml(string: "<div style=\"font-size:12px;font-family:'Helvetica';color:#9A9A9A\"><span style=\"font-family:'FontAwesome';font-size:12px;\"></span> \( try msg.getElementsByClass("message-responseRow--likes").get(0).getElementsByTag("a").get(0).text())</div>")
                            }else {
                                new_msg.msg_likes = stringFromHtml(string: "")
                            }
                            if FA_rank["style\(new_msg.styleid)"] != nil {
                                new_msg.author_attr = stringFromHtml(string: "<b style=\"color:\(getColorRank(rank: Int(new_msg.styleid)!).shortHexOrHexString);font-family:'Helvetica';font-size:15px;\"><span style=\"font-family:'FontAwesome';font-size:15px;\">\(FA_rank["style\(new_msg.styleid)"]!)</span> \(new_msg.author)</b>")
                            }else {
                                new_msg.author_attr = stringFromHtml(string: "<b style=\"color:\(getColorRank(rank: Int(new_msg.styleid)!).shortHexOrHexString);font-family:'Helvetica';font-size:15px;\">\(new_msg.author)</b>")
                            }
                            new_msg.id = try msg.getElementsByClass("u-anchorTarget").get(0).attr("id").remove(text: ["profile-post-"])
                            self.messages.append(new_msg)
                        }
                    })
                    self.tableView.reloadData()
                    self.removeAllOverlays()
                } catch Exception.Error(let type, let message) {
                    print(type, message)
                } catch {
                    print("error")
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "MessagesProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "msg")
        let cell = tableView.dequeueReusableCell(withIdentifier: "msg", for: indexPath) as! MessagesProfileTableViewCell
        
        getLinkAvatarFormPseudo(username: self.messages[indexPath.row].author, imageView: cell.avatar)
        cell.pseudo.attributedText = self.messages[indexPath.row].author_attr
        cell.content.text = self.messages[indexPath.row].content
        cell.date.text = self.messages[indexPath.row].date
        cell.likes.attributedText = self.messages[indexPath.row].msg_likes
        cell.content.enabledTypes = [.url, .mention]
        cell.content.handleURLTap { (url) in
            let desti = parse_dir(path: url.absoluteString)
            if desti.isMember(of: UIViewController.self)  { return }
            self.navigationController?.pushViewController(desti)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }


}
