//
//  ShoutboxStripeViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 11/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SwiftSoup
import SwifterSwift
import SideMenu
var array_for_shout:[UIViewController] = []
var setup_ok:Bool = false
class ShoutboxStripeViewController: ButtonBarPagerTabStripViewController {

    let blueInstagramColor = UIColor(red: 37/255.0, green: 111/255.0, blue: 206/255.0, alpha: 1.0)
    override func viewDidLoad() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = blueInstagramColor
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .black
            newCell?.label.textColor = self?.blueInstagramColor
        }
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuShadowRadius = 3
        SideMenuManager.default.menuWidth = 260
        let btn = UIBarButtonItem(image: UIImage.fontAwesomeIcon(name: "", textColor: .black, size: CGSize(width: 30, height: 35)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openMenu))
        btn.tintColor = .black
        self.navigationItem.leftBarButtonItems = [btn]
    }

    @objc func openMenu() {
        self.performSegue(withIdentifier: "openmenu", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController, completionHandler: @escaping ([UIViewController]) -> ()) {
        array_for_shout.append(ShoutboxSelect_RoomsViewController.init(itemInfo: IndicatorInfo(title: "Parcourir les salons")))
        array_for_shout.removeDuplicates()
        var i = 0
        array_for_shout.forEach { (viewcont) in
            if viewcont is ShoutboxSelect_RoomsViewController {
                i += 1
            }
            if viewcont is ShoutboxSelect_RoomsViewController && i > 1 {
                array_for_shout.remove(viewcont)
            }
        }
        if self.viewControllers.count > 0 || setup_ok { completionHandler(array_for_shout); return }
        setup_ok = true
        completionHandler(array_for_shout)
    }
    
}
