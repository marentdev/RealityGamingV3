//
//  ShoutboxList+RoomsTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 13/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class ShoutboxList_RoomsTableViewCell: UITableViewCell {

    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var Desc: UILabel!
    @IBOutlet weak var Info: UILabel!
    @IBOutlet weak var join: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
