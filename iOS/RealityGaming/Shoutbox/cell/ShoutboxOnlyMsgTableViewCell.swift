//
//  ShoutboxOnlyMsgTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 05/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import ActiveLabel
class ShoutboxOnlyMsgTableViewCell: UITableViewCell {

    @IBOutlet weak var content_html: ActiveLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
