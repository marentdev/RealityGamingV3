//
//  ShoutboxMsgTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 12/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import ActiveLabel
class ShoutboxMsgTableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var content_html: ActiveLabel!
    @IBOutlet weak var pseudo_date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
