//
//  ShoutboxSelect+RoomsViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 13/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import SwifterSwift
import SwiftMessages
import SwiftSoup
import SwiftyJSON
import Alamofire
import XLPagerTabStrip
class ShoutboxSelect_RoomsViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource {

    var itemInfo = IndicatorInfo(title: "name")
    
    struct room {
        var name:String = ""
        var desc:String = ""
        var author:String = ""
        var nb_user:Int = 0
        var id:Int = 0
        var canjoin:Bool = true
    }
    
    var rooms:[room] = []
    var tableView:UITableView = UITableView()
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsSelection = false
        self.tableView.estimatedRowHeight = 96
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.view.addSubview(tableView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        setupPage()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register( UINib(nibName: "ShoutboxList+RoomsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ShoutboxList_RoomsTableViewCell
        cell.join.removeTarget(self, action: self.rooms[indexPath.row].canjoin ? #selector(self.leaveButton) : #selector(self.joinButton), for: .touchUpInside)
        cell.Title.text = self.rooms[indexPath.row].name
        cell.Desc.text = self.rooms[indexPath.row].desc
        cell.join.tag = indexPath.row
        cell.join.setTitleForAllStates(self.rooms[indexPath.row].canjoin ? "Rejoindre le salon" : "Quitter le salon")
        cell.join.addTarget(self, action: self.rooms[indexPath.row].canjoin ? #selector(self.joinButton) : #selector(self.leaveButton), for: .touchUpInside)
        cell.Info.text = "Créé par \(self.rooms[indexPath.row].author), \(self.rooms[indexPath.row].nb_user) Utilisateurs"
        return cell
    }
    
    @objc func joinButton(_ sender:UIButton) {
        session.request(URL(string: "https://\(domain_name)/chat/room/\(self.rooms[sender.tag].id)/join")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType": "json"]).validate().responseJSON { (datarep) in
            if datarep.result.value == nil { return }
            do {
                let json:JSON = JSON(datarep.result.value!)
                if json["action"] != JSON.null {
                    if json["action"].string! == "join" {
                        //good
                        self.rooms[sender.tag].canjoin = false
                        self.setupPage()
                    }
                }else {
                    let alert =  MessageView.viewFromNib(layout: .cardView)
                    alert.configureContent(title: "Erreur", body: json["message"].string!)
                    alert.configureTheme(.error)
                    var config = SwiftMessages.defaultConfig
                    config.duration = .seconds(seconds: 3)
                    alert.button?.isHidden = true
                    SwiftMessages.show(config: config, view: alert)
                }
            }catch {}
        }
    }
    @objc func leaveButton(_ sender:UIButton) {
        session.request(URL(string: "https://\(domain_name)/chat/room/\(self.rooms[sender.tag].id)/leave")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType": "json"]).validate().responseJSON { (datarep) in
            if datarep.result.value == nil { return }
            do {
                let json:JSON = JSON(datarep.result.value!)
                if json["action"] != JSON.null {
                    if json["action"].string! == "leave" {
                        //good
                        self.rooms[sender.tag].canjoin = true
                        self.setupPage()
                    }
                }else {
                    let alert =  MessageView.viewFromNib(layout: .cardView)
                    alert.configureContent(title: "Erreur", body: json["message"].string!)
                    alert.configureTheme(.error)
                    var config = SwiftMessages.defaultConfig
                    config.duration = .seconds(seconds: 3)
                    alert.button?.isHidden = true
                    SwiftMessages.show(config: config, view: alert)
                }
            }catch {}
        }
    }
    
    func setupPage() {
        self.rooms.removeAll()
        session.request(URL(string: "https://\(domain_name)/chat/room/list")!, method: .get, parameters: ["_xfToken":_xfToken, "_xfResponseType": "json"]).validate().responseJSON { (datarep) in
            do {
                let json:JSON = try! JSON(datarep.result.value!)
                if json["status"].string! == "ok" {
                    let document:Document = try! SwiftSoup.parse(json["html"]["content"].string!)
                    let li_room:Elements = try! document.getElementsByAttribute("data-room-id")
                    li_room.forEach({ (elem) in
                        var new_room:room = room()
                        new_room.name = try! elem.getElementsByTag("h3").get(0).text()
                        new_room.desc = try! elem.getElementsByClass("siropuChatRoomDescription").get(0).text()
                        new_room.author = try! elem.getElementsByClass("username").get(0).text()
                        new_room.nb_user = Int(try! elem.getElementsByClass("siropuChatRoomUsers").get(0).getElementsByTag("b").get(0).text())!
                        new_room.id = Int((try! elem.getAttributes()?.get(key: "data-room-id"))!)!
                        if try! elem.getElementsByClass("siropuChatRoomLeave").array().count > 0 { new_room.canjoin = false }
                        print(new_room)
                        self.rooms.append(new_room)
                    })
                    self.tableView.reloadData()
                }
            }catch { }
        }
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
