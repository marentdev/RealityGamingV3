//
//  ShoutboxV2ViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 11/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwiftSoup
import SwiftyJSON
import SwiftMessages
import SwifterSwift
import ActiveLabel
import FCAlertView
import PTPopupWebView
class ShoutboxV2ViewController: MessageViewController, UITableViewDataSource, UITableViewDelegate, MessageAutocompleteControllerDelegate, IndicatorInfoProvider {

    internal struct chat_msg {
        var author:String = ""
        var author_attr:NSAttributedString?
        var id_message:Int = 0
        var id_author:Int = 0
        var content:String = ""
        var date:String = ""
        var tag:[String] = []
        var rank:Int = 0
        var isbot:Bool = false
    }
    var is_invert:Bool = false
    var itemInfo = IndicatorInfo(title: "room_name")
    var infos:[String] = [] //room_id
    var data:[chat_msg] = []
    var autocompleteUsers = [String]()
    let tableView = UITableView()
    var updateTimer: Timer!
    var busy:Bool = false
    var scroll_auto:Bool = true
    init(itemInfo: IndicatorInfo, infos: [String]) {
        self.itemInfo = itemInfo
        self.infos = infos
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getProfile { }
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        view.addSubview(tableView)
        self.tableView.estimatedRowHeight = 96
        self.tableView.rowHeight = UITableViewAutomaticDimension
        borderColor = .lightGray
        
        messageView.inset = UIEdgeInsets(top: 10, left: 16, bottom: 8, right: 16)
        messageView.textView.placeholderText = "New message..."
        messageView.textView.placeholderTextColor = .lightGray
        messageView.font = UIFont.systemFont(ofSize: 17)
    
        messageView.set(buttonTitle: "Send", for: .normal)
        messageView.addButton(target: self, action: #selector(onButton))
        messageView.buttonTint = .blue
        
        messageView.height = 40
        
        setup(scrollView: tableView)
        tableView.rowHeight = UITableViewAutomaticDimension
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressCell))
        self.tableView.addGestureRecognizer(longPressRecognizer)
        DispatchQueue.global(qos: .background).async {
            self.setupPage()
        }
    
        self.hideKeyboardWhenTappedAround()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.navigationController?.navigationBar.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    @objc func longPressCell(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            if busy { return }
            //busy = true
            view.endEditing(true)
                let touchPoint = longPressGestureRecognizer.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                if self.data[indexPath.row].isbot { return }
                let alert = UIAlertController(style: .actionSheet)
                if self.data[indexPath.row].author == Profile.pseudo! {
                    alert.addAction(title: "Edit",style: .default, isEnabled: true, handler: { (okey) in
                        let alert = FCAlertView()
                        let textfield:UITextField = UITextField()
                        textfield.text = self.data[indexPath.row].content
                        alert.addTextField(withCustomTextField: textfield, andPlaceholder: "Message", andTextReturn: { str in
                            textfield.text = str
                        })
                        alert.addButton("Sauvegarder", withActionBlock: {
                            session.request(URL(string: "https://\(domain_name)/chat/message/\(self.data[indexPath.row].id_message)/edit")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType":"json", "message_html": textfield.text!]).responseJSON(completionHandler: { (datarep) in
                                do {
                                    let json:JSON = try! JSON(datarep.result.value!)
                                    if json["message"] != JSON.null {
                                        if json["message"].string! == "Le message a été modifié." {
                                            self.data.remove(at: indexPath.row)
                                            self.tableView.deleteRows(at: [indexPath], with: .automatic)
                                        }
                                    }else if json["errors"] != JSON.null {
                                        let alert =  MessageView.viewFromNib(layout: .cardView)
                                        alert.configureContent(title: "Erreur", body: json["errors"][0].string!)
                                        alert.configureTheme(.error)
                                        alert.button?.isHidden = true
                                        var config = SwiftMessages.defaultConfig
                                        config.duration = .seconds(seconds: 3)
                                        SwiftMessages.show(config: config, view: alert)
                                    }
                                    self.busy = false
                                }catch {}
                            })
                        })
                        alert.doneBlock = {
                            self.busy = false
                        }
                        alert.showAlert(withTitle: "Edit message", withSubtitle: "", withCustomImage: nil, withDoneButtonTitle: "Cancel", andButtons: nil)
                    })
                    alert.addAction(title: "Supprimer", style: .default, isEnabled: true, handler: { (alert) in
                        session.request(URL(string: "https://\(domain_name)/chat/message/\(self.data[indexPath.row].id_message)/delete")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType":"json"]).responseJSON(completionHandler: { (datarep) in
                            do {
                                let json:JSON = try! JSON(datarep.result.value!)
                                if json["message"] != JSON.null {
                                    if json["message"].string! == "Le message a été supprimé." {
                                        self.data.remove(at: indexPath.row)
                                        self.tableView.deleteRows(at: [indexPath], with: .automatic)
                                    }
                                }else if json["status"] != JSON.null {
                                    let alert =  MessageView.viewFromNib(layout: .cardView)
                                    alert.configureContent(title: "Erreur", body: json["errors"][0].string!)
                                    alert.configureTheme(.error)
                                    alert.button?.isHidden = true
                                    var config = SwiftMessages.defaultConfig
                                    config.duration = .seconds(seconds: 3)
                                    SwiftMessages.show(config: config, view: alert)
                                }
                                self.busy = false
                            }catch {}
                        })
                    })
                }else {
                    alert.addAction(title: "Signaler", style: .default, isEnabled: true, handler: { (alert) in
                        let alert = FCAlertView()
                        let textfield:UITextField = UITextField()
                        alert.addTextField(withCustomTextField: textfield, andPlaceholder: "Message", andTextReturn: { str in
                            textfield.text = str
                        })
                        alert.addButton("Signaler", withActionBlock: {
                            session.request(URL(string: "https://\(domain_name)/chat/message/\(self.data[indexPath.row].id_message)/report")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType":"json", "message_html": textfield.text!]).responseJSON(completionHandler: { (datarep) in
                                do {
                                    let json:JSON = try! JSON(datarep.result.value!)
                                    if json["message"] != JSON.null {
                                    }else if json["status"] != JSON.null {
                                        let alert =  MessageView.viewFromNib(layout: .cardView)
                                        alert.configureContent(title: "Erreur", body: json["errors"][0].string!)
                                        alert.configureTheme(.error)
                                        alert.button?.isHidden = true
                                        var config = SwiftMessages.defaultConfig
                                        config.duration = .seconds(seconds: 3)
                                        SwiftMessages.show(config: config, view: alert)
                                    }
                                    self.busy = false
                                }catch {}
                            })
                        })
                        alert.doneBlock = {
                            self.busy = false
                        }
                        alert.showAlert(withTitle: "Signaler le message", withSubtitle: "", withCustomImage: nil, withDoneButtonTitle: "Cancel", andButtons: nil)
                    })
                }
                alert.addAction(title: "Cancel", style: .destructive, isEnabled: true, handler: { (alert) in
                    self.busy = false
                })
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    @objc func onButton() {
        self.messageView.button.isEnabled = false
        let msg = messageView.text
        messageView.text = ""
        session.request(URL(string: "https://\(domain_name)/chat/submit")!, method: .post, parameters: ["channel":"room", "room_id": self.infos[0], "last_id[\(self.infos[0])]": self.data.last!.id_message, "is_chat_page": true, "_xfToken": _xfToken, "_xfResponseType": "json", "message": msg]).responseJSON { (datarep) in
            do {
                let json:JSON = try! JSON(datarep.result.value!)
                if json["action"] != JSON.null {
                    
                }else {
                    self.messageView.text = msg
                    let alert =  MessageView.viewFromNib(layout: .cardView)
                    alert.configureContent(title: "Erreur", body: json["errors"].string!)
                    alert.configureTheme(.error)
                    alert.button?.isHidden = true
                    var config = SwiftMessages.defaultConfig
                    config.duration = .seconds(seconds: 3)
                    SwiftMessages.show(config: config, view: alert)
                }
            }catch {}
        }
    }
    
    func setupPage() {
        session.request(URL(string: "https://\(domain_name)/chat/fullpage")!).responseString { (datarep) in
            let document:Document = try! SwiftSoup.parse(datarep.result.value!)
            let div_chatbox:Elements = try! document.getElementsByClass("siropuChatRoom siropuChatMessages")
            //<input type="checkbox" name="inverse" value="1">
            let val = try! document.getElementsByAttributeValue("name", "inverse").get(0).getAttributes()?.get(key: "checked")
            if val == "1" {
                self.is_invert = true
            }
            div_chatbox.forEach({ (elem) in
                if elem.getAttributes()?.get(key: "data-room-id") == self.infos[0] {
                    let li_msg:Elements = try! elem.getElementsByTag("li")
                    li_msg.forEach({ (msg) in
                        var new_msg = chat_msg()
                        new_msg.date = try! msg.getElementsByClass("siropuChatMessageContentRight").get(0).getElementsByClass("siropuChatDateTime").get(0).text()
                        let content_left:Element = try! msg.getElementsByClass("siropuChatMessageContentLeft").get(0)
                        new_msg.author = try! content_left.getElementsByTag("span").get(0).text()
                        if new_msg.author.count <= 1 {
                            new_msg.author = try! content_left.getElementsByTag("span").get(1).text()
                            new_msg.rank = Int((try! content_left.getElementsByTag("span").get(1).getAttributes()?.get(key: "class").remove(text: ["username--style", " ", "username--staff", "username--admin", "username--moderator"]))!)!
                        }else if new_msg.author == "BOT" {
                            new_msg.isbot = true
                            new_msg.rank = Int((try! content_left.getElementsByTag("span").get(0).getAttributes()?.get(key: "class").remove(text: ["username--style", " ", "username--staff", "username--admin", "username--moderator"]))!)!
                        }else if try! content_left.getElementsByClass("avatar").get(0).getElementsByTag("span").array().count > 0 {
                            new_msg.rank = Int((try! content_left.getElementsByTag("span").get(1).getAttributes()?.get(key: "class").remove(text: ["username--style", " ", "username--staff", "username--admin", "username--moderator"]))!)!
                        }else {
                            new_msg.rank = Int((try! content_left.getElementsByClass("username").get(0).getElementsByTag("span").get(0).getAttributes()?.get(key: "class").remove(text: ["username--style", " ", "username--staff", "username--admin", "username--moderator"]))!)!
                        }
                        new_msg.id_author = Int((try! content_left.getElementsByTag("a").get(0).getAttributes()?.get(key: "data-user-id"))!)!
                        let result = self.parseMsg(msg: try! content_left.getElementsByClass("siropuChatMessageText").get(0).html())
                        new_msg.content = result[0][0]
                        new_msg.tag = result[1]
                        new_msg.id_message = Int((try! msg.getAttributes()?.get(key: "data-id"))!)!
                        if new_msg.author == "BOT" && result[1].count > 0 { new_msg.author = result[1][0] }
                        if FA_rank["style\(new_msg.rank)"] != nil {
                            new_msg.author_attr = stringFromHtml(string: "<b style=\"color:\(getColorRank(rank: Int(new_msg.rank)).shortHexOrHexString);font-family:'Helvetica';font-size:15px;\"><span style=\"font-family:'FontAwesome';font-size:15px;\">\(FA_rank["style\(new_msg.rank)"]!)</span> \(new_msg.author)</b>")
                            //<span style=\"font-size:12px;font-family:'Helvetica';color:'#9D9D9D';\">\(new_msg.date)</span>
                        }else {
                            new_msg.author_attr = stringFromHtml(string: "<b style=\"color:\(getColorRank(rank: Int(new_msg.rank)).shortHexOrHexString);font-family:'Helvetica';font-size:15px;\">\(new_msg.author)</b>")
                            //<span style=\"font-size:12px;font-family:'Helvetica';color:'#9D9D9D';\">\(new_msg.date)</span>
                        }
                        self.data.append(new_msg)
                    })
                }
            })
            if self.is_invert {
                self.data.reverse()
            }
            self.tableView.reloadData {
                self.tableView.scrollToBottom()
                self.updateTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.updateChat), userInfo: nil, repeats: true)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.updateTimer != nil {
            self.updateTimer.invalidate()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.scrollToBottom()
        if self.data.isEmpty { return }
        if self.updateTimer != nil { if self.updateTimer.isValid { return } }
        self.updateTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.updateChat), userInfo: nil, repeats: true)
    }
    
    func parseMsg(msg:String) -> [[String]] {
        do {
            let msg_doc:Document = try! SwiftSoup.parse(msg)
            var tagged:[String] = []
            try! msg_doc.getElementsByClass("smilie").forEach({ (elem) in
                try! elem.html((try! elem.getAttributes()?.get(key: "alt"))!)
            })
            try! msg_doc.getElementsByClass("username").forEach({ (elem) in
                tagged.append(try! elem.text())
            })
            return [[try! msg_doc.text()], tagged]
        }catch {}
        return []
    }
    
    @objc func updateChat() {
        if data.isEmpty { return }
        DispatchQueue.global(qos: .background).async {
            session.request(URL(string: "https://\(domain_name)/chat/update")!, method: .post, parameters: ["channel":"room", "room_id": self.infos[0], "last_id[\(self.infos[0])]": self.data.last!.id_message, "is_chat_page": true, "_xfToken": _xfToken, "_xfResponseType": "json"]).validate().responseJSON(completionHandler: { (datajson) in
                do {
                    var updatedData:[chat_msg] = []
                    debugPrint(datajson)
                    if datajson.response == nil { return }
                    let json:JSON = JSON(datajson.result.value!)
                    if json["status"] == "error" { return }
                    if json["rooms"][self.infos[0]]["messages"].string! == "\n\t\n" { debugPrint(json) } else {
                        let doc:Document = try! SwiftSoup.parse(json["rooms"][self.infos[0]]["messages"].string!)
                        let li_msg:Elements = try! doc.getElementsByTag("li")
                        li_msg.forEach({ (msg) in
                            var new_msg = chat_msg()
                            new_msg.date = try! msg.getElementsByClass("siropuChatMessageContentRight").get(0).getElementsByClass("siropuChatDateTime").get(0).text()
                            let content_left:Element = try! msg.getElementsByClass("siropuChatMessageContentLeft").get(0)
                            new_msg.author = try! content_left.getElementsByTag("span").get(0).text()
                            if new_msg.author.count <= 1 {
                                new_msg.author = try! content_left.getElementsByTag("span").get(1).text()
                                new_msg.rank = Int((try! content_left.getElementsByTag("span").get(1).getAttributes()?.get(key: "class").remove(text: ["username--style", " ", "username--staff", "username--admin", "username--moderator"]))!)!
                            }else if new_msg.author == "BOT" {
                                new_msg.isbot = true
                                new_msg.rank = Int((try! content_left.getElementsByTag("span").get(0).getAttributes()?.get(key: "class").remove(text: ["username--style", " ", "username--staff", "username--admin", "username--moderator"]))!)!
                            }else if try! content_left.getElementsByClass("avatar").get(0).getElementsByTag("span").array().count > 0 {
                                new_msg.rank = Int((try! content_left.getElementsByTag("span").get(1).getAttributes()?.get(key: "class").remove(text: ["username--style", " ", "username--staff", "username--admin", "username--moderator"]))!)!
                            }else {
                                new_msg.rank = Int((try! content_left.getElementsByClass("username").get(0).getElementsByTag("span").get(0).getAttributes()?.get(key: "class").remove(text: ["username--style", " ", "username--staff", "username--admin", "username--moderator"]))!)!
                            }
                            new_msg.id_author = Int((try! content_left.getElementsByTag("a").get(0).getAttributes()?.get(key: "data-user-id"))!)!
                            let result = self.parseMsg(msg: try! content_left.getElementsByClass("siropuChatMessageText").get(0).html())
                            new_msg.content = result[0][0]
                            new_msg.tag = result[1]
                            new_msg.id_message = Int((try! msg.getAttributes()?.get(key: "data-id"))!)!
                            if new_msg.author == "BOT" && result[1].count > 0 { new_msg.author = result[1][0] }
                            if FA_rank["style\(new_msg.rank)"] != nil {
                                new_msg.author_attr = stringFromHtml(string: "<b style=\"color:\(getColorRank(rank: Int(new_msg.rank)).shortHexOrHexString);font-family:'Helvetica';font-size:15px;\"><span style=\"font-family:'FontAwesome';font-size:15px;\">\(FA_rank["style\(new_msg.rank)"]!)</span> \(new_msg.author)</b>")
                                //<span style=\"font-size:12px;font-family:'Helvetica';color:'#9D9D9D';\">\(new_msg.date)</span>
                            }else {
                                new_msg.author_attr = stringFromHtml(string: "<b style=\"color:\(getColorRank(rank: Int(new_msg.rank)).shortHexOrHexString);font-family:'Helvetica';font-size:15px;\">\(new_msg.author)</b>")
                                //<span style=\"font-size:12px;font-family:'Helvetica';color:'#9D9D9D';\">\(new_msg.date)</span>
                            }
                            updatedData.append(new_msg)
                            if self.is_invert {
                                updatedData.reverse()
                            }
                            self.data.append(updatedData)
                        })
                        self.data.forEach({ (msg) in
                            var one:Int = 0
                            var i:Int = 0
                            while i < self.data.count {
                                if msg.id_message == self.data[i].id_message {
                                    if one == 1 {
                                        self.data.remove(at: i)
                                    }else { one = 1 }
                                }
                                i += 1
                            }
                        })
                        if self.scroll_auto {
                            self.tableView.reloadData()
                            self.tableView.scrollToRow(at: IndexPath(row: self.data.count - 1, section: 0), at: .bottom, animated: true)
                        }else {
                            self.tableView.reloadData()
                        }
                    }
                    self.manageEdit(json: json)
                }catch {
                    
                }
            })
        }
    }
    
    func manageEdit(json:JSON) {
        if json["actions"] != JSON.null {
            json["actions"].forEach({ (id_room, room) in
                if id_room == self.infos[0] {
                    room.forEach({ (id_msg, json) in
                        self.tableView.reloadData()
                        json["action"].first(where: { (key, useless) -> Bool in
                            if key == "edit" {
                                let result = self.parseMsg(msg: json["html"].string!)
                                let i = self.data.index(where: { (chat_msg) -> Bool in if chat_msg.id_message == Int(id_msg) { return true }; return false})!
                                let msg = chat_msg(author: self.data[i].author, author_attr: nil, id_message: self.data[i].id_message, id_author: self.data[i].id_author, content: result[0][0], date: self.data[i].date, tag: result[1], rank: self.data[i].rank, isbot: self.data[i].isbot)
                                if self.data[i].content == msg.content {
                                    return true
                                }
                                self.data[i] = msg
                                self.tableView.beginUpdates()
                                self.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .none)
                                self.tableView.endUpdates()
                            } else if key == "delete" {
                                let i = self.data.index(where: { (chat_msg) -> Bool in if chat_msg.id_message == Int(id_msg) { return true }; return false})
                                if i == nil { return true }
                                self.data.remove(at: i!)
                                self.tableView.beginUpdates()
                                self.tableView.deleteRows(at: [IndexPath.init(row: i!, section: 0)], with: .automatic)
                                self.tableView.endUpdates()
                            }
                            return true
                        })
                    })
                }
            })
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView === self.tableView
            ? data.count
            : autocompleteUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if tableView === self.tableView {
            tableView.register(UINib(nibName: "ShoutboxMsgTableViewCell", bundle: nil), forCellReuseIdentifier: "msg")
            let cellmsg = tableView.dequeueReusableCell(withIdentifier: "msg", for: indexPath) as! ShoutboxMsgTableViewCell
            var ar_ment:[String] = []
            self.data[indexPath.row].tag.forEach({ (str) in
                ar_ment.append(str)
            })
            ActiveLabel.setupMentionRegex(regex: ar_ment)
            getLinkAvatarFormPseudo(username: self.data[indexPath.row].author, imageView: cellmsg.avatar)
            cellmsg.pseudo_date.attributedText = self.data[indexPath.row].author_attr
            cellmsg.avatar.borderWidth = 2
            cellmsg.avatar.borderColor = getColorRank(rank: self.data[indexPath.row].rank)
            cellmsg.avatar.layer.masksToBounds = true
            cellmsg.avatar.layer.cornerRadius = 5
            cellmsg.avatar.isUserInteractionEnabled = true
            cellmsg.avatar.tag = indexPath.row
            cellmsg.content_html.text = self.data[indexPath.row].content
            cellmsg.content_html.customize({ (label) in
                label.handleMentionTap({ (mention) in
                    var mention = mention
                    if mention.hasPrefix("@") {
                        mention.removeFirst()
                    }
                    print(mention)
                })
                label.handleURLTap({ (url) in
                    if !url.absoluteString.isValidHttpsUrl {
                        let alert =  MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
                        alert.configureContent(title: "Erreur", body: "URL: \(url.absoluteString) n'est pas sûr !")
                        alert.configureTheme(.warning)
                        alert.button?.isHidden = true
                        var config = SwiftMessages.defaultConfig
                        config.duration = .seconds(seconds: 3)
                        SwiftMessages.show(config: config, view: alert)
                        return
                    }
                    if url.host == domain_name {
                        let desti = parse_dir(path: url.absoluteString)
                        if desti.isMember(of: UIViewController.self)  { return }
                        self.navigationController?.pushViewController(desti)
                        return
                    }
                    if url.host != domain_name {
                        let al_web = PTPopupWebViewController()
                        al_web.popupView.URL(string: url.absoluteString)
                        al_web.show()
                    }
                })
            })
            if indexPath.row + 1 == self.data.count || indexPath.row + 2 == self.data.count {
                self.scroll_auto = true
            }else {
                self.scroll_auto = false
            }
            cellmsg.pseudo_date.isUserInteractionEnabled = true
            cellmsg.pseudo_date.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onpseudo))
            cellmsg.pseudo_date.addGestureRecognizer(tap)
            return cellmsg
        } else {
            cell.textLabel?.text = autocompleteUsers[indexPath.row]
        }
        return cell
    }
    
    @objc func onpseudo(gesture: UITapGestureRecognizer) {
        if let sender = gesture.view as? UILabel {
            user_id = String(self.data[sender.tag].id_author)
            let next:ProfileViewController = self.parent?.storyboard?.instantiateViewController(withIdentifier: "gotoprrofil") as! ProfileViewController
            self.parent?.navigationController?.pushViewController(next, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.messageView.text = "\(self.messageView.text)@\(self.data[indexPath.row].author)"
    }

    func didFind(controller: MessageAutocompleteController, prefix: String, word: String) {
        
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    
}
