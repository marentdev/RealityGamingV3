//
//  AlertsViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 21/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSoup
import SwifterSwift
import SwiftMessages
import PullToRefreshKit
class AlertsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    enum direction {
        case trophies
        case profile_post
        case post
        case conv_shout
        case thread
        case mp
        case none
    }
    
    struct alert {
        var dir:direction = .none
        var content:String = ""
        var tagged:[String] = []
        var id:Int = 0
        var time:String = ""
        var id_dir:Int = 0
    }
    
    var tableView:UITableView = UITableView()
    var alerts:[alert] = []
    var current:Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height + self.navigationController!.navigationBar.height
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height - barHeight))
        self.tableView.estimatedRowHeight = 96
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.view.addSubview(self.tableView)
        self.navigationItem.title = "Alertes"
        self.tableView.configRefreshFooter(with: DefaultRefreshFooter.footer()) {
            self.current += 1
            self.nextPage(index: self.current)
        }
        self.setupPage()
    }
    
    func nextPage(index:Int) {
        var temp:[alert] = []
        session.request(URL(string: "https://\(domain_name)/compte/alerts?page=\(index)&_xfResponseType=json&_xfToken=\(_xfToken)")!).validate().responseJSON { (datarep) in
            let json:JSON = JSON(datarep.result.value!)
            do {
                if json["status"].string! == "ok" {
                    let document:Document = try SwiftSoup.parse(json["html"]["content"].string!)
                    let alerts:Elements = try document.getElementsByAttribute("data-alert-id")
                    try alerts.forEach({ (elem) in
                        var stck_alert:alert = alert()
                        stck_alert.time = try elem.getElementsByClass("contentRow-minor").get(0).text()
                        stck_alert.content = try elem.getElementsByClass("contentRow-main").get(0).text().remove(text: [stck_alert.time])
                        if let id = Int((elem.getAttributes()?.get(key: "data-alert-id"))!) {
                            stck_alert.id = id
                        }else {
                            stck_alert.id = -1
                        }
                        let links = try elem.getElementsByClass("contentRow-main").get(0).getElementsByTag("a")
                        if links.array().count > 1 {
                            stck_alert.tagged.append(try links.get(0).text())
                            let link:String = (links.get(1).getAttributes()?.get(key: "href"))!
                            link.components(separatedBy: "/").forEach({ (str) in
                                if let id = Int(str) {
                                    stck_alert.id_dir = id
                                }else if str.contains(".") && str != "realitygaming.fr" {
                                    stck_alert.id_dir = Int(str.components(separatedBy: ".")[1])!
                                }else if str == "posts" {
                                    stck_alert.dir = .post
                                }else if str == "profile-posts" {
                                    stck_alert.dir = .profile_post
                                }else if str == "threads" {
                                    stck_alert.dir = .thread
                                }else if str == "posts" {
                                    stck_alert.dir = .post
                                }else if str == "chat" {
                                    stck_alert.dir = .conv_shout
                                }else if str == "messages" {
                                    stck_alert.dir = .mp
                                }
                            })
                        }else if links.array().count > 0 {
                            if (links.get(0).getAttributes()?.get(key: "class"))!.contains("username") {
                                stck_alert.tagged.append(try links.get(0).text())
                            }else {
                                let link:String = (links.get(0).getAttributes()?.get(key: "href"))!
                                if link.contains("/thread") {
                                    print("TESTEST", link.components(separatedBy: "/"))
                                    stck_alert.id_dir = Int(link.components(separatedBy: "/")[2].components(separatedBy: ".")[1])!
                                }else {
                                    link.components(separatedBy: "/").forEach({ (str) in
                                        if let id = Int(str) {
                                            stck_alert.id_dir = id
                                        }else if str == "trophies" {
                                            stck_alert.dir = .trophies
                                        }
                                    })
                                    
                                }
                            }
                        }
                        self.alerts.append(stck_alert)
                        temp.append(stck_alert)
                    })
                    if temp.count < 1 {
                        self.tableView.switchRefreshFooter(to: .noMoreData)
                    }else {
                        self.tableView.reloadData()
                        self.tableView.scrollToRow(at: IndexPath(row: self.alerts.count - 1 - temp.count, section: 0), at: .bottom, animated: true)
                        self.tableView.switchRefreshFooter(to: .normal)
                    }
                }else {
                    //error
                }
            }catch Exception.Error(let type, let message) {
                print(type, message)
            } catch {
                print("error")
            }
        }
    }
    
    func setupPage() {
        alerts = []
        session.request(URL(string: "https://\(domain_name)/compte/alerts?_xfResponseType=json&_xfToken=\(_xfToken)")!).validate().responseJSON { (datarep) in
            let json:JSON = JSON(datarep.result.value!)
            do {
                if json["status"].string! == "ok" {
                    debugPrint(json)
                    let document:Document = try SwiftSoup.parse(json["html"]["content"].string!)
                    let alerts:Elements = try document.getElementsByAttribute("data-alert-id")
                    try alerts.forEach({ (elem) in
                        var stck_alert:alert = alert()
                        stck_alert.time = try elem.getElementsByClass("contentRow-minor").get(0).text()
                        stck_alert.content = try elem.getElementsByClass("contentRow-main").get(0).text().remove(text: [stck_alert.time])
                        if let id = Int((elem.getAttributes()?.get(key: "data-alert-id"))!) {
                            stck_alert.id = id
                        }else {
                            stck_alert.id = -1
                        }
                        let links = try elem.getElementsByClass("contentRow-main").get(0).getElementsByTag("a")
                        if links.array().count > 1 {
                            stck_alert.tagged.append(try links.get(0).text())
                            let link:String = (links.get(1).getAttributes()?.get(key: "href"))!
                            link.components(separatedBy: "/").forEach({ (str) in
                                if let id = Int(str) {
                                    stck_alert.id_dir = id
                                }else if str.contains(".") && str != "realitygaming.fr" {
                                    stck_alert.id_dir = Int(str.components(separatedBy: ".")[1])!
                                }else if str == "posts" {
                                    stck_alert.dir = .post
                                }else if str == "profile-posts" {
                                    stck_alert.dir = .profile_post
                                }else if str == "threads" {
                                    stck_alert.dir = .thread
                                }else if str == "posts" {
                                    stck_alert.dir = .post
                                }else if str == "chat" {
                                    stck_alert.dir = .conv_shout
                                }else if str == "messages" {
                                    stck_alert.dir = .mp
                                }
                            })
                        }else if links.array().count > 0 {
                            if (links.get(0).getAttributes()?.get(key: "class"))!.contains("username") {
                                stck_alert.tagged.append(try links.get(0).text())
                            }else {
                                let link:String = (links.get(0).getAttributes()?.get(key: "href"))!
                                    link.components(separatedBy: "/").forEach({ (str) in
                                        if let id = Int(str) {
                                            stck_alert.id_dir = id
                                        }else if str == "trophies" {
                                            stck_alert.dir = .trophies
                                        }
                                    })
                            }
                        }
                        debugPrint(stck_alert)
                        self.alerts.append(stck_alert)
                    })
                    debugPrint(self.alerts)
                    self.tableView.reloadData()
                }else {
                    //error
                }
            }catch Exception.Error(let type, let message) {
                print(type, message)
            } catch {
                print("error")
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.alerts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "AlertTableViewCell", bundle: nil), forCellReuseIdentifier: "alert")
        let cell = tableView.dequeueReusableCell(withIdentifier: "alert", for: indexPath) as! AlertTableViewCell
        cell.content.text = self.alerts[indexPath.row].content
        cell.avatar.layer.masksToBounds = true
        cell.avatar.layer.cornerRadius = cell.avatar.height / 2
        if self.alerts[indexPath.row].tagged.count > 0 {
            getLinkAvatarFormPseudo(username: self.alerts[indexPath.row].tagged[0], imageView: cell.avatar)
        }else {
            cell.avatar.image = #imageLiteral(resourceName: "noavatar")
        }
        cell.date.text = self.alerts[indexPath.row].time
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch self.alerts[indexPath.row].dir {
        case .mp:
            break
        case .trophies:
            break
        case .profile_post:
            break
        case .post:
            is_post = true
            topic_id = String(self.alerts[indexPath.row].id_dir)
            let next:WebViewTopicViewController = self.storyboard?.instantiateViewController(withIdentifier: "topicgo") as! WebViewTopicViewController
            self.navigationController?.pushViewController(next, animated: true)
            break
        case .conv_shout:
            break
        case .thread:
            topic_id = String(self.alerts[indexPath.row].id_dir)
            let next:WebViewTopicViewController = self.storyboard?.instantiateViewController(withIdentifier: "topicgo") as! WebViewTopicViewController
            self.navigationController?.pushViewController(next, animated: true)
            break
        default:
            break;
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > scrollView.contentSize.height {
            print("yo")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
