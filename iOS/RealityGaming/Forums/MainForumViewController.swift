//
//  MainForumViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 18/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import Alamofire
import SideMenu
import SwiftSoup
import SwifterSwift
class MainForumViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    struct forum {
        var name:String = ""
        var id:Int = 0
        var nb_discussions:Int = 0
        var nb_messages:Int = 0
        var last_activity:String = ""
        var isforum:Bool = false
    }
    
    struct category {
        var name:String = ""
        var desc:String = ""
        var forums:[forum] = []
    }
    
    var mainforum:[category] = []
    var tableView:UITableView = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        section_active = [false, true, false]
        self.showWaitOverlayWithText("Loading...")
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        self.navigationController?.viewControllers = [self]
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height + self.navigationController!.navigationBar.height
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height - barHeight))
        self.tableView.estimatedRowHeight = 96
        self.tableView.estimatedSectionHeaderHeight = 50
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.isHidden = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.view.addSubview(self.tableView)
        self.navigationItem.title = "Forums"
        let btn = UIBarButtonItem(image: UIImage.fontAwesomeIcon(name: "", textColor: .black, size: CGSize(width: 30, height: 35)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openMenu))
        btn.tintColor = .black
        self.navigationItem.leftBarButtonItems = [btn]
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.setupPage()
    }
    
    @objc func openMenu() {
        self.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupPage() {
        session.request(URL(string: "https://\(domain_name)/index.php")!).validate().responseString { (datarep) in
            do {
                let document:Document = try SwiftSoup.parse(datarep.result.value!)
                let blocks:Elements = try document.getElementsByClass("block--category")
                self.mainforum.removeAll()
                try blocks.forEach({ (block) in
                    var stck_cat:category = category()
                    stck_cat.name = try block.getElementsByClass("block-header").get(0).children().eq(0).text()
                    let desc = try block.getElementsByClass("block-desc")
                    if desc.array().count > 0 {
                        stck_cat.desc = try desc.get(0).text()
                    }
                    let forums:Elements = try block.getElementsByClass("node")
                    try forums.forEach({ (elem) in
                        var stck_forum:forum = forum()
                        stck_forum.name = try elem.getElementsByClass("node-title").get(0).text()
                        let data = elem.getAttributes()?.get(key: "class")
                        if let id = Int((GetElement(input: data! as NSString, paternn: "node--id(.*?) ") as String).remove(text: ["node--id", " "])) {
                            stck_forum.id = id
                        }else {
                            stck_forum.id = -1
                        }
                        if (GetElement(input: data! as NSString, paternn: "node--depth2 node--(.*?) node") as String).remove(text: ["node--depth2 node--", " node"]) == "forum" {
                            stck_forum.isforum = true
                        }else {
                            stck_forum.isforum = false
                        }
                        if let nb_disc = Int((try elem.getElementsByClass("pairs").get(0).getElementsByTag("dd").text() as String).remove(text: [" "])) {
                            stck_forum.nb_discussions = nb_disc
                        }else {
                            stck_forum.nb_discussions = -1
                        }
                        if let nb_msg = Int((try elem.getElementsByClass("pairs").get(1).getElementsByTag("dd").text() as String).remove(text: [" "])) {
                            stck_forum.nb_messages = nb_msg
                        }else {
                            stck_forum.nb_messages = -1
                        }
                        stck_forum.last_activity = try elem.getElementsByClass("node-extra-date").text()
                        stck_cat.forums.append(stck_forum)
                    })
                    self.mainforum.append(stck_cat)
                })
                self.tableView.reloadData()
                self.tableView.isHidden = false
            }
            catch Exception.Error(let type, let message) {
                print(type, message)
            } catch {
                print("error")
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mainforum.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mainforum[section].forums.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        tableView.register(UINib(nibName: "HeaderForumTableViewCell", bundle: nil), forCellReuseIdentifier: "header")
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderForumTableViewCell
        cell.title.text = self.mainforum[section].name
        cell.backgroundColor = #colorLiteral(red: 0.7422102094, green: 0.764362216, blue: 0.7821244597, alpha: 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "SousForumTableViewCell", bundle: nil), forCellReuseIdentifier: "forum")
        let cell = tableView.dequeueReusableCell(withIdentifier: "forum", for: indexPath) as! SousForumTableViewCell //Is like SousForum
        let forum = self.mainforum[indexPath.section].forums[indexPath.row]
        cell.title.text = forum.name
        cell.nb_discussions.text = forum.nb_discussions.formattedWithSeparator
        cell.nb_messages.text = forum.nb_messages.formattedWithSeparator
        cell.last_activity.text = forum.last_activity
        if UIDevice.current.modelName == "iPhone SE" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "Simulator" {
            cell.last_activity.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.mainforum[indexPath.section].forums[indexPath.row].isforum {
            is_forum = true
            forum_name_now = self.mainforum[indexPath.section].forums[indexPath.row].name
            forum_id = String(self.mainforum[indexPath.section].forums[indexPath.row].id)
            let next:NodeForumViewController = self.storyboard?.instantiateViewController(withIdentifier: "gotonode") as! NodeForumViewController
            self.navigationController?.pushViewController(next, animated: true)
        }else {
            is_forum = false
            forum_name_now = self.mainforum[indexPath.section].forums[indexPath.row].name
            forum_id = String(self.mainforum[indexPath.section].forums[indexPath.row].id)
            let next:NodeForumViewController = self.storyboard?.instantiateViewController(withIdentifier: "gotonode") as! NodeForumViewController
            self.navigationController?.pushViewController(next, animated: true)
        }
    }

}
