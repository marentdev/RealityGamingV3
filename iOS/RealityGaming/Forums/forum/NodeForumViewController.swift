//
//  NodeForumViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 17/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import SwiftSoup
import Alamofire
import SwiftyJSON
import SwifterSwift
import SideMenu
import DropDown
import PullToRefreshKit
import SwiftMessages
var forum_id:String = ""
var forum_name_now:String = ""
var is_forum:Bool = true
class NodeForumViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerPreviewingDelegate {

    struct node {
        var name:String = ""
        var image:UIImage = UIImage()
        var nb_discussions:Int = 0
        var nb_messages:Int = 0
        var id:Int = 0
        var last_activity:String = ""
        var isforum:Bool = true
    }
    
    struct topic {
        var name:String = ""
        var id:Int = 0
        var author:String = ""
        var tag:String = ""
        var nb_rep:Int = 0
        var nb_view:Int = 0
        var last_activity:String = ""
    }
    
    struct forum {
        var nodes:[node] = []
        var topics:[topic] = []
        var sticky:[topic] = []
        var isfollow:Bool = false
    }
    
    var tableView:UITableView = UITableView()
    var theforum:forum = forum()
    var current:Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        section_active = [false, false, false]
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height + self.navigationController!.navigationBar.height
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height - barHeight))
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isHidden = true
        self.tableView.estimatedRowHeight = 96
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.view.addSubview(self.tableView)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = forum_name_now
        self.showWaitOverlayWithText("Loading...")
        self.tableView.isHidden = true
        if is_forum {
            let btn = UIBarButtonItem(image: UIImage.fontAwesomeIcon(name: "", textColor: .black, size: CGSize(width: 25, height: 25)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openMenu))
            btn.tintColor = .black
            self.navigationItem.rightBarButtonItems = [btn]
            self.setupPage(link: "https://\(domain_name)/forums/\(forum_id)")
        }else {
            self.setupPage(link: "https://\(domain_name)/categories/\(forum_id)")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = tableView.indexPathForRow(at: location) else {
            return nil
        }
        if indexPath.section == 0 && theforum.nodes.count > 0 {
            return nil
        }
        if (indexPath.section == 0 && theforum.nodes.count <= 0 && theforum.sticky.count > 0) || (indexPath.section == 1 && theforum.nodes.count > 0 && theforum.sticky.count > 0) {
            topic_id = String(self.theforum.sticky[indexPath.row].id)
        }else {
            topic_id = String(self.theforum.topics[indexPath.row].id)
        }
        let next:TopicViewViewController = self.storyboard?.instantiateViewController(withIdentifier: "topicgo") as! TopicViewViewController
        return next
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        navigationController?.pushViewController(viewControllerToCommit)
    }
    
    @objc func openMenu() {
        let drop:DropDown = DropDown()
        drop.dataSource = ["Créer topic", "Marquer comme lu", self.theforum.isfollow ? "Ne plus suivre" : "Suivre"]
        drop.anchorView = self.navigationItem.rightBarButtonItems![0]
        drop.bottomOffset = CGPoint(x: 0, y:(drop.anchorView?.plainView.bounds.height)!)
        drop.selectionAction = { (index, str) in
            if index == 0 {
                session.request(URL(string: "https://\(domain_name)/forums/\(forum_id)/post-thread")!).responseString(completionHandler: { (datarep) in
                    print(datarep.response?.statusCode)
                    if datarep.response?.statusCode == 404 || datarep.response?.statusCode == 403 {
                        let alert =  MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
                        alert.configureContent(title: "Erreur", body: "Vous ne pouvez poster aucun topic ici !")
                        alert.configureTheme(.warning)
                        alert.button?.isHidden = true
                        var config = SwiftMessages.defaultConfig
                        config.duration = .seconds(seconds: 3)
                        SwiftMessages.show(config: config, view: alert)
                    }else {
                        self.performSegue(withIdentifier: "createtopic", sender: self)
                    }
                })
            }else if index == 1 {
                session.request(URL(string: "https://\(domain_name)/forums/\(forum_id)/mark-read?date=\("\(Date().timeIntervalSince1970)".components(separatedBy: ".")[0])")!, method: .post, parameters: ["_xfToken": _xfToken, "_xfResponseType": "json"]).validate().responseJSON(completionHandler: { (datarep) in
                    let json = JSON(datarep.result.value!)
                    if json["status"].string! == "ok" {
                        return
                    }else {
                        //error
                    }
                })
            }else if index == 2 && self.theforum.isfollow == false {
                session.request(URL(string: "https://\(domain_name)/forums/\(forum_id)/watch")!, method: .post, parameters: ["_xfToken": _xfToken, "_xfResponseType": "json", "notify": "thread", "send_alert": 1]).validate().responseJSON(completionHandler: { (datarep) in
                    let json = JSON(datarep.result.value!)
                    if json["status"].string! == "ok" {
                        self.theforum.isfollow = true
                        return
                    }else {
                        //error
                    }
                })
            }else if index == 2 && self.theforum.isfollow {
                session.request(URL(string: "https://\(domain_name)/forums/\(forum_id)/watch")!, method: .post, parameters: ["_xfToken": _xfToken, "_xfResponseType": "json", "stop": 1]).validate().responseJSON(completionHandler: { (datarep) in
                    let json = JSON(datarep.result.value!)
                    if json["status"].string! == "ok" {
                        self.theforum.isfollow = false
                        return
                    }else {
                        //error
                    }
                })
            }
        }
        drop.show()
    }
    
    func setupPage(link:String) {
        theforum = forum()
        current = 1
        session.request(URL(string: link)!).validate().responseString { (datarep) in
            do {
                let document:Document = try SwiftSoup.parse(datarep.result.value!)
                let nodes:Elements = try document.getElementsByClass("node")
                if is_forum {
                    let iswatch = try document.getElementsByAttribute("data-sk-watch").get(0).text()
                    if iswatch == "Suivre" {
                        self.theforum.isfollow = false
                    }else {
                        self.theforum.isfollow = true
                    }
                }
               try nodes.forEach({ (elem) in
                var stck_node:node = node()
                    if let nb_disc = Int((try elem.getElementsByClass("pairs").get(0).getElementsByTag("dd").text() as String).remove(text: [" "])) {
                        stck_node.nb_discussions = nb_disc
                    }else {
                        stck_node.nb_discussions = 0
                    }
                if let nb_msg = Int((try elem.getElementsByClass("pairs").get(1).getElementsByTag("dd").text() as String).remove(text: [" "])) {
                        stck_node.nb_messages = nb_msg
                    }else {
                        stck_node.nb_messages = 0
                    }
                stck_node.name = try elem.getElementsByAttributeValue("data-shortcut", "node-description").get(0).text()
                let data = elem.getAttributes()?.get(key: "class")
                if let id = Int((GetElement(input: data! as NSString, paternn: "node--id(.*?) ") as String).remove(text: ["node--id", " "])) {
                    stck_node.id = id
                }else {
                    stck_node.id = -1
                }
                if (GetElement(input: data! as NSString, paternn: "node--depth2 node--(.*?) node") as String).remove(text: ["node--depth2 node--", " node"]) == "forum" {
                    stck_node.isforum = true
                }else {
                    stck_node.isforum = false
                }
                stck_node.last_activity = try elem.getElementsByClass("node-extra-date").text()
                    self.theforum.nodes.append(stck_node)
                })
                let sticky_cont:Elements = try document.getElementsByClass("XenStickyBg")
                if sticky_cont.array().count > 0 {
                    try sticky_cont.get(0).getElementsByClass("structItem").forEach({ (elem) in
                        var stck_topic:topic = topic()
                        stck_topic.author = (elem.getAttributes()?.get(key: "data-author"))!
                        let tag = try elem.getElementsByClass("labelLink")
                        if tag.array().count > 0 {
                            stck_topic.tag = try tag.get(0).text()
                        }
                        stck_topic.name = (try elem.getElementsByClass("structItem-title").get(0).text()).stringByReplacingFirstOccurrenceOfString(target: stck_topic.tag.isEmpty ? "" : "\(stck_topic.tag) ", withString: "")
                        if try elem.getElementsByClass("structItem-latestDate").array().count > 0 {
                            stck_topic.last_activity = try elem.getElementsByClass("structItem-latestDate").get(0).text()
                        }else {
                            stck_topic.last_activity = "N/A"
                        }
                        if let id = Int((GetElement(input: elem.getAttributes()?.get(key: "class") as! NSString, paternn: "js-threadListItem-(.*)") as String).remove(text: ["js-threadListItem-"])) {
                            stck_topic.id = id
                        }else {
                            stck_topic.id = -1
                        }
                        if let nb_msg = Int((try elem.getElementsByClass("pairs").get(0).getElementsByTag("dd").text() as String).remove(text: [" "])) {
                            stck_topic.nb_rep = nb_msg
                        }else {
                            stck_topic.nb_rep = 0
                        }
                        self.theforum.sticky.append(stck_topic)
                    })
                }
                let norm_topic:Elements = try document.getElementsByClass("structItemContainer-group js-threadList")
                if norm_topic.array().count > 0 {
                    try norm_topic.get(0).getElementsByClass("structItem").forEach({ (elem) in
                            var stck_topic:topic = topic()
                            stck_topic.author = (elem.getAttributes()?.get(key: "data-author"))!
                            stck_topic.name = try elem.getElementsByClass("structItem-title").get(0).text()
                        let tag = try elem.getElementsByClass("labelLink")
                        if tag.array().count > 0 {
                            stck_topic.tag = try tag.get(0).text()
                        }
                        stck_topic.name = (try elem.getElementsByClass("structItem-title").get(0).text()).stringByReplacingFirstOccurrenceOfString(target: stck_topic.tag.isEmpty ? "" : "\(stck_topic.tag) ", withString: "")
                            if try elem.getElementsByClass("structItem-latestDate").array().count > 0 {
                                stck_topic.last_activity = try elem.getElementsByClass("structItem-latestDate").get(0).text()
                            }else {
                                stck_topic.last_activity = "N/A"
                            }
                        if let id = Int((GetElement(input: elem.getAttributes()?.get(key: "class") as! NSString, paternn: "js-threadListItem-(.*)") as String).remove(text: ["js-threadListItem-"])) {
                                stck_topic.id = id
                            }else {
                                stck_topic.id = -1
                            }
                            if let nb_msg = Int((try elem.getElementsByClass("pairs").get(0).getElementsByTag("dd").text() as String).remove(text: [" "])) {
                                stck_topic.nb_rep = nb_msg
                            }else {
                                stck_topic.nb_rep = 0
                            }
                            self.theforum.topics.append(stck_topic)
                        })
                }
                if self.theforum.topics.count == 25 {
                    self.tableView.configRefreshFooter(with: DefaultRefreshFooter.footer()) {
                        self.current += 1
                        self.nextPage(index: self.current)
                    }
                }else {
                    self.tableView.switchRefreshFooter(to: .noMoreData)
                }
                self.tableView.reloadData()
                self.tableView.isHidden = false
                self.removeAllOverlays()
            } catch Exception.Error(let type, let message) {
                print(type, message)
            } catch {
                print("error")
            }
        }
    }
    
    func nextPage(index:Int) {
        var temp:[topic] = []
        session.request(URL(string: "https://\(domain_name)/forums/\(forum_id)/page-\(index)")!).validate().responseString { (datarep) in
            do {
                let document:Document = try SwiftSoup.parse(datarep.result.value!)
                let iswatch = try document.getElementsByAttribute("data-sk-watch").get(0).text()
                if iswatch == "Suivre" {
                    self.theforum.isfollow = false
                }else {
                    self.theforum.isfollow = true
                }
                let norm_topic:Elements = try document.getElementsByClass("structItemContainer-group js-threadList")
                if norm_topic.array().count > 0 {
                    try norm_topic.get(0).getElementsByClass("structItem").forEach({ (elem) in
                        var stck_topic:topic = topic()
                        stck_topic.author = (elem.getAttributes()?.get(key: "data-author"))!
                        stck_topic.name = try elem.getElementsByClass("structItem-title").get(0).text()
                        let tag = try elem.getElementsByClass("labelLink")
                        if tag.array().count > 0 {
                            stck_topic.tag = try tag.get(0).text()
                        }
                        stck_topic.name = (try elem.getElementsByClass("structItem-title").get(0).text()).stringByReplacingFirstOccurrenceOfString(target: stck_topic.tag.isEmpty ? "" : "\(stck_topic.tag) ", withString: "")
                        if try elem.getElementsByClass("structItem-latestDate").array().count > 0 {
                            stck_topic.last_activity = try elem.getElementsByClass("structItem-latestDate").get(0).text()
                        }else {
                            stck_topic.last_activity = "N/A"
                        }
                        if let id = Int((GetElement(input: elem.getAttributes()?.get(key: "class") as! NSString, paternn: "js-threadListItem-(.*)") as String).remove(text: ["js-threadListItem-"])) {
                            stck_topic.id = id
                        }else {
                            stck_topic.id = -1
                        }
                        if let nb_msg = Int((try elem.getElementsByClass("pairs").get(0).getElementsByTag("dd").text() as String).remove(text: [" "])) {
                            stck_topic.nb_rep = nb_msg
                        }else {
                            stck_topic.nb_rep = 0
                        }
                        if Int((datarep.response?.url?.absoluteString.components(separatedBy: "-").last!)!) == self.current - 1 {
                            return
                        }else {
                            temp.append(stck_topic)
                            self.theforum.topics.append(stck_topic)
                        }
                    })
                    if Int((datarep.response?.url?.absoluteString.components(separatedBy: "-").last!)!) == self.current - 1 {
                        self.tableView.switchRefreshFooter(to: .noMoreData)
                    }else {
                        self.tableView.reloadData()
                        self.tableView.scrollToRow(at: IndexPath(row: self.theforum.topics.count - 1 - temp.count, section: self.tableView.numberOfSections - 1), at: .bottom, animated: true)
                        self.tableView.switchRefreshFooter(to: .normal)
                    }
                }
            } catch Exception.Error(let type, let message) {
                print(type, message)
            } catch {
                print("error")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (theforum.nodes.count > 0 ? 1 : 0) + (theforum.sticky.count > 0 ? 1 : 0) + (theforum.topics.count > 0 ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0 && theforum.nodes.count > 0) {
            return "Sous-forum"
        }else if (section == 0 && theforum.nodes.count <= 0 && theforum.sticky.count > 0) || (section == 1 && theforum.nodes.count > 0 && theforum.sticky.count > 0) {
            return "Sujets épinglés"
        }else {
            return "Sujets normaux"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0 && theforum.nodes.count > 0) {
            return theforum.nodes.count
        }else if (section == 0 && theforum.nodes.count <= 0 && theforum.sticky.count > 0) || (section == 1 && theforum.nodes.count > 0 && theforum.sticky.count > 0) {
            return theforum.sticky.count
        }else {
            return theforum.topics.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "TopicForumTableViewCell", bundle: nil), forCellReuseIdentifier: "topic")
        tableView.register(UINib(nibName: "SousForumTableViewCell", bundle: nil), forCellReuseIdentifier: "forum")
        if (indexPath.section == 0 && theforum.nodes.count > 0) {
            let forum = tableView.dequeueReusableCell(withIdentifier: "forum", for: indexPath) as! SousForumTableViewCell
            forum.avatar.layer.masksToBounds = true
            forum.avatar.layer.cornerRadius = forum.avatar.height / 2
            forum.title.text = self.theforum.nodes[indexPath.row].name
            forum.nb_discussions.text = self.theforum.nodes[indexPath.row].nb_discussions.formattedWithSeparator
            forum.nb_messages.text = self.theforum.nodes[indexPath.row].nb_messages.formattedWithSeparator
            forum.last_activity.text = self.theforum.nodes[indexPath.row].last_activity
            if UIDevice.current.modelName == "iPhone SE" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "iPhone 5" {
                forum.last_activity.isHidden = true
            }
            return forum
        }else if (indexPath.section == 0 && theforum.nodes.count <= 0 && theforum.sticky.count > 0) || (indexPath.section == 1 && theforum.nodes.count > 0 && theforum.sticky.count > 0) {
            let topic = tableView.dequeueReusableCell(withIdentifier: "topic", for: indexPath) as! TopicForumTableViewCell
            topic.avatar.layer.masksToBounds = true
            topic.avatar.layer.cornerRadius = topic.avatar.height / 2
            topic.title.text = self.theforum.sticky[indexPath.row].name
            topic.nb_reponses.text = self.theforum.sticky[indexPath.row].nb_rep.formattedWithSeparator
            topic.author.text = self.theforum.sticky[indexPath.row].author
            topic.last_activity.text = self.theforum.sticky[indexPath.row].last_activity
            getLinkAvatarFormPseudo(username: self.theforum.sticky[indexPath.row].author, imageView: topic.avatar)
            if UIDevice.current.modelName == "iPhone SE" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "iPhone 5"{
                topic.last_activity.isHidden = true
            }
            return topic
        }else {
            let topic = tableView.dequeueReusableCell(withIdentifier: "topic", for: indexPath) as! TopicForumTableViewCell
            topic.avatar.layer.masksToBounds = true
            topic.avatar.layer.cornerRadius = topic.avatar.height / 2
            topic.title.text = self.theforum.topics[indexPath.row].name
            topic.nb_reponses.text = self.theforum.topics[indexPath.row].nb_rep.formattedWithSeparator
            topic.author.text = self.theforum.topics[indexPath.row].author
            topic.last_activity.text = self.theforum.topics[indexPath.row].last_activity
            getLinkAvatarFormPseudo(username: self.theforum.topics[indexPath.row].author, imageView: topic.avatar)
            if UIDevice.current.modelName == "iPhone SE" || UIDevice.current.modelName == "iPhone 5c" || UIDevice.current.modelName == "iPhone 5s" || UIDevice.current.modelName == "iPhone 5" {
                topic.last_activity.isHidden = true
            }
            return topic
        }
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (indexPath.section == 0 && theforum.nodes.count > 0) {
            //forum
            if self.theforum.nodes[indexPath.row].isforum {
                is_forum = true
                forum_name_now = self.theforum.nodes[indexPath.row].name
                forum_id = String(self.theforum.nodes[indexPath.row].id)
                let next:NodeForumViewController = self.storyboard?.instantiateViewController(withIdentifier: "gotonode") as! NodeForumViewController
                self.navigationController?.pushViewController(next, animated: true)
            }else {
                is_forum = false
                forum_name_now = self.theforum.nodes[indexPath.row].name
                forum_id = String(self.theforum.nodes[indexPath.row].id)
                let next:NodeForumViewController = self.storyboard?.instantiateViewController(withIdentifier: "gotonode") as! NodeForumViewController
                self.navigationController?.pushViewController(next, animated: true)
            }
        }else if (indexPath.section == 0 && theforum.nodes.count <= 0 && theforum.sticky.count > 0) || (indexPath.section == 1 && theforum.nodes.count > 0 && theforum.sticky.count > 0) {
            //topic
            topic_id = String(self.theforum.sticky[indexPath.row].id)
            let next:WebViewTopicViewController = self.storyboard?.instantiateViewController(withIdentifier: "topicgo") as! WebViewTopicViewController
            self.navigationController?.pushViewController(next, animated: true)
        }else {
            //topic
            topic_id = String(self.theforum.topics[indexPath.row].id)
            let next:WebViewTopicViewController = self.storyboard?.instantiateViewController(withIdentifier: "topicgo") as! WebViewTopicViewController
            self.navigationController?.pushViewController(next, animated: true)
        }
    }
}
