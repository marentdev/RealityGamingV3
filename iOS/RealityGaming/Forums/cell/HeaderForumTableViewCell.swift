//
//  HeaderForumTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 18/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class HeaderForumTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
