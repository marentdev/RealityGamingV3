//
//  SousForumTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 18/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class SousForumTableViewCell: UITableViewCell {

    @IBOutlet weak var nb_messages: UILabel!
    @IBOutlet weak var nb_discussions: UILabel!
    @IBOutlet weak var last_activity: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
