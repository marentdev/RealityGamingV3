//
//  TopicForumTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 17/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class TopicForumTableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var title: UILabel!

    @IBOutlet weak var nb_reponses: UILabel!
    @IBOutlet weak var last_activity: UILabel!

    @IBOutlet weak var author: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
