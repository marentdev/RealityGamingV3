//
//  ConnexionController.swift
//  RealityGaming
//
//  Created by Marentdev on 05/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import SwifterSwift
import Alamofire
import SwiftMessages
import RSLoadingView
import SwiftSoup
import SwiftyJSON
import FCAlertView
import XLPagerTabStrip
import LocalAuthentication
import SideMenu
class ConnexionController: UIViewController {

    @IBOutlet weak var connexionB: UIButton!
    @IBOutlet weak var mdpT: UITextField!
    @IBOutlet weak var pseudoT: UITextField!
    @IBOutlet weak var pseudoL: UILabel!
    @IBOutlet weak var mdpL: UILabel!
    @IBOutlet weak var noaccount: UILabel!
    
    @IBOutlet weak var keyboardoncstrainte: NSLayoutConstraint!
    
    let alert =  MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
    var config = SwiftMessages.defaultConfig
    var tempconst:CGFloat?
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        tempconst = self.keyboardoncstrainte?.constant
        self.connexionB.dropShadow(color: UIColor(hex: 0x0B74C4), offSet: CGSize(width: 0, height: 2))
        let attributesT = [
            NSAttributedStringKey.font : UIFont(name: "Avenir-Medium", size: 17)!
        ]
        self.mdpT.attributedPlaceholder = NSAttributedString(string: "●●●●●", attributes: attributesT)
        self.pseudoT.attributedPlaceholder = NSAttributedString(string: "bot@realitygaming.fr", attributes: attributesT)
        self.noaccount.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.back))
        self.noaccount.addGestureRecognizer(tap)
        /* Pseudo Icon */
        let attch_pseudo = NSTextAttachment()
        attch_pseudo.image = #imageLiteral(resourceName: "PseudoText")
        let pseudo_attchstr = NSAttributedString(attachment: attch_pseudo)
        let pseudo_str = NSMutableAttributedString()
        pseudo_str.append(pseudo_attchstr)
        pseudo_str.append(NSAttributedString(string: " PSEUDONYME", attributes:  [NSAttributedStringKey.font : UIFont(name: "Avenir-Medium", size: 16)!, NSAttributedStringKey.foregroundColor: UIColor.white]))
        self.pseudoL.attributedText = pseudo_str
        /* MDP Icon */
        let attch_mdp = NSTextAttachment()
        attch_mdp.image = #imageLiteral(resourceName: "MDPText")
        let mdp_attchstr = NSAttributedString(attachment: attch_mdp)
        let mdp_str = NSMutableAttributedString()
        mdp_str.append(mdp_attchstr)
        mdp_str.append(NSAttributedString(string: " MOT DE PASSE", attributes:  [NSAttributedStringKey.font : UIFont(name: "Avenir-Medium", size: 16)!, NSAttributedStringKey.foregroundColor: UIColor.white]))
        self.mdpL.attributedText = mdp_str
        /* Border Bottom */
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(hex: 0x1079C8).cgColor
        border.frame = CGRect(x: 0, y: self.mdpT.frame.size.height + 2.5, width:  self.mdpT.frame.size.width + 2.5, height: self.mdpT.frame.size.height)
        border.borderWidth = width
        self.mdpT.layer.addSublayer(border)
        self.mdpT.layer.masksToBounds = true
        let border2 = CALayer()
        border2.borderColor = UIColor(hex: 0x1079C8).cgColor
        border2.frame = CGRect(x: 0, y: self.pseudoT.frame.size.height + 2.5, width:  self.pseudoT.frame.size.width + 2.5, height: self.pseudoT.frame.size.height)
        border2.borderWidth = width
        self.pseudoT.layer.addSublayer(border2)
        self.pseudoT.layer.masksToBounds = true
        /* Fake Appify
        headers["device"] = "ios"
        headers["appify"] = "true"
        headers["pushToken"] = "ExponentPushToken[undefined]"
        headers["uniquedeviceidentifier"] = "undefined"
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = headers
        configuration.httpCookieStorage?.setCookies(HTTPCookie.cookies(withResponseHeaderFields: ["device" : "ios", "isappify" : "true", "pushToken" : "ExponentPushToken[undefined]", "uniquedeviceidentifier" : "undefined"], for: URL(string: "https://\(domain_name)")!), for: URL(string: "https://\(domain_name)")!, mainDocumentURL: nil)
        session = SessionManager(configuration: configuration)*/
        headers["User-Agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36"
        headers["Accept-Language"] = "fr-FR,fr;q=0.9,en;q=0.8"
        headers["Accept-Encoding"] = "gzip, deflate, br"
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = headers
        session = SessionManager(configuration: configuration)
        let next = self.storyboard?.instantiateViewController(withIdentifier: "menuside") as! SideMenuViewController
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: next)
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuShadowRadius = 3
        SideMenuManager.default.menuWidth = 260
        /* Connexion Auto */
        let authenticationContext = LAContext()
        var error:NSError?
        if !(save.string(forKey: "pseudo") == nil) && !(save.string(forKey: "password") == nil) {
            if !save.string(forKey: "pseudo")!.isEmpty && !save.string(forKey: "password")!.isEmpty {
                if save.bool(forKey: "touchid") {
                    guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
                        save.set(self.pseudoT.text!, forKey: "pseudo")
                        save.set(self.mdpT.text!, forKey: "password")
                        save.set(false, forKey: "touchid")
                        self.pseudoT.text = save.string(forKey: "pseudo")!
                        self.mdpT.text = save.string(forKey: "password")!
                        self.connectB(UIButton())
                        return
                    }
                    authenticationContext.evaluatePolicy(
                        .deviceOwnerAuthenticationWithBiometrics,
                        localizedReason: "Connexion to RealityGaming",
                        reply: { [unowned self] (success, error) -> Void in
                            if success {
                                DispatchQueue.main.async(execute: {
                                    self.pseudoT.text = save.string(forKey: "pseudo")!
                                    self.mdpT.text = save.string(forKey: "password")!
                                    self.connectB(UIButton())
                                })
                            }else {
                                if let error = error {
                                    DispatchQueue.main.async(execute: {
                                        let message = self.errorMessageForLAErrorCode(errorCode: error._code)
                                        self.alert.configureContent(title: "Erreur", body: "")
                                        self.alert.configureTheme(.error)
                                        self.alert.button?.isHidden = true
                                        self.config.preferredStatusBarStyle = .lightContent
                                        self.config.duration = .seconds(seconds: 3)
                                        self.alert.configureContent(body: message)
                                        SwiftMessages.show(config: self.config, view: self.alert)
                                        if error._code == LAError.touchIDLockout.rawValue || error._code == LAError.authenticationFailed.rawValue {
                                            save.set(self.pseudoT.text!, forKey: "pseudo")
                                            save.set(self.mdpT.text!, forKey: "password")
                                            save.set(nil, forKey: "touchid")
                                            self.logout()
                                        }
                                    })
                                }
                            }
                    })
                }else {
                    self.pseudoT.text = save.string(forKey: "pseudo")!
                    self.mdpT.text = save.string(forKey: "password")!
                    self.connectB(UIButton())
                }
            }
        }
    }

    func logout() {
        session.request(URL(string: "https://\(domain_name)/")!).validate().responseString { (datarep) in
            do {
                let document:Document = try SwiftSoup.parse(datarep.result.value!)
                _xfToken = try document.getElementsByAttributeValue("name", "_xfToken").attr("value")
                session.request(URL(string: "https://\(domain_name)/logout")!, method: .get, parameters: ["t":_xfToken]).validate().responseString { (response) in
                    do {
                        let document:Document = try SwiftSoup.parse(response.result.value!)
                        if try document.getElementsByClass("p-title-value").eq(0).text().contains("RealityGaming")
                        {
                            save.set(nil, forKey: "pseudo")
                            save.set(nil, forKey: "password")
                            save.set(nil, forKey: "touchid")
                            self.back()
                        }
                    } catch Exception.Error(let type, let message) {
                        print(type, message)
                    } catch {
                        print("error")
                    }
                }
            } catch Exception.Error(let type, let message) {
                print(type, message)
            } catch {
                print("error")
            }
        }
    }
    
    @objc func back() {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardoncstrainte?.constant = tempconst ?? 0.0
            } else {
                self.keyboardoncstrainte?.constant = (endFrame?.size.height ?? 0.0) + 5
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func connectB(_ sender: UIButton) {
        sender.isEnabled = false
        alert.configureContent(title: "Erreur", body: "")
        alert.configureTheme(.error)
        alert.button?.isHidden = true
        config.preferredStatusBarStyle = .lightContent
        config.duration = .seconds(seconds: 3)
        if self.mdpT.isEmpty || self.pseudoT.isEmpty {
            alert.configureContent(body: "Pseudo ou mot de passe vide...")
            SwiftMessages.show(config: config, view: alert)
            sender.isEnabled = true
            return
        }
        connexionRG { (result) in
            if result == -1 {
                save.set(nil, forKey: "pseudo")
                save.set(nil, forKey: "password")
                save.set(nil, forKey: "touchid")
                self.alert.configureContent(body: "Erreur serveur...")
                SwiftMessages.show(config: self.config, view: self.alert)
            }else if result == -2 {
                save.set(nil, forKey: "pseudo")
                save.set(nil, forKey: "password")
                save.set(nil, forKey: "touchid")
                self.alert.configureContent(body: "Mauvais pseudo ou mot de passe")
                SwiftMessages.show(config: self.config, view: self.alert)
            }else if result == -3 {
                save.set(nil, forKey: "pseudo")
                save.set(nil, forKey: "password")
                save.set(nil, forKey: "touchid")
                self.alert.configureContent(body: "Erreur authentification !")
                SwiftMessages.show(config: self.config, view: self.alert)
            }else if result == -4 {
                save.set(nil, forKey: "pseudo")
                save.set(nil, forKey: "password")
                save.set(nil, forKey: "touchid")
                self.alert.configureContent(body: "Erreur inconnue ptdr !")
                SwiftMessages.show(config: self.config, view: self.alert)
            }else if result == 1 {
                self.endlogin(sender)
            }else if result == 2 {
                sender.isEnabled = true
                self.removeAllOverlays()
                let alertC = FCAlertView()
                alertC.colorScheme = #colorLiteral(red: 0.190430969, green: 0.4948838353, blue: 0.7447223663, alpha: 1)
                alertC.addButton("Email", withActionBlock: {
                    alertC.dismiss()
                    let code:UITextField = UITextField()
                    code.keyboardType = .numberPad
                    let alertT = FCAlertView()
                    alertT.colorScheme = #colorLiteral(red: 0.190430969, green: 0.4948838353, blue: 0.7447223663, alpha: 1)
                    alertT.addTextField(withCustomTextField: code, andPlaceholder: "Code", andTextReturn: { str in })
                    alertT.addButton("Envoyer", withActionBlock: {
                        self.two_step(code: code.text!, email: true, closure: { (res) in
                            if res == -1 {
                                save.set(nil, forKey: "pseudo")
                                save.set(nil, forKey: "password")
                                save.set(nil, forKey: "touchid")
                                self.alert.configureContent(body: "Erreur serveur...")
                                SwiftMessages.show(config: self.config, view: self.alert)
                            }else if res == 1 {
                                self.endlogin(sender)
                            }else if res == -2 {
                                save.set(nil, forKey: "pseudo")
                                save.set(nil, forKey: "password")
                                save.set(nil, forKey: "touchid")
                                self.alert.configureContent(body: "Erreur Two step...")
                                SwiftMessages.show(config: self.config, view: self.alert)
                            }
                        })
                    })
                    alertT.showAlert(withTitle: "Two Step", withSubtitle: "Merci de saisir votre code", withCustomImage: nil, withDoneButtonTitle: "Annuler", andButtons: [])
                })
                alertC.addButton("Google authentificator", withActionBlock: {
                    alertC.dismiss()
                    let code:UITextField = UITextField()
                    code.keyboardType = .numberPad
                    let alertT = FCAlertView()
                    alertT.colorScheme = #colorLiteral(red: 0.190430969, green: 0.4948838353, blue: 0.7447223663, alpha: 1)
                    alertT.addTextField(withCustomTextField: code, andPlaceholder: "Code", andTextReturn: { str in })
                    alertT.addButton("Envoyer", withActionBlock: {
                        self.two_step(code: code.text!, email: false, closure: { (res) in
                            if res == -1 {
                                save.set(nil, forKey: "pseudo")
                                save.set(nil, forKey: "password")
                                save.set(nil, forKey: "touchid")
                                self.alert.configureContent(body: "Erreur serveur...")
                                SwiftMessages.show(config: self.config, view: self.alert)
                            }else if res == 1 {
                                self.endlogin(sender)
                            }else if res == -2 {
                                save.set(nil, forKey: "pseudo")
                                save.set(nil, forKey: "password")
                                save.set(nil, forKey: "touchid")
                                self.alert.configureContent(body: "Erreur Two step...")
                                SwiftMessages.show(config: self.config, view: self.alert)
                            }
                        })
                    })
                    alertT.showAlert(withTitle: "Two Step", withSubtitle: "Merci de saisir votre code", withCustomImage: nil, withDoneButtonTitle: "Annuler", andButtons: [])
                })
                alertC.showAlert(withTitle: "Two Step", withSubtitle: "Quelle est votre methode ?", withCustomImage: nil, withDoneButtonTitle: "Annuler", andButtons:[])
            }
            sender.isEnabled = true
            self.removeAllOverlays()
        }
    }

    func pushend(_ sender:UIButton) {
        sender.isEnabled = true//main
        alert.configureTheme(.success)
        alert.configureContent(body: "Authentification avec success !")
        SwiftMessages.show(config: config, view: alert)
        getProfile {
            self.beforechange()
        }
    }
    
    func endlogin(_ sender:UIButton) {
        self.removeAllOverlays()
        if !(save.string(forKey: "pseudo") == nil) && !(save.string(forKey: "password") == nil) {
            if !save.string(forKey: "pseudo")!.isEmpty && !save.string(forKey: "password")!.isEmpty {
                self.pushend(sender)
                return
            }
        }
        /* Connexion Auto */
        let save_cred = UIAlertController()
        save_cred.title = "Connexion automatique"
        save_cred.message = "Voulez-vous vous connecter automatiquement la prochaine fois ?"
        save_cred.addAction(UIAlertAction(title: "Oui", style: .default, handler: { (prout) in
            let authenticationContext = LAContext()
            var error:NSError?
            guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
                save.set(self.pseudoT.text!, forKey: "pseudo")
                save.set(self.mdpT.text!, forKey: "password")
                save.set(false, forKey: "touchid")
                self.pushend(sender)
                return
            }
            if  !(save.string(forKey: "pseudo") == nil) && !save.bool(forKey: "touchid") { self.pushend(sender); return }
            authenticationContext.evaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                localizedReason: "Connexion to RealityGaming",
                reply: { [unowned self] (success, error) -> Void in
                    if success {
                        DispatchQueue.main.async(execute: {
                            save.set(self.pseudoT.text!, forKey: "pseudo")
                            save.set(self.mdpT.text!, forKey: "password")
                            save.set(true, forKey: "touchid")
                            self.pushend(sender)
                        })
                    }else {
                        if let error = error {
                            DispatchQueue.main.async(execute: {
                                let message = self.errorMessageForLAErrorCode(errorCode: error._code)
                                let alert =  MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
                                alert.configureContent(title: "Erreur", body: "")
                                alert.configureTheme(.error)
                                alert.button?.isHidden = true
                                var config = SwiftMessages.defaultConfig
                                config.preferredStatusBarStyle = .lightContent
                                config.duration = .seconds(seconds: 3)
                                alert.configureContent(body: message)
                                SwiftMessages.show(config: config, view: alert)
                                if error._code == LAError.touchIDLockout.rawValue || error._code == LAError.authenticationFailed.rawValue {
                                    save.set(self.pseudoT.text!, forKey: "pseudo")
                                    save.set(self.mdpT.text!, forKey: "password")
                                    save.set(false, forKey: "touchid")
                                    self.pushend(sender)
                                }
                            })
                        }
                    }
            })
        }))
        save_cred.addAction(UIAlertAction(title: "Non", style: .destructive, handler: { (prout) in
                sender.isEnabled = true
                self.alert.configureTheme(.success)
                self.alert.configureContent(body: "Authentification avec success !")
                SwiftMessages.show(config: self.config, view: self.alert)
                getProfile {
                    self.beforechange()
                }
        }))
        self.present(save_cred, animated: true, completion: nil)
    }
    
    func connexionRG(closure: @escaping (Int) -> ()) {
        if !isConnected() { return }
        self.showWaitOverlayWithText("Loading...")
        let params:Parameters = ["login":self.pseudoT.text!, "password":self.mdpT.text!, "_xfToken":"", "remember":1]
        session.request(URL(string: "https://\(domain_name)/login/login")!, method: .post, parameters: params).validate().responseString { (response) in
            if response.result.value == nil { closure(-1) }
            do {
                let document:Document = try SwiftSoup.parse(response.result.value!)
                if try document.getElementsByClass("p-title-value").get(0).text() == "RealityGaming" {
                    closure(1)
                    return
                }else if try document.getElementsByClass("p-title-value").get(0).text().contains("deux étapes") {
                    closure(2)
                    return
                }else if try document.getElementsByClass("blockMessage--error").array().count > 0 {
                    closure(-2)
                    return
                }else {
                    closure(-3)
                    return
                }
            }catch {}
            closure(-4)
            self.removeAllOverlays()
        }
    }

    func two_step(code:String, email:Bool = false, closure: @escaping (Int) -> ()) {
        let params:Parameters = ["code":code, "trust":1, "provider":email ? "email":"totp", "confirm":1, "_xfToken":_xfToken, "remember":1, "redirect":"https://\(domain_name)/", "save":"Confirmer", "_xfResponseType":"json"]
        session.request(URL(string: "https://\(domain_name)/login/two-step")!, method: .post, parameters: params).validate().responseJSON { (response) in
            if response.result.value == nil { closure(-1) }
            let json:JSON = JSON(response.result.value!)
            debugPrint(json)
            if json["status"].string! == "ok" {
                closure(1)
            }else if json["status"].string! == "error" {
                closure(-2)
            }
        }
        
    }
    
    func beforechange() {
        self.showWaitOverlayWithText("loading...")
        session.request(URL(string: "https://\(domain_name)/chat")!).responseString { (datarep) in
            array_for_shout = []
            var room_ids:[String] = []
            var room_title:[String] = []
            let document:Document = try! SwiftSoup.parse(datarep.result.value!)
            let ids_elemen:Elements = try! document.getElementsByAttribute("data-room-id")
            let title_elemen:Elements = try! document.getElementsByAttribute("data-title")
            ids_elemen.forEach({ (elem) in
                room_ids.append(try! elem.attr("data-room-id"))
            })
            title_elemen.forEach({ (elem) in
                room_title.append(try! elem.attr("data-title"))
            })
            room_ids.removeDuplicates()
            room_title.removeDuplicates()
            for i in 0 ..< room_ids.count {
                array_for_shout.append(ShoutboxV2ViewController.init(itemInfo: IndicatorInfo(title: room_title[i]), infos: [room_ids[i]]))
            }
            array_for_shout.removeDuplicates()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.removeAllOverlays()
                self.performSegue(withIdentifier: "shoutbox", sender: self)
            })
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.mdpT.resignFirstResponder()
        self.pseudoT.resignFirstResponder()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if net?.listener != nil { return }
        net?.listener = { status in
            if net?.isReachable ?? false {
                if (net?.isReachableOnEthernetOrWiFi) != nil {
                    RSLoadingView.hideFromKeyWindow()
                } else if (net?.isReachableOnWWAN)! {
                    RSLoadingView.hideFromKeyWindow()
                }
            } else {
                let loading = RSLoadingView(effectType: RSLoadingView.Effect.spinAlone)
                loading.mainColor = .white
                loading.showOnKeyWindow()
            }
        }
        net?.startListening()
    }
    
    func errorMessageForLAErrorCode( errorCode:Int ) -> String{
        var message = ""
        switch errorCode {
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
        case LAError.touchIDLockout.rawValue:
            message = "Too many failed attempts."
        case LAError.touchIDNotAvailable.rawValue:
            message = "TouchID is not available on the device"
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
        default:
            message = "Did not find error code on LAError object"
        }
        return message
        
    }
}
