//
//  StartPageViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 05/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import RevealingSplashView
import SafariServices
class StartPageViewController: UIViewController {

    @IBOutlet weak var have_account: UIButton!
    @IBOutlet weak var no_account: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        let revealingSplashView = RevealingSplashView(iconImage: #imageLiteral(resourceName: "logo rg"),iconInitialSize: CGSize(width: 119, height: 90), backgroundColor: #colorLiteral(red: 0.1219567135, green: 0.6249053478, blue: 0.9032843709, alpha: 1))
        self.view.addSubview(revealingSplashView)
        revealingSplashView.startAnimation(){
            print("Completed")
        }

        
        self.navigationController?.navigationBar.isHidden = true
        self.have_account.dropShadow(color: UIColor(hex: 0x0B74C4), offSet: CGSize(width: 0, height: 2))
        self.no_account.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.register))
        self.no_account.addGestureRecognizer(tap)
    }

    @objc func register(gesture: UITapGestureRecognizer) {
        let svc = SFSafariViewController(url: URL(string: "https://\(domain_name)/register")!)
        self.present(svc, animated: true, completion: nil)
    }
    
    @IBAction func login(_ sender: UIButton) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
