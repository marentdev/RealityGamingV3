//
//  CreateTopicViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 21/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import Alamofire
import RichEditorView
import SwifterSwift
import SwiftSoup
import SwiftyJSON
import SwiftMessages
class CreateTopicViewController: UIViewController, RichEditorDelegate, UITextViewDelegate {

    @IBOutlet weak var editorView: RichEditorView!
    @IBOutlet weak var htmlView: UITextView!
    @IBOutlet weak var title_topic: UITextField!
    
    lazy var toolbar: RGToolbar = {
        return RGToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 45))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let btn = UIBarButtonItem(image: UIImage.fontAwesomeIcon(name: "", textColor: .black, size: CGSize(width: 25, height: 25)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.createTopic))
        btn.tintColor = .black
        self.navigationItem.rightBarButtonItems = [btn]
        let left = UIBarButtonItem(image: UIImage.fontAwesomeIcon(name: "", textColor: .black, size: CGSize(width: 25, height: 25)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.closemodal))
        left.tintColor = .black
        self.navigationItem.leftBarButtonItems = [left]
        self.navigationItem.title = "Nouvelle discussion"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.isNavigationBarHidden = false
        editorView.placeholder = "Ecrivez ici..."
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        toolbar.editor = editorView
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Hide HTML", style: .done, target: self, action: #selector(self.showEditor))
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.htmlView.delegate = self
        self.htmlView.inputAccessoryView = doneToolbar
        self.htmlView.borderColor = .lightGray
        self.htmlView.borderWidth = 0.3
    }

    @objc func showEditor() {
        self.editorView.isHidden = false
        self.htmlView.endEditing(true)
    }
    
    func richEditorDidFinishLoading(_ editor: RichEditorView) {
        editorView.runJS("document.getElementById('editor').style.paddingLeft = '3px'")
        editorView.runJS("document.getElementById('editor').style.paddingRight = '3px'") 
    }
    
    @objc func closemodal() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func createTopic() {
        if self.title_topic.isEmpty || self.editorView.contentHTML.isEmpty {
            let alert =  MessageView.viewFromNib(layout: .cardView)
            alert.configureContent(title: "Erreur", body: "Merci de renseigner tout les champs !")
            alert.configureTheme(.error)
            alert.button?.isHidden = true
            var config = SwiftMessages.defaultConfig
            config.duration = .seconds(seconds: 3)
            SwiftMessages.show(config: config, view: alert)
            return
        }
        session.request(URL(string: "https://\(domain_name)/forums/\(forum_id)/post-thread")!, method: .post, parameters: ["_xfToken": _xfToken, "_xfResponseType": "json", "prefix_id": 0, "title": self.title_topic.text!, "message_html": self.editorView.contentHTML, "watch_thread": 1, "watch_thread_email": 1, "_xfSet[watch_thread]": 1]).validate().responseJSON { (datarep) in
            let json:JSON = JSON(datarep.result.value!)
            debugPrint(json)
            if json["status"].string! == "ok" && json["message"].exists() && json["redirect"].exists() {
                self.dismiss(animated: true, completion: nil)
            }else {
                //error
            }
        }
    }
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if !self.editorView.isHidden {
            if content == "<br>" {
                htmlView.clear()
            } else {
                htmlView.text = content
            }
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        if self.editorView.isHidden {
            self.editorView.html = self.htmlView.text!
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
