//
//  TopicViewViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 25/02/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import MessageKit
import Alamofire
import SwifterSwift
import SwiftyJSON
import SwiftSoup
import RichEditorView
import SideMenu
import SwiftMessages
import DropDown
import FCAlertView
var topic_id:String = ""
var is_post:Bool = false
class TopicViewViewController: MessagesViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, RichEditorDelegate, UIWebViewDelegate {

    struct topic {
        var id:Int = Int(topic_id)!
        var author:String = ""
        var author_id:Int = 0
        var content:String = ""
        var id_post:Int = 0
        var messages:[message] = []
        var can_rep:Bool = true
        var liked_name:String = "J'aime"
    }
    struct message {
        var id:Int = 0
        var content:String = ""
        var author:String = ""
        var author_id:Int = 0
        var liked_name:String = "J'aime"
    }
    
    var webview = UIWebView()
    var new_topic:topic = topic()
    var contentHeights:[Int] = []
    var ispassed:[Int:Int] = [:]
    var tableView: UITableView = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        section_active = [false, false, false]
        getProfile{ }
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuShadowRadius = 3
        SideMenuManager.default.menuWidth = 260
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        self.defaultStyle()
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height + self.navigationController!.navigationBar.height
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.width - messageInputBar.height, height: self.view.height - barHeight))
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.view.addSubview(self.tableView)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.webview = UIWebView(frame: CGRect(x: 0, y: 0, width: self.view.width - 40, height: self.view.height - barHeight))
        webview.delegate = self
        self.webview.isHidden = true
        self.view.addSubview(self.webview)
        self.view.backgroundColor = .white
        self.tableView.isHidden = true
        self.webview.sizeToFit()
        setupPage()
    }

    
    @objc func onButton() {
        //Send action
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        is_post = false
    }
    
    func setupPage() {
        new_topic = topic()
        contentHeights = []
        ispassed = [:]
        self.end = false
        self.showWaitOverlayWithText("Loading...")
        let link = is_post ? "https://\(domain_name)/posts/\(topic_id)?_xfResponseType=json&_xfToken=\(_xfToken)" : "https://\(domain_name)/threads/\(topic_id)?_xfResponseType=json&_xfToken=\(_xfToken)"
        if is_post {
            session.request(URL(string: link)!).validate().responseJSON { datarep in
                let json:JSON = JSON(datarep.result.value!)
                if json["status"].string! == "ok" {
                    self.nextSetupPage(link: "\(json["redirect"].string!.components(separatedBy: "#")[0])?_xfResponseType=json&_xfToken=\(_xfToken)")
                }else {
                    //error
                }
            }
        }else {
            self.nextSetupPage(link: link)
        }
    }
    
    func nextSetupPage(link:String) {
        session.request(URL(string: link)!).validate().responseJSON { datarep in
            do {
                let json:JSON = JSON(datarep.result.value!)
                if json["status"].string! == "ok" && json["redirect"].exists() {
                    self.nextSetupPage(link: json["redirect"].string! + "?_xfResponseType=json&_xfToken=\(_xfToken)")
                }else if json["status"].string! == "ok" {
                    self.navigationItem.title = Clean_string(text: json["html"]["title"].string!)
                    let document:Document = try! SwiftSoup.parse(json["html"]["content"].string!)
                    if try document.getElementsByClass("blockStatus-message--locked").array().count > 0 { self.new_topic.can_rep = false; self.messageInputBar.inputTextView.isEditable = false; self.messageInputBar.inputTextView.placeholder = "Discussion close" }
                    if is_post {
                        is_post = false
                        topic_id = link.components(separatedBy: ".")[2].components(separatedBy: ["/"])[0]
                    }
                    let article_msg:Elements = try! document.getElementsByClass("message--post")
                    let main_msg:Element = article_msg.first()!
                    self.new_topic.author = (try! main_msg.getAttributes()?.get(key: "data-author"))!
                    self.new_topic.id_post = Int((try! main_msg.getAttributes()?.get(key: "id"))!.remove(text: ["js-post-"]))!
                    self.new_topic.content = try! main_msg.getElementsByClass("bbWrapper").get(0).html()
                    article_msg.forEach({ (elem) in
                        if elem != main_msg {
                            var new_msg:message = message()
                            new_msg.author = (try! elem.getAttributes()?.get(key: "data-author"))!
                            new_msg.id = Int((try! elem.getAttributes()?.get(key: "id"))!.remove(text: ["js-post-"]))!
                            new_msg.content = try! elem.getElementsByClass("bbWrapper").get(0).html()
                            self.new_topic.messages.append(new_msg)
                        }
                    })
                    for i in 0 ..< article_msg.array().count {
                        let like = try article_msg.get(i).getElementsByClass("actionBar-set").get(0).child(0).text()
                        if i == 0 {
                            self.new_topic.liked_name = like
                        }else {
                            self.new_topic.messages[i - 1].liked_name = like
                        }
                    }
                    self.contentHeights = [Int](repeating: 0, count: self.new_topic.messages.count + (self.new_topic.content == topic().content ? 0 : 1))
                    self.tableView.reloadData()
                    self.setupHeight(index: 0)
                }
            }catch { }
        }
    }
    
    func setupHeight(index:Int) {
        var normalize:String = ""
        var style:String = ""
        let url = Bundle.main.url(forResource: "normalize", withExtension: "css")
        let url2 = Bundle.main.url(forResource: "style", withExtension: "css")
        do {
            normalize = try String(contentsOf: url!, encoding: String.Encoding.utf8)
            style = try String(contentsOf: url2!, encoding: String.Encoding.utf8)
        }catch {}
        let html_before = """
        <!DOCTYPE html>
        <html lang="fr">
        <head>
        <meta name="viewport" content="user-scalable=no">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>\(normalize)</style>
        <style>\(style)</style>
        </head>
        <body>
        <div id="editor" contentEditable="true" placeholder="" class="placeholder"></div>
        <script type="text/javascript" src="rich_editor.js"></script>
        </body>
        </html>
        """
        let html_after = "</div></body></html>"
        if (self.new_topic.messages.count + (self.new_topic.content == topic().content ? 0 : 1) - 1) < index {
            end = true
            self.tableView.reloadData {
                self.removeAllOverlays()
                self.tableView.isHidden = false
            }
            return
        }
        else if index == 0 {
            webview.tag = index
            webview.loadHTMLString(html_before + parseTopicHTML(html: self.new_topic.content) + html_after, baseURL: nil)
        }else {
            webview.tag = index
            webview.loadHTMLString(html_before + parseTopicHTML(html: self.new_topic.messages[index - 1].content) + html_after, baseURL: nil)
        }
    }
    var end:Bool = false
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading { end = true; self.tableView.isHidden = false; return }
        if (self.new_topic.messages.count + (self.new_topic.content == topic().content ? 0 : 1) - 1) >= webView.tag {
            let result = Int(webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].clientHeight;")!)!
            if self.contentHeights[webView.tag] < result { self.contentHeights[webView.tag]  = result }
            self.setupHeight(index: webView.tag + 1)
        }else {
            return
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.contentHeights.count > indexPath.row {
            return CGFloat(self.contentHeights[indexPath.row] + 76)
        }
        return 76
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.new_topic.messages.count + (self.new_topic.content == topic().content ? 0 : 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "TopicTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TopicTableViewCell
        cell.content_topic.isEditingEnabled = false
        cell.content_topic.isScrollEnabled = false
        cell.content_topic.webView.scrollView.isScrollEnabled = false
        cell.content_topic.html = parseTopicHTML(html: indexPath.row == 0 ? self.new_topic.content : self.new_topic.messages[indexPath.row - 1].content)
        cell.content_topic.delegate = self
        cell.content_topic.tag = indexPath.row
        cell.content_topic.sizeToFit()
        cell.author.text = indexPath.row == 0 ? self.new_topic.author : self.new_topic.messages[indexPath.row - 1].author
        getLinkAvatarFormPseudo(username: indexPath.row == 0 ? self.new_topic.author : self.new_topic.messages[indexPath.row - 1].author, imageView: cell.avatar)
        cell.selectionStyle = .none
        cell.menu.tag = indexPath.row
        cell.menu.addTarget(self, action: #selector(self.optionMenu), for: .touchUpInside)
        return cell
    }
    
    @objc func optionMenu(_ sender:UIButton) {
        let dropdown:DropDown = DropDown(anchorView: sender)
        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.dataSource = [sender.tag == 0 ? self.new_topic.liked_name : self.new_topic.messages[sender.tag - 1].liked_name]
        dropdown.selectionAction = { index, str in
            if index == 0 {
                session.request(URL(string: "https://\(domain_name)/posts/\(sender.tag == 0 ? self.new_topic.id_post : self.new_topic.messages[sender.tag - 1].id)/like")!, method: .post, parameters: ["_xfResponseType": "json", "_xfToken": _xfToken]).responseJSON(completionHandler: { (datarep) in
                    let json:JSON = JSON(datarep.result.value!)
                    if json["status"].string! == "ok" {
                        if sender.tag == 0  {
                            self.new_topic.liked_name = self.new_topic.liked_name.wordCount() > 3 ? "J'aime" : "Je n'aime plus"
                        }else {
                            self.new_topic.messages[sender.tag - 1].liked_name = self.new_topic.messages[sender.tag - 1].liked_name.wordCount() > 3 ? "J'aime" : "Je n'aime plus"
                        }
                    }else if json["errors"].exists() {
                        let alert =  MessageView.viewFromNib(layout: .cardView)
                        alert.configureContent(title: "Erreur", body: json["errors"][0].string!)
                        alert.configureTheme(.error)
                        alert.button?.isHidden = true
                        var config = SwiftMessages.defaultConfig
                        config.duration = .seconds(seconds: 3)
                        SwiftMessages.show(config: config, view: alert)
                    }
                })
            }/*else if index == 1 {
                let alert:FCAlertView = FCAlertView()
                let msg_warn:UITextField = UITextField()
                alert.addTextField(withCustomTextField: msg_warn, andPlaceholder: "Raison...", andTextReturn: { text in
                    //do Nothing
                })
                alert.addButton("Signaler", withActionBlock: {
                    session.request(URL(string: "https://\(domain_name)/posts/\(sender.tag == 0 ? self.new_topic.id_post : self.new_topic.messages[sender.tag - 1].id)/report")!, method: .post, parameters: ["_xfResponseType": "json", "_xfToken": _xfToken, "message": msg_warn.text!]).responseJSON(completionHandler: { (datarep) in
                        let json:JSON = JSON(datarep.result.value!)
                        if json["status"].string! == "ok" {
                            let alert =  MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
                            alert.configureContent(title: "Signalement", body: "Merci pour votre signalement !")
                            alert.configureTheme(.success)
                            alert.button?.isHidden = true
                            var config = SwiftMessages.defaultConfig
                            config.duration = .seconds(seconds: 3)
                            SwiftMessages.show(config: config, view: alert)
                        }else if json["errors"].exists() {
                            let alert =  MessageView.viewFromNib(layout: .cardView)
                            alert.configureContent(title: "Erreur", body: json["errors"][0].string!)
                            alert.configureTheme(.error)
                            alert.button?.isHidden = true
                            var config = SwiftMessages.defaultConfig
                            config.duration = .seconds(seconds: 3)
                            SwiftMessages.show(config: config, view: alert)
                        }
                       
                    })
                })
                alert.doneActionBlock({
                    self.messageInputBar.isHidden = true
                })
                alert.showAlert(withTitle: "Signalement du message de \(sender.tag == 0 ? self.new_topic.author : self.new_topic.messages[sender.tag - 1].author)", withSubtitle: "", withCustomImage: nil, withDoneButtonTitle: "Annuler", andButtons: nil)
            }*/
        }
        dropdown.show()
    }
    
    func richEditor(_ editor: RichEditorView, heightDidChange height: Int) {
        if self.contentHeights[editor.tag] < height && end == false {
            self.contentHeights[editor.tag] = height
            self.tableView.reloadData()
        }else {
            return
        }
    }
    
    func richEditor(_ editor: RichEditorView, shouldInteractWith url: URL) -> Bool {
        return false
    }
    
}

extension TopicViewViewController {
    func parseTopicHTML(html:String) -> String {
        do {
            let msg_doc:Document = try! SwiftSoup.parse(html)
            var result:String = ""
            /* Remove Block */
            try! msg_doc.getElementsByClass("bbCodeBlock--quote").forEach({ (elem) in
                //Quote BBCODE
                try! elem.remove()
            })
            try! msg_doc.getElementsByClass("bbCodeBlock--code").forEach({ (elem) in
                //Code BBCODE
                try! elem.remove()
            })
            try! msg_doc.getElementsByClass("bbCodeInline").forEach({ (elem) in
                //Code Inline BBCODE
                try! elem.remove()
            })
            try! msg_doc.getElementsByClass("smilie").forEach({ (elem) in
                if (elem.getAttributes()?.get(key: "src"))!.contains("data:image") {
                    try! elem.html((elem.getAttributes()?.get(key: "alt"))!)
                }
            })
            result = try! msg_doc.html()
            result = result.replacingOccurrences(of: "<img src=\"/", with: "<img src=\"https://\(domain_name)/")
            result = result.replacingOccurrences(of: " src=\"data:text/html", with: " marent=\"data:text/html").replacingOccurrences(of: "data-s9e-lazyload-src=\"", with: "src=\"").replacingOccurrences(of: "src=\"//", with: "src=\"https://").replacingOccurrences(of: "&amp;width=700&amp;height=80&amp;", with: "&amp;width=50&amp;height=50&amp;").replacingOccurrences(of: "<div class=\"bbCodeBlock bbCodeBlock--spoiler\">", with: "<div class=\"bbCodeBlock bbCodeBlock--spoiler\" style=\"display:none;\">").replacingOccurrences(of: " <button type=\"button\" class=\"bbCodeSpoiler-button button--longText button\"", with: " <button type=\"button\" class=\"bbCodeSpoiler-button button--longText button\" onclick=\"alert('ShowSpoiler')\"")
            debugPrint("Start:", result, "END")
            let url_fontawesome = Bundle.main.url(forResource: "font-awesome", withExtension: "css")
            let fontawesome:String = try String(contentsOf: url_fontawesome!, encoding: String.Encoding.utf8)
            return "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\"><style>\(fontawesome)</style>\(result)"
        }catch {}
        return""
    }
    
    @objc func sendReply() {
        var msg:String = messageInputBar.inputTextView.text
        messageInputBar.inputTextView.clear()
        msg = msg.replacingOccurrences(of: "\n", with: "<br>").replacingOccurrences(of: "\r", with: "<br>")

        session.request(URL(string: "https://\(domain_name)/threads/\(topic_id)/add-reply")!, method: .post, parameters: ["message_html": "<p>" + msg + "</p>", "_xfToken": _xfToken, "_xfResponseType": "json"]).validate().responseJSON { (datarep) in
            if datarep.response?.statusCode == 403 {
                //Cloudflare 🖕
                return
            }
            let json:JSON = JSON(datarep.result.value!)
            if json["status"].string! == "ok" {
                //message for the msg
                self.setupPage()
            }else if json["status"].string! == "error" {
                //error -> errors[0]
                let alert =  MessageView.viewFromNib(layout: .cardView)
                alert.configureContent(title: "Erreur", body: json["errors"][0].string!)
                alert.configureTheme(.error)
                alert.button?.isHidden = true
                var config = SwiftMessages.defaultConfig
                config.duration = .seconds(seconds: 3)
                SwiftMessages.show(config: config, view: alert)
            }
        }
    }
    
    func defaultStyle() {
        let newMessageInputBar = MessageInputBar()
        newMessageInputBar.sendButton.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
        newMessageInputBar.backgroundView.backgroundColor = .white
        newMessageInputBar.inputTextView.borderColor = .white
        newMessageInputBar.inputTextView.placeholder = "Quick reply..."
        newMessageInputBar.sendButton.addTarget(self, action: #selector(self.sendReply), for: .touchUpInside)
        //newMessageInputBar.delegate = self
        messageInputBar = newMessageInputBar
        reloadInputViews()
    }
    
    // MARK: - Helpers
    
    func makeButton(named: String) -> InputBarButtonItem {
        return InputBarButtonItem()
            .configure {
                $0.spacing = .fixed(10)
                $0.image = UIImage(named: named)?.withRenderingMode(.alwaysTemplate)
                $0.setSize(CGSize(width: 30, height: 30), animated: true)
            }.onSelected {
                $0.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
            }.onDeselected {
                $0.tintColor = UIColor.lightGray
            }.onTouchUpInside { _ in
                print("Item Tapped")
        }
    }
}
