//
//  TopicTableViewCell.swift
//  RealityGaming
//
//  Created by Mathieu VIGNE on 2/14/18.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import RichEditorView
class TopicTableViewCell: UITableViewCell {

    @IBOutlet weak var content_topic: RichEditorView!
    @IBOutlet weak var author_bar: UIView!
    @IBOutlet weak var menu: UIButton!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
