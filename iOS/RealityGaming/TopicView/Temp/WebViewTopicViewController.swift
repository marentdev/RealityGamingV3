//
//  WebViewTopicViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 02/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import PTPopupWebView
import SwiftMessages
import SwifterSwift
import SideMenu
var from_url:Bool = false
class WebViewTopicViewController: UIViewController, UIWebViewDelegate {

    var webView:UIWebView = UIWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.showWaitOverlayWithText("Loading...")
        getProfile{ }
        
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuShadowRadius = 3
        SideMenuManager.default.menuWidth = 260
        section_active = [false, false, false]
       let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height + self.navigationController!.navigationBar.height
        self.webView = UIWebView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height - barHeight))
        self.webView.delegate = self
        let link = is_post ? "https://\(domain_name)/posts/\(topic_id)" : "https://\(domain_name)/threads/\(topic_id)"
        self.webView.loadRequest(URLRequest(url: URL(string: link)!))
        self.webView.isHidden = true
        self.webView.backgroundColor = .white
        self.view.addSubview(webView)
        if from_url {
            let btn = UIBarButtonItem(image: UIImage.fontAwesomeIcon(name: "", textColor: .black, size: CGSize(width: 30, height: 35)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openMenu))
            btn.tintColor = .black
            self.navigationItem.leftBarButtonItems = [btn]
            from_url = false
        }
        self.navigationItem.title = "RealityGaming"
    }

    @objc func openMenu() {
        self.performSegue(withIdentifier: "openmenu", sender: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        if !webView.isLoading {
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('p-navSticky')[0].style.display = 'none';")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('p-body')[0].style.backgroundColor = 'white'")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('notices')[0].style.display = 'none';")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('p-breadcrumbs')[0].style.display = 'none';")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('p-breadcrumbs')[1].style.display = 'none';")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('appifyMobileAdDisabled')[0].style.display = 'none';")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('appifyMobileAdDisabled')[1].style.display = 'none';")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('p-title-value')[0].style.fontSize = '17px'")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('block block--messages')[0].children[0].style.display = 'none'")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('block similarthreads-narrow')[0].style.display = 'none';")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('block-container block-wrtt')[0].style.display = 'none';")
            self.webView.stringByEvaluatingJavaScript(from: "document.cookie = \"appify=\"")
            self.webView.stringByEvaluatingJavaScript(from: "document.getElementById('footer').style.display = 'none'")
            self.webView.isHidden = false
            self.removeAllOverlays()
        }
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url:String = (request.url?.absoluteString)!
        print("Called:", url, navigationType.hashValue)
        if url.contains("googleads") ||  url.contains("about:") || url.contains("data:text/html"){
            return false
        }
        if !url.isValidHttpsUrl {
            let alert =  MessageView.viewFromNib(layout: MessageView.Layout.statusLine)
            alert.configureContent(title: "Erreur", body: "URL: \(url) n'est pas sûr !")
            alert.configureTheme(.warning)
            alert.button?.isHidden = true
            var config = SwiftMessages.defaultConfig
            config.duration = .seconds(seconds: 3)
            SwiftMessages.show(config: config, view: alert)
            return false
        }
        if (request.url?.host == "\(domain_name)" || url.contains("embed") || url.contains("google.com")) && navigationType != .linkClicked {
            return true
        }
        if navigationType == .other && url.isValidUrl {
            print("WHY", url, navigationType.hashValue)
            let al_web = PTPopupWebViewController()
            al_web.popupView.URL(string: url)
            al_web.show()
            return false
        }
        if request.url?.host == "\(domain_name)" && navigationType == .linkClicked {
            let fragment = url.components(separatedBy: "/")
            if url == "https://realitygaming.fr/fabien/" {
                user_id = "38138"
                let next:ProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "gotoprrofil") as! ProfileViewController
                self.navigationController?.pushViewController(next, animated: true)
                return false
            }
            if url == "https://realitygaming.fr/jb/"  {
                user_id = "1"
                let next:ProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "gotoprrofil") as! ProfileViewController
                self.navigationController?.pushViewController(next, animated: true)
                return false
            }
            if fragment[3] == "members" {
                user_id = fragment[4].contains(".") ? fragment[4].components(separatedBy: ".")[1] : fragment[4]
                let next:ProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "gotoprrofil") as! ProfileViewController
                self.navigationController?.pushViewController(next, animated: true)
            }
            if url.contains(topic_id) && (url.contains("page-") || url.ends(with: "\(topic_id)/")) { return true }
        }
        return false
    }
    
}
