//
//  ParticipantTableViewCell.swift
//  RealityGaming
//
//  Created by Mathieu VIGNE on 2/28/18.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import Eureka

public class ParticipantTableViewCell: Cell<Bool>, CellType {
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var title_perso: UILabel!
    
    public override func setup() {
        super.setup()
        
    }
    
    public override func update() {
        super.update()
    }
}

// The custom Row also has the cell: CustomCell and its correspond value
public final class ParticipantRow: Row<ParticipantTableViewCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        // We set the cellProvider to load the .xib corresponding to our cell
        cellProvider = CellProvider<ParticipantTableViewCell>(nibName: "ParticipantTableViewCell")
    }
}
