//
//  ConvInfosViewController.swift
//  RealityGaming
//
//  Created by Mathieu VIGNE on 2/27/18.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import SwifterSwift
import Alamofire
import SwiftyJSON
import SwiftSoup
import Eureka
import FCAlertView
import DropDown
class ConvInfosViewController: FormViewController, UITextFieldDelegate {
    
    var sec_member:Section = Section("Participants:")
    var sec_action:Section = Section()
    let textfield_member:UITextField = UITextField()
    let dropdown = DropDown()
    var old_length:Int = 0
    
    struct participant {
        var name:String = ""
        var id:Int = 0
        var title_perso:String = ""
    }
    
    struct infos {
        var can_add:Bool = false
        var can_edit:Bool = false
        var is_star:Bool = false
        var no_read:Bool = false
    }
    
    var participants:[participant] = []
    var info:infos = infos()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.textfield_member.delegate = self
        self.setupPage()
        //put this at the end !
    }
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupPage() {
        self.showWaitOverlayWithText("Loading...")
        self.participants.removeAll()
        session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/page-999999")!).validate().responseString { (datarep) in
            do {

                let document:Document = try SwiftSoup.parse(datarep.result.value!)
                let block_part:Elements = try document.getElementsByClass("p-body-sidebar").get(0).getElementsByClass("block").get(1).getElementsByClass("block-row")
                try block_part.forEach({ (part) in
                    var particip:participant = participant()
                    particip.name = try part.getElementsByClass("username").get(0).text()
                    particip.title_perso = try part.getElementsByClass("contentRow-minor").get(0).text()
                    if let id = Int((try part.getElementsByClass("username").get(0).getAttributes()?.get(key: "data-user-id"))!) {
                        particip.id = id
                    }else {
                        particip.id = -1
                    }
                    self.participants.append(particip)
                })
                let star:Elements = try document.getElementsByAttributeValue("data-sk-star", "Star")
                if star.array().count > 0 {
                    self.info.is_star = try document.getElementsByAttributeValue("data-sk-star", "Star").text() == "Star" ? false : true
                }
                let edit:Elements = try document.getElementsByClass("button--icon--edit")
                if edit.array().count > 0 {
                    self.info.can_edit = true
                }
                let nore:Elements = try document.getElementsByAttributeValue("data-sk-read", "Marquer comme lu")
                if nore.array().count > 0 {
                    self.info.no_read = true
                }
                let add:Elements = try document.getElementsByClass("block-footer-controls")
                if add.array().count > 0 {
                    self.info.can_add = true
                }
                self.setupDesign()
            }catch {
                
            }
        }
    }
    
    func setupDesign() {
        self.participants.forEach { (particip) in
            //Custom cell participant
            let row:ParticipantRow = ParticipantRow()
            row.cell.name.text = particip.name
            row.cell.avatar.layer.masksToBounds = true
            row.cell.avatar.layer.cornerRadius = row.cell.avatar.height / 2
            loadImageFromUrl(url: get_avatar_link(id: String(particip.id)), view: row.cell.avatar)
            row.cell.title_perso.text = particip.title_perso
            sec_member += [row]
            debugPrint(particip)
        }
        if self.info.can_add {
            let add_people:ButtonRow = ButtonRow()
            add_people.title = "Inviter d'autres membres"
            add_people.onCellSelection { (btncell, btn) in
                let alert:FCAlertView = FCAlertView()
                alert.colorScheme = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
                self.textfield_member.addTarget(self, action: #selector(self.textfieldDidchange(textfield:)), for: .editingChanged)
                alert.addTextField(withCustomTextField: self.textfield_member, andPlaceholder: "Membres", andTextReturn: { (text) in
                })
                alert.addButton("Invités", withActionBlock: {
                    var membres:[String] = self.textfield_member.text!.components(separatedBy: "/ ")
                    membres.removeLast()
                    session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/invite")!, method: .post, parameters: ["_xfResponseType": "json", "_xfToken": _xfToken, "recipients": membres.joined(separator: ", ")]).responseJSON(completionHandler: { (datarep) in
                        do {
                            let json:JSON = try JSON(datarep.result.value!)
                            print(json)
                            if json["errors"][0] != JSON.null {
                                let alert = FCAlertView()
                                alert.colorScheme = .red
                                alert.makeAlertTypeWarning()
                                alert.showAlert(inView: self, withTitle: "Erreur", withSubtitle: json["errors"][0].string!, withCustomImage: nil, withDoneButtonTitle: "Ok", andButtons: nil)
                            }else {
                                let alert = FCAlertView()
                                alert.makeAlertTypeSuccess()
                                alert.showAlert(inView: self, withTitle: "Success", withSubtitle: "Membre(s) ajouté(s): \(membres.joined(separator: ", "))", withCustomImage: nil, withDoneButtonTitle: "Ok", andButtons: nil)
                                self.dismiss(animated: true, completion: nil)
                            }
                        } catch {
                            
                        }
                    })
                })
                alert.doneActionBlock({  })
                alert.showAlert(inView: self, withTitle: "Inviter des membres à la conversation", withSubtitle: "Séparer les noms avec une virgule. Les invités pourront voir les discussions depuis le début. Vous pouvez inviter jusqu'à 194 membres.", withCustomImage: nil, withDoneButtonTitle: "Cancel", andButtons: nil)
            }
            sec_member += [add_people]
        }
        if self.info.can_edit {
            let edit_conv:ButtonRow = ButtonRow()
            edit_conv.title = "Editer la conversation"
            edit_conv.onCellSelection { (btncell, btn) in
                var title:String = ""
                session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/edit")!, method: .get, parameters: ["_xfToken":_xfToken, "_xfResponseType":"json"]).responseString(completionHandler: { (repdata) in
                    do {
                        let html:String = repdata.result.value!.remove(text: ["\\\""])
                        title = (GetElement(input: html as NSString, paternn: "class=input name=title value=(.*?)maxlength=150 id=_xfUi") as String).remove(text: ["class=input name=title value=", "maxlength=150 id=_xfUi"])
                        let alert:FCAlertView = FCAlertView()
                        let textfield:UITextField = UITextField()
                        alert.colorScheme = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
                        textfield.text = title
                        alert.addTextField(withCustomTextField: textfield, andPlaceholder: "Title", andTextReturn: { (text) in
                        })
                        alert.addButton("Sauvegarder", withActionBlock: {
                            session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/edit")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType":"json", "title": textfield.text!]).responseString(completionHandler: { (repdata) in
                                do {
                                    let json:JSON = JSON.init(parseJSON: repdata.result.value!)
                                    if json["status"] == "ok" {
                                        self.dismiss(animated: true, completion: nil)
                                    }else {
                                        //error
                                    }
                                }catch {}
                            })
                        })
                        alert.showAlert(inView: self, withTitle: "Editer la conversation", withSubtitle: "Vous etes sur le point d'editer le titre de la conversation", withCustomImage: nil, withDoneButtonTitle: "Cancel", andButtons: nil)
                    }catch {}
                })
            }
            sec_action += [edit_conv]
        }
        let star_conv:ButtonRow = ButtonRow()
        star_conv.title = self.info.is_star ? "Unstar" : "Star"
        star_conv.onCellSelection { (btncell, btn) in
            session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/star")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType":"json"]).validate().responseString(completionHandler: { (repdata) in
                do {
                    let json:JSON = try JSON.init(parseJSON: repdata.result.value!)
                    if json["status"] == "ok" {
                        self.info.is_star = !self.info.is_star
                        self.dismiss(animated: true, completion: nil)
                    }else {
                        //error
                    }
                }catch {}
            })
        }
        let no_read:ButtonRow = ButtonRow()
        no_read.title = self.info.no_read ? "Marquer comme non lu" : "Marquer comme lu"
        no_read.onCellSelection { (btncell, btn) in
            session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/mark-unread")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType":"json"]).validate().responseString(completionHandler: { (repdata) in
                do {
                    let json:JSON = try JSON.init(parseJSON: repdata.result.value!)
                    if json["status"] == "ok" {
                        self.info.no_read = !self.info.no_read
                        self.dismiss(animated: true, completion: nil)
                    }else {
                        //error
                    }
                }catch {}
            })
        }
        let leave_conv:ButtonRow = ButtonRow()
        leave_conv.title = "Leave the conversation"
        leave_conv.cellUpdate { (cell, row) in
            cell.textLabel?.textColor = .red
        }
        leave_conv.onCellSelection { (btncell, btn) in
            var stock:Int = 0
            let alert = UIAlertController(style: .actionSheet, title: "Quitter la conversation", message: "Vous etes sur le point de quitter la conversation !")
            
            let choix: [String] = ["Accepter les messages à venir", "Ignorer les messages à venir"]
            let pickerViewValues: [[String]] = [choix]
            let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 0)
            
            alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
                stock = index.row
                print(choix[stock])
            }
            alert.preferredContentSize.height = 100
            alert.addAction(UIAlertAction(title: "Leave", style: .default, handler: { (alert) in
                session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/leave")!, method: .post, parameters: ["_xfToken":_xfToken, "_xfResponseType":"json", "recipient_state": stock == 0 ? "deleted" : "deleted_ignored"]).validate().responseString(completionHandler: { (repdata) in
                    do {
                        let json:JSON = JSON.init(parseJSON: repdata.result.value!)
                        if json["status"] == "ok" {
                            self.dismiss(animated: true, completion: nil)
                        }else {
                            //error
                        }
                    }catch {}
                })
            }))
            alert.addAction(title: "Annuler", style: .cancel)
            self.present(alert, animated: true, completion: nil)
        }
        sec_action += [star_conv, no_read, leave_conv]
        self.removeAllOverlays()
        form += [sec_member, sec_action]
    }

    @objc func textfieldDidchange(textfield: UITextField) {
        if textfield == self.textfield_member {
            if textfield.text!.count > 0 && old_length > textfield.text!.count && textfield.text!.last! == "/" {
                var ar = textfield.text!.components(separatedBy: "/ ")
                ar.removeLast()
                var res:String = ""
                for i in 0 ..< ar.count {
                    res = res + "\(ar[i])/ "
                }
                self.textfield_member.text = res
            }else if textfield.text!.count >= 2 {
                session.request(URL(string: "https://\(domain_name)/members/find")!, method: .get, parameters: ["_xfResponseType": "json", "_xfToken": _xfToken, "q": self.textfield_member.text!.contains("/ ") ? self.textfield_member.text!.components(separatedBy: "/ ").last!: textfield.text!]).responseJSON(completionHandler: { (datarep) in
                    do {
                        let json:JSON = JSON(datarep.result.value!)
                        self.dropdown.dataSource.removeAll()
                        for i in 0 ..< (json["results"].count > 4 ? 4 : json["results"].count) {
                            self.dropdown.dataSource.append(json["results"][i]["id"].string!)
                        }
                        if self.textfield_member.text!.contains("/ ") {
                            self.dropdown.dataSource.append("\(textfield.text!.components(separatedBy: "/ ").last!)")
                        }else {
                            self.dropdown.dataSource.append("\(textfield.text!)")
                        }
                        self.dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                            if self.textfield_member.text!.contains("/ ") {
                                var ar = self.textfield_member.text!.components(separatedBy: "/ ")
                                ar.removeLast()
                                var res:String = ""
                                for i in 0 ..< ar.count {
                                    res = res + "\(ar[i])/ "
                                }
                                self.textfield_member.text = res + "\(item)/ "
                            } else {
                                self.textfield_member.text = "\(item)/ "
                            }
                            self.old_length = self.textfield_member.text!.count
                        }
                    }catch {
                        
                    }
                })
            }else {
                dropdown.dataSource = ["Please enter \(2 - textfield.text!.count) or more characters"]
            }
            dropdown.anchorView = textfield
            dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
            dropdown.reloadAllComponents()
            dropdown.show()
            old_length = self.textfield_member.text!.count
        }
    }
}
