//
//  ConvViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 19/01/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import MessageKit
import SwiftSoup
import Alamofire
import SwiftMessages
import DropDown
import SwiftyJSON
import FCAlertView
var conv_id:String = ""
var conv_title:String = ""
class ConvViewController: MessagesViewController  {
    let refreshControl = UIRefreshControl()
    
    var messageList: [MockMessage] = []
    
    var isTyping = false
    
    var page_cur:Int = 0
    
    var ar:[Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isTranslucent = false
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
        messageInputBar.delegate = self
        
        self.messagesCollectionView.reloadData()
        self.messagesCollectionView.scrollToBottom()
        scrollsToBottomOnKeybordBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false
        
        self.messagesCollectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(ConvViewController.load_more), for: .valueChanged)
        let button = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ConvViewController.back))
        let buttonm = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ConvViewController.miniMenu))
        if let font = UIFont(name: "Font Awesome 5 Free", size: 17) {
            buttonm.setTitleTextAttributes([NSAttributedStringKey.font: font], for: .normal)
            buttonm.setTitleTextAttributes([NSAttributedStringKey.font: font], for: .selected)
        }
        if let font = UIFont(name: "Font Awesome 5 Free", size: 20) {
            button.setTitleTextAttributes([NSAttributedStringKey.font: font], for: .normal)
            button.setTitleTextAttributes([NSAttributedStringKey.font: font], for: .selected)
        }
        button.tintColor = UIColor.black
        buttonm.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItems = [button]
        self.navigationItem.rightBarButtonItems = [buttonm]
        self.navigationItem.title = conv_title
        iMessage()
        setupPage()
        
    }
    
    @objc func miniMenu() {
        self.performSegue(withIdentifier: "gotoinfoconv", sender: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/page-999999")!).validate().responseString { (repdata) in
            if repdata.result.value == nil {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func back() {
        dismiss(animated: true, completion: nil)
    }
    
    func setupPage() {
        messageList = []
        session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/page-999999")!).responseString { (repdata) in
            let document:Document = try! SwiftSoup.parse(repdata.result.value!)
            let blocks:Elements = try! document.getElementsByClass("message message--conversationMessage   js-message")
            print("Start")
            do {
                if try document.getElementsByClass("pageNav-page--current").array().count > 0 {
                    self.page_cur = Int(try document.getElementsByClass("pageNav-page--current").get(0).text())!//existe pas check
                }
            }
            catch { self.page_cur = 0 }
            for i in 0 ..< blocks.array().count {
                let auteur:String = (try! blocks.get(i).getAttributes()?.get(key: "data-author"))!
                let id_auteur:String = (try! blocks.get(i).getElementsByClass("message-name").get(0).getElementsByClass("username").get(0).getAttributes()?.get(key: "data-user-id"))!
                let date:String = try! blocks.get(i).getElementsByClass("u-dt").get(0).text()
                var toreplace:String = ""
                do {
                    toreplace = try blocks.get(i).getElementsByClass("message-body js-selectToQuote").get(0).getElementsByClass("bbCodeBlock-expandContent").html()
                }
                catch {
                    
                }
                var message:String = try! blocks.get(i).getElementsByClass("message-body js-selectToQuote").html().stringByReplacingFirstOccurrenceOfString(target: toreplace, withString: "").remove(text: ["&nbsp;", "<div class=\"js-selectToQuoteEnd\">", "<div class=\"bbWrapper\">", "</div>", "\n", "\r", "\t"])
                message = message.remove(text: [(GetElement(input: message as NSString, paternn: "<div class=\"bbCodeBlock-expandContent\">(.*?)</a>") as String), (GetElement(input: message as NSString, paternn: "<a href=\"/goto/c(.*?)</a>") as String), "<a>Cliquez pour agrandir...</a>"])
                print(message)
                let id:String = (try! blocks.get(i).getElementsByClass("u-anchorTarget").get(0).getAttributes()?.get(key: "id").remove(text: ["convMessage-"]))!
                let attrsec = try! NSAttributedString(
                    data: "<style>body { font-size: 15; font-family: Helvetica;}</style><body>\(message)</body>".data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                self.messageList.append(MockMessage(attributedText: attrsec, sender: Sender(id: id_auteur, displayName: auteur), messageId: "\(id)", date: Date(timeIntervalSinceNow: 0)))
            }
            self.messagesCollectionView.reloadData()
            self.messagesCollectionView.scrollToBottom()
        }
    }
    
    @objc func load_more() {
        self.refreshControl.beginRefreshing()
        page_cur -= 1
        if page_cur < 0 { self.refreshControl.endRefreshing(); return }
        var j:Int = 0
        session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/page-\(page_cur)")!).responseString { (repdata) in
            let document:Document = try! SwiftSoup.parse(repdata.result.value!)
            let blocks:Elements = try! document.getElementsByClass("message message--conversationMessage   js-message")
            print("Start")
            do {
                if try document.getElementsByClass("pageNav-page--current").array().count > 0 {
                    self.page_cur = Int(try document.getElementsByClass("pageNav-page--current").get(0).text())!//existe pas check
                }
            }
            catch { self.page_cur = 0 }
            for i in 0 ..< blocks.array().count {
                let auteur:String = (try! blocks.get(i).getAttributes()?.get(key: "data-author"))!
                let id_auteur:String = (try! blocks.get(i).getElementsByClass("message-name").get(0).getElementsByClass("username").get(0).getAttributes()?.get(key: "data-user-id"))!
                let date:String = try! blocks.get(i).getElementsByClass("u-dt").get(0).text()
                var toreplace:String = ""
                do {
                    toreplace = try blocks.get(i).getElementsByClass("message-body js-selectToQuote").get(0).getElementsByClass("bbCodeBlock bbCodeBlock--expandable bbCodeBlock--quote").html()
                }
                catch {
                    
                }
                var message:String = try! blocks.get(i).getElementsByClass("message-body js-selectToQuote").html().stringByReplacingFirstOccurrenceOfString(target: toreplace, withString: "").remove(text: ["&nbsp;", "<div class=\"js-selectToQuoteEnd\">", "<div class=\"bbWrapper\">", "</div>", "\n", "\r", "\t"])
                message = message.remove(text: [(GetElement(input: message as NSString, paternn: "<div class=\"bbCodeBlock-expandContent\">(.*?)</a>") as String), (GetElement(input: message as NSString, paternn: "<a href=\"/goto/c(.*?)</a>") as String)])
                let id:String = (try! blocks.get(i).getElementsByClass("u-anchorTarget").get(0).getAttributes()?.get(key: "id").remove(text: ["convMessage-"]))!
                let attrsec = try! NSAttributedString(
                    data: "<style>body { font-size: 15; font-family: Helvetica;}</style><body>\(message)</body>".data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                self.messageList.insert(MockMessage(attributedText: attrsec, sender: Sender(id: id_auteur, displayName: auteur), messageId: "\(id)", date: Date(timeIntervalSinceNow: 0)), at: j)
                j += 1
            }
            self.messagesCollectionView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    // MARK: - Keyboard Style
    
    func iMessage() {
        defaultStyle()
    }
    
    func defaultStyle() {
        let newMessageInputBar = MessageInputBar()
        newMessageInputBar.sendButton.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
        newMessageInputBar.backgroundView.backgroundColor = .white
        newMessageInputBar.inputTextView.borderColor = .white
        newMessageInputBar.inputTextView.placeholder = "Quick reply..."
        newMessageInputBar.sendButton.addTarget(self, action: #selector(self.sendMsg), for: .touchUpInside)
        //newMessageInputBar.delegate = self
        messageInputBar = newMessageInputBar
        reloadInputViews()
    }
    
    @objc func sendMsg() {
        session.request(URL(string: "https://\(domain_name)/conversations/\(conv_id)/add-reply")!, method: .post, parameters: ["message_html": self.messageInputBar.inputTextView.text.replacingOccurrences(of: "\n", with: "<br>"), "_xfToken": _xfToken, "_xfResponseType": "json"]).responseString { (repdata) in
            print(repdata.result.value)
            if (repdata.result.value?.contains("Vos changements sont correctement"))! {
                let attrsec = try! NSAttributedString(
                    data: "<style>body { font-size: 15; font-family: Helvetica;}</style><body>\(self.messageInputBar.inputTextView.text.replacingOccurrences(of: "\n", with: "<br>"))</body>".data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                let message = MockMessage(attributedText: attrsec, sender: self.currentSender(), messageId: UUID().uuidString, date: Date())
                self.messageList.append(message)
                self.messagesCollectionView.insertSections([self.messageList.count - 1])
                self.messageInputBar.inputTextView.clear()
                self.messagesCollectionView.scrollToBottom()
            } else if (repdata.result.value?.contains("Vous devez attendre au moins"))! {
                let alert = MessageView.viewFromNib(layout: .cardView)
                var config = SwiftMessages.defaultConfig
                alert.button?.isHidden = true
                SwiftMessages.hideAll()
                config.duration = .seconds(seconds: 2)
                alert.configureTheme(.error)
                alert.configureDropShadow()
                alert.configureContent(title: "Error", body: "Please wait 12 seconds before do that !")
                SwiftMessages.show(config: config, view: alert)
            }else {
                //error
                let alert = MessageView.viewFromNib(layout: .cardView)
                var config = SwiftMessages.defaultConfig
                alert.button?.isHidden = true
                SwiftMessages.hideAll()
                config.duration = .seconds(seconds: 2)
                alert.configureTheme(.error)
                alert.configureDropShadow()
                alert.configureContent(title: "Error", body: "The message was not sended !")
                SwiftMessages.show(config: config, view: alert)
            }
        }
    }
    
    // MARK: - Helpers
    
    func makeButton(named: String) -> InputBarButtonItem {
        return InputBarButtonItem()
            .configure {
                $0.spacing = .fixed(10)
                $0.image = UIImage(named: named)?.withRenderingMode(.alwaysTemplate)
                $0.setSize(CGSize(width: 30, height: 30), animated: true)
            }.onSelected {
                $0.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
            }.onDeselected {
                $0.tintColor = UIColor.lightGray
            }.onTouchUpInside { _ in
                print("Item Tapped")
        }
    }
}

// MARK: - MessagesDataSource

extension ConvViewController: MessagesDataSource {
    
    func currentSender() -> Sender {
        return Sender(id: Profile.id!, displayName: Profile.pseudo!)
    }
    
    func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }
    
    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        struct ConversationDateFormatter {
            static let formatter: DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateStyle = .medium
                return formatter
            }()
        }
        let formatter = ConversationDateFormatter.formatter
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
}

// MARK: - MessagesDisplayDelegate

extension ConvViewController: MessagesDisplayDelegate {
    
    // MARK: - Text Messages
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .white : .darkText
    }
    
    
    
    func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedStringKey : Any] {
        return MessageLabel.defaultAttributes
    }
    
    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.url, .address, .phoneNumber, .date]
    }
    
    // MARK: - All Messages
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1) : UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
        //        let configurationClosure = { (view: MessageContainerView) in}
        //        return .custom(configurationClosure)
    }
    
    func avatarL(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> String {
        return get_avatar_link(id: message.sender.id)
    }
    
}

// MARK: - MessagesLayoutDelegate

extension ConvViewController: MessagesLayoutDelegate {
    
    func avatarPosition(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> AvatarPosition {
        return AvatarPosition(horizontal: .natural, vertical: .messageBottom)
    }
    
    func messagePadding(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIEdgeInsets {
        if isFromCurrentSender(message: message) {
            return UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 4)
        } else {
            return UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 30)
        }
    }
    
    func cellTopLabelAlignment(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> LabelAlignment {
        if isFromCurrentSender(message: message) {
            return .messageTrailing(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10))
        } else {
            return .messageLeading(UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
        }
    }
    
    func cellBottomLabelAlignment(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> LabelAlignment {
        if isFromCurrentSender(message: message) {
            return .messageLeading(UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
        } else {
            return .messageTrailing(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10))
        }
    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        
        return CGSize(width: messagesCollectionView.bounds.width, height: 5)
    }
    
    // MARK: - Location Messages
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 200
    }
    
}

// MARK: - MessageCellDelegate

extension ConvViewController: MessageCellDelegate {
    
    func didTapAvatar(in cell: MessageCollectionViewCell) {
        print("Avatar tapped")
    }
    
    func didTapMessage(in cell: MessageCollectionViewCell) {
        print("Message tapped")
    }
    
    func didTapTopLabel(in cell: MessageCollectionViewCell) {
        print("Top label tapped")
    }
    
    func didTapBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom label tapped")
    }
    
}

// MARK: - MessageLabelDelegate

extension ConvViewController: MessageLabelDelegate {
    
    func didSelectAddress(_ addressComponents: [String : String]) {
        print("Address Selected: \(addressComponents)")
    }
    
    func didSelectDate(_ date: Date) {
        print("Date Selected: \(date)")
    }
    
    func didSelectPhoneNumber(_ phoneNumber: String) {
        print("Phone Number Selected: \(phoneNumber)")
    }
    
    func didSelectURL(_ url: URL) {
        print("URL Selected: \(url)")
    }
    
}

// MARK: - MessageInputBarDelegate

extension ConvViewController: MessageInputBarDelegate {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        
        reloadInputViews()

    }
}
