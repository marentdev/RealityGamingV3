//
//  ConversationsViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 18/01/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import SwiftSoup
import SideMenu
import PullToRefreshKit
class ConversationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var conv_data:[[String]] = []
    var tableview:UITableView = UITableView()
    var current:Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        section_active = [false, false, false]
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height + self.navigationController!.navigationBar.height
        self.tableview = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height - barHeight))
        self.tableview.estimatedRowHeight = 96
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.view.addSubview(self.tableview)
        self.navigationController?.viewControllers = [self]
        self.navigationItem.title = "Conversations"
        let btn = UIBarButtonItem(image: UIImage.fontAwesomeIcon(name: "", textColor: .black, size: CGSize(width: 30, height: 35)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openMenu))
        btn.tintColor = .black
        self.navigationItem.leftBarButtonItems = [btn]
        let creat = UIBarButtonItem(image: UIImage.fontAwesomeIcon(name: "", textColor: .black, size: CGSize(width: 25, height: 25)), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openModal))
        creat.tintColor = .black
        self.navigationItem.rightBarButtonItems = [creat]
    }
    @objc func openMenu() {
        self.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    @objc func openModal() {
        self.performSegue(withIdentifier: "opencreate", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showWaitOverlayWithText("Loading...")
        setupPage()
    }
    
    func setupPage() {
        conv_data = []
        session.request(URL(string: "https://\(domain_name)/conversations")!).responseString { (repdata) in
            let document:Document = try! SwiftSoup.parse(repdata.result.value!)
            let blocks:Elements = try! document.getElementsByClass("structItem--conversation")
            for i in 0 ..< blocks.array().count {
                var temp_data:[String] = [] // ID Title auteur participants lastactive
                temp_data.append(( try! blocks.get(i).getElementsByAttributeValue("type", "checkbox").get(0).getAttributes()?.get(key: "value"))!)
                temp_data.append(try! blocks.get(i).getElementsByClass("structItem-title").get(0).text())
                temp_data.append((try! blocks.get(i).getAttributes()?.get(key: "data-author"))!)
                temp_data.append(try! blocks.get(i).getElementsByClass("listInline listInline--comma listInline--selfInline").get(0).text())
                temp_data.append(try! blocks.get(i).getElementsByClass("structItem-latestDate u-dt").get(0).text())
                self.conv_data.append(temp_data)
            }
            self.tableview.reloadData()
            if self.conv_data.count <= 24 {
                self.tableview.switchRefreshFooter(to: .noMoreData)
            }else {
                self.tableview.configRefreshFooter(with: DefaultRefreshFooter.footer(), action: {
                    self.current += 1
                    self.nextSetupPage(index: self.current)
                })
            }
            self.removeAllOverlays()
        }
    }
    
    func nextSetupPage(index:Int) {
        var temp:[[String]] = []
        session.request(URL(string: "https://\(domain_name)/conversations/page-\(index)")!).responseString { (repdata) in
            let document:Document = try! SwiftSoup.parse(repdata.result.value!)
            let blocks:Elements = try! document.getElementsByClass("structItem--conversation")
            for i in 0 ..< blocks.array().count {
                var temp_data:[String] = [] // ID Title auteur participants lastactive
                temp_data.append(( try! blocks.get(i).getElementsByAttributeValue("type", "checkbox").get(0).getAttributes()?.get(key: "value"))!)
                temp_data.append(try! blocks.get(i).getElementsByClass("structItem-title").get(0).text())
                temp_data.append((try! blocks.get(i).getAttributes()?.get(key: "data-author"))!)
                temp_data.append(try! blocks.get(i).getElementsByClass("listInline listInline--comma listInline--selfInline").get(0).text())
                temp_data.append(try! blocks.get(i).getElementsByClass("structItem-latestDate u-dt").get(0).text())
                if Int((repdata.response?.url?.absoluteString.components(separatedBy: "-").last!)!) == self.current - 1 {
                    self.tableview.switchRefreshFooter(to: .noMoreData)
                    return
                }else {
                    self.conv_data.append(temp_data)
                    temp.append(temp_data)
                }
            }
            self.tableview.reloadData()
            if Int((repdata.response?.url?.absoluteString.components(separatedBy: "-").last!)!) == self.current - 1 {
                self.tableview.switchRefreshFooter(to: .noMoreData)
            }else {
                self.tableview.reloadData()
                self.tableview.scrollToRow(at: IndexPath(row: self.conv_data.count - 1 - temp.count, section: self.tableview.numberOfSections - 1), at: .bottom, animated: true)
                self.tableview.switchRefreshFooter(to: .normal)
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.conv_data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "ConvTableViewCell", bundle: nil), forCellReuseIdentifier: "conv")
        let cell = tableView.dequeueReusableCell(withIdentifier: "conv", for: indexPath) as! ConvTableViewCell
        cell.title.text = self.conv_data[indexPath.row][1]
        cell.participants.text = self.conv_data[indexPath.row][3]
        cell.last.text = self.conv_data[indexPath.row][4]
        getLinkAvatarFormPseudo(username: self.conv_data[indexPath.row][2], imageView: cell.avatar)
        cell.avatar.layer.masksToBounds = true
        cell.avatar.layer.cornerRadius = cell.avatar.layer.frame.height / 2
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableview.deselectRow(at: indexPath, animated: true)
        conv_id = self.conv_data[indexPath.row][0]
        conv_title = self.conv_data[indexPath.row][1]
        self.performSegue(withIdentifier: "convview", sender: self)
    }
}
