//
//  ChatboxMSGTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 26/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import ActiveLabel
class ChatboxMSGTableViewCell: UITableViewCell {

    @IBOutlet weak var pseudo_height: NSLayoutConstraint!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var pseudo: UILabel!
    @IBOutlet weak var content: ActiveLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func sameUser() {
        self.pseudo_height.constant = 0
        self.pseudo.isHidden = true
        self.avatar.isHidden = true
    }
    
    public func otherUser() {
        self.pseudo_height.constant = 21
        self.pseudo.isHidden = false
        self.avatar.isHidden = false
    }
}
