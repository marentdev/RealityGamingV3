//
//  ChatboxViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 07/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import Alamofire
import SwifterSwift
import SwiftyJSON
import SwiftSoup
import ActiveLabel
import Repeat
import NVActivityIndicatorView
class ChatboxViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIWebViewDelegate {

    @IBOutlet weak var navBar: NavBarCustom!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bot_input: NSLayoutConstraint!
    @IBOutlet weak var input: UITextField!
    
    struct message {
        var author:NSAttributedString?
        var author_id:String?
        var clean_auth:String?
        var id:Any?
        var content:NSAttributedString?
        var date:String?
    }
    
    var messages:[message] = []
    var timer:Repeater?
    let webView:UIWebView = UIWebView()
    var loading:NVActivityIndicatorView?
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = NVActivityIndicatorView(frame: CGRect(x: (self.view.bounds.maxX / 2) - 25, y: (self.view.bounds.maxY / 2) - 25, width: 50, height: 50), type: NVActivityIndicatorType.ballClipRotate, color: UIColor(hex: 0x1E8CE0))
        self.view.addSubview(loading!)
        loading?.startAnimating()
        self.webView.delegate = self
        self.webView.loadRequest(URLRequest(url: URL(string: "https://\(domain_name)/shoutbox")!))
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        self.navBar.where.text = "RealityGaming"
        self.navBar.here.text = "Shoutbox"
        self.navBar.hideArrowBack()
        self.input.placeholder = "Entrer un message..."
        self.input.returnKeyType = .send
        self.input.delegate = self
        self.tableView.separatorColor = .clear
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.hideKeyboardWhenTappedAround()
        self.setupPage()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if !webView.isLoading {
            //end
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .send && !textField.isEmpty {
            webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('overlay-container')[document.getElementsByClassName('overlay-container').length - 1].click();")
            webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('input-shoutbox')[0].value = '\(textField.text!.replacingOccurrences(of: "\"", with: "\\\"").replacingOccurrences(of: "\'", with: "\\\'"))';")
            webView.stringByEvaluatingJavaScript(from: "const event = new KeyboardEvent(\"keypress\", {view: window,keyCode: 13,bubbles: true,cancelable: true});")
            webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('input-shoutbox')[0].dispatchEvent(event);")
            textField.clear()
            self.updateChat()
            return true
        }
        return false
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.navBar.addGestureRecognizer(tap)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.bot_input?.constant = 0.0
            } else {
                self.bot_input?.constant = (endFrame?.size.height ?? 0.0) - 49
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.timer?.start()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.timer?.pause()
    }
    
    func setupPage() {
        messages = []
        session.request(URL(string: "https://\(domain_name)/shoutbox/load-messages")!, method: .post, parameters: ["_xfToken": _xfToken, "_xfResponseType":"json"]).validate().responseJSON { (data) in
            let json:JSON = JSON(data.result.value!)
            if json["html"].exists() && json["messagesIds"].exists() {
                do {
                    let document:Document = try SwiftSoup.parse(json["html"].string!)
                    let block_msg:Elements = try document.getElementsByTag("li")
                    var i = 0
                    block_msg.forEach({ (elem:Element) in
                        self.messages.append(self.parse_msg(elem: elem, i: i, json: json))
                        i += 1
                    })
                    self.tableView.reloadData()
                    self.timer = Repeater(interval: .seconds(5), mode: .infinite) { _ in
                        DispatchQueue.global(qos: .background).async {
                            self.updateChat()
                        }
                    }
                    self.loading?.stopAnimating()
                    self.loading?.isHidden = false
                    self.timer?.start()
                } catch Exception.Error(let type, let message) {
                    print(type, message)
                } catch {
                    print("error")
                }
            }
        }
    }

    func updateChat() {
        session.request(URL(string: "https://\(domain_name)/shoutbox/load-messages")!, method: .post, parameters: ["_xfToken": _xfToken, "_xfResponseType":"json"]).validate().responseJSON { (data) in
            let json:JSON = JSON(data.result.value ?? "")
            if json["html"].exists() && json["messagesIds"].exists() {
                do {
                    let document:Document = try SwiftSoup.parse(json["html"].string!)
                    let block_msg:Elements = try document.getElementsByTag("li")
                    var i = 0
                    var temps:[message] = []
                    var new = false
                    var edit = false
                    block_msg.forEach({ (elem:Element) in
                        let msg = self.parse_msg(elem: elem, i: i, json: json)
                        if msg.id as! Int > self.messages[0].id as! Int {
                            //Gestion d'un new message
                            temps.append(msg)
                            new = true
                        }
                        for j in 0 ..< self.messages.count {
                            if msg.id as! Int == self.messages[j].id as! Int {
                                //Gestion de l'edit
                                if self.messages[j].content != msg.content &&
                                    msg.id as! Int == self.messages[0].id as! Int{
                                    self.messages[j].content = msg.content
                                    edit = true
                                }
                            }
                        }
                        i += 1
                    })
                    if new {
                        self.messages.insert(temps, at: 0)
                        self.tableView.reloadData()
                    }else if edit {
                        self.tableView.reloadData()
                    }
                } catch Exception.Error(let type, let message) {
                    print(type, message)
                } catch {
                    print("error")
                }
            }
        }
    }
    func parse_msg(elem:Element, i:Int, json:JSON) -> message {
        var ms = message()
        do {
            let style = try elem.getElementsByAttributeValueMatching("class", "username--style").get(0).attr("class").remove(text: ["username--staff", "username--moderator", "username--admin", " "])
            ms.author = stringFromHtml(string: "<div style=\"font-weight:bold;font-size:16px;font-family:'Avenir';color:\(getColorRank(rank: Int(style.remove(text: ["username--style"])) ?? 0).hexString);\">\(returnPseudo(pseudo: try elem.getElementsByClass("username").get(0).text(), style: style.remove(text: ["username--"])))</div>")
            ms.clean_auth = try elem.getElementsByClass("username").get(0).text()
            ms.author_id = try elem.getElementsByAttribute("data-user-id").get(0).attr("data-user-id")
            ms.id = json["messagesIds"].arrayObject?[i]
            ms.content = parse_msg_html(elem: try elem.getElementsByClass("shoutbox-message-content").get(0))
        } catch Exception.Error(let type, let message) {
            print(type, message)
        } catch {
            print("error")
        }
        return ms
    }
    
    func parse_msg_html(elem:Element) -> NSAttributedString {
        let all = NSMutableAttributedString()
        do {
            try elem.getElementsByClass("smilie").forEach({ (txt) in
                try txt.html(try txt.attr("alt"))
            })
            let complete = try elem.text()
            all.append(NSAttributedString(string: complete))
            return all
        } catch Exception.Error(let type, let message) {
            print(type, message)
        } catch {
            print("error")
        }
        return all
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.register(UINib(nibName: "ChatboxMSGTableViewCell", bundle: nil), forCellReuseIdentifier: "msg")
        let msg = tableView.dequeueReusableCell(withIdentifier: "msg", for: indexPath) as! ChatboxMSGTableViewCell
        let data = messages[indexPath.row]
        msg.avatar.maskToBounds = true
        msg.avatar.cornerRadius = 5
        msg.pseudo.attributedText = data.author
        msg.content.enabledTypes = [.url]
        msg.content.attributedText = data.content
        loadImageFromUrl(url: get_avatar_link(id: data.author_id ?? "0"), view: msg.avatar)
        return msg
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.input.text =  "\(self.input.text ?? "")@\(self.messages[indexPath.row].clean_auth ?? "") "
    }
    
}
