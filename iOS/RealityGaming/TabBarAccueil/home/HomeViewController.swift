//
//  HomeViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 07/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import Alamofire
import SwifterSwift
import SwiftyJSON
import SwiftSoup
import PullToRefreshKit
import NVActivityIndicatorView

var categories:[HomeViewController.category] = []
class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var navBar: NavBarCustom!
    @IBOutlet weak var collectionView: UICollectionView!
    
    struct forum {
        var name:String?
        var infos:NSAttributedString?
        var lastActivity:String?
        var id:String?
        var isCategories:Bool = false
		var img:UIImage?
    }
    
    struct category {
        var name:String?
        var description:String?
        var id:String?
        var forums:[forum] = []
    }
    
    var loading:NVActivityIndicatorView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.here.text = "Home"
        self.navBar.where.text = "Forums"
        loading = NVActivityIndicatorView(frame: CGRect(x: (self.view.bounds.maxX / 2) - 25, y: (self.view.bounds.maxY / 2) - 25, width: 50, height: 50), type: NVActivityIndicatorType.ballClipRotate, color: UIColor(hex: 0x1E8CE0))
        self.loading?.startAnimating()
        self.view.addSubview(loading!)
        collectionView.register(UINib(nibName: "HeaderForumCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "header")
        collectionView.register(UINib(nibName: "ContentForumCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "content")
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        }
        self.navBar.hideArrowBack()
		
		self.collectionView.isHidden = true
		self.loading?.isHidden = false
		self.loading?.startAnimating()
		categories.removeAll()
		self.setupPage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setupPage() {
        session.request(URL(string: "https://\(domain_name)/")!).validate().responseString { (datarep) in
            do {
                let document:Document = try SwiftSoup.parse(datarep.result.value ?? "")
                let blockCategory = try document.getElementsByAttributeValueContaining("class", "block block--category block--category")
                try blockCategory.forEach({ (catego) in
                    var cat = category()
                    cat.name = try catego.getElementsByClass("block-header").get(0).getElementsByTag("a").get(0).text()
                    cat.description = try catego.getElementsByClass("block-header").get(0).getElementsByClass("block-desc").get(0).text()
                    cat.id = try catego.attr("class").remove(text: ["block block--category block--category", " "])
                    let blockForum = try catego.getElementsByAttributeValueMatching("class", "node node--id")
                    try blockForum.forEach({ (elem) in
                        var fofo = forum()
                        fofo.name = try elem.getElementsByClass("node-title").get(0).text()
                        fofo.id = self.parseFofoId(classId: try elem.attr("class"))
                        let msg = try elem.getElementsByClass("node-stats").get(0).getElementsByTag("dl").get(1).getElementsByTag("dd").get(0).text()
                        let disc = try elem.getElementsByClass("node-stats").get(0).getElementsByTag("dl").get(0).getElementsByTag("dd").get(0).text()
						fofo.infos = stringFromHtml(string: "<div style=\"font-family: 'Avenir Medium'; font-size: 12px; color: #3A3939;\"><span style=\"font-family:'Font Awesome 5 Free';\">\u{f086}</span> \(disc)&nbsp;&nbsp;&nbsp;<span style=\"font-family:'Font Awesome 5 Free';\">\u{f4ad}</span> \(msg)</div>")
                        fofo.isCategories = try elem.attr("class").contains("node--category")
                        fofo.lastActivity = try elem.getElementsByClass("u-dt").get(0).attr("datetime")
						fofo.img = self.getImgForum(fofo.id ?? "0")
                        cat.forums.append(fofo)
                    })
                    categories.append(cat)
                    self.collectionView.reloadData {
                        self.loading?.stopAnimating()
                        self.loading?.isHidden = true
                        self.collectionView.isHidden = false
                    }
                })
            }catch Exception.Error(let type, let message) {
                print(type, message)
            } catch {
                print("error")
            }
        }
    }
	
	func getImgForum(_ id:String) -> UIImage {
		switch id {
		case "3":
			return #imageLiteral(resourceName: "home")
		case "17":
			return #imageLiteral(resourceName: "discussions")
		case "443":
			return #imageLiteral(resourceName: "informatique")
		default:
			return UIImage()
		}
	}
    
    func parseFofoId(classId:String) -> String {
        let range = classId.range(of: "node--id")
        var id = ""
        if range?.upperBound.encodedOffset == nil { return "0" }
        for i in (range?.upperBound.encodedOffset)! ..< classId.count {
            if classId.charactersArray[i].isNumber {
                id += "\(classId.charactersArray[i])"
            }else {
                break
            }
        }
        return id
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count * 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row % 2 == 1 {
            let content = collectionView.dequeueReusableCell(withReuseIdentifier: "content", for: indexPath) as! ContentForumCollectionViewCell
            content.tableView.tag = indexPath.row / 2
			content.tableView.reloadData()
			content.heightConstrainte.constant = CGFloat(51.6 * Double(categories[indexPath.row / 2].forums.count))
			content.tableView.isScrollEnabled = false
			content.tableView.maskToBounds = true
			content.tableView.cornerRadius = 5
			content.onCellSelected = self.didSelectRow
            return content
        }else {
            let header = collectionView.dequeueReusableCell(withReuseIdentifier: "header", for: indexPath) as! HeaderForumCollectionViewCell
            header.contentALL.cornerRadius = 5
            header.contentALL.maskToBounds = true
            header.titre.text = categories[indexPath.row / 2].name
            return header
        }
    }
	
	func didSelectRow(indexPath:IndexPath, tag:Int) {
		let forum = categories[tag].forums[indexPath.row]
		if forum.isCategories {
			
		}else {
			
		}
	}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
