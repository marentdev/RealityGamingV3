//
//  ContentForumCollectionViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 29/04/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import SwifterSwift
class ContentForumCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var witdhConstrainte: NSLayoutConstraint!
	@IBOutlet weak var heightConstrainte: NSLayoutConstraint!
	
	var onCellSelected:((IndexPath, Int) -> Void)?
	
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        witdhConstrainte.constant = screenWidth - (2 * 12)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
	
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories[tableView.tag].forums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "ForumContentTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ForumContentTableViewCell
        cell.titre.text = categories[tableView.tag].forums[indexPath.row].name
		cell.icone.image = categories[tableView.tag].forums[indexPath.row].img
		cell.infos.attributedText = categories[tableView.tag].forums[indexPath.row].infos
        return cell
    }
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.onCellSelected?(indexPath, tableView.tag)
	}

}
