//
//  HeaderForumCollectionViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 29/04/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class HeaderForumCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contentALL: UIView!
    @IBOutlet weak var titre: UILabel!
    @IBOutlet weak var icon: UILabel!
    @IBOutlet weak var widthConstrainte: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        widthConstrainte.constant = screenWidth - (2 * 12)
    }

}
