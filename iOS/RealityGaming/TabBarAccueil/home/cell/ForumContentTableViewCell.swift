//
//  ForumContentTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 29/04/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class ForumContentTableViewCell: UITableViewCell {

    @IBOutlet weak var titre: UILabel!
    @IBOutlet weak var infos: UILabel!
    @IBOutlet weak var icone: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
