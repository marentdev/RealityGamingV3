//
//  TabBarAccueilViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 07/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import SwifterSwift
import Repeat
import Alamofire
import SwiftyJSON
class TabBarAccueilViewController: UITabBarController {
    
    var timer:Repeater?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.items![0].image = #imageLiteral(resourceName: "icon-home")
        self.tabBar.items![1].image = #imageLiteral(resourceName: "icon-chatbox")
        self.tabBar.items![2].image = #imageLiteral(resourceName: "icon-newsfeed").withRenderingMode(.alwaysOriginal)
        self.tabBar.items![3].image = #imageLiteral(resourceName: "icon-conversations")
        self.tabBar.items![4].image = #imageLiteral(resourceName: "icon-alerts")
        self.tabBar.tintColor = UIColor(hex: 0x1E8CE0)
        self.tabBar.borderColor = UIColor(hex: 0xE8E8E8)
        self.tabBar.shadowColor = .clear
        self.tabBar.clipsToBounds = true
        self.tabBar.borderWidth = 1.0
        self.tabBar.items?.forEach({ (itembar) in
            itembar.title = ""
            itembar.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
            if #available(iOS 10.0, *) {
                itembar.badgeColor = UIColor(hex: 0x1E8CE0)
            } else {
                // Fallback on earlier versions
            }
        })
        timer = Repeater(interval: .seconds(5), mode: .infinite) { _ in
            self.checkAll()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.timer?.start()
        self.timer?.fire()
    }
    
    public func firecheck() {
        self.timer?.fire()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func checkAll() {
        session.request(URL(string: "https://\(domain_name)/compte/visitor-menu?_xfRequestUri=%2F&_xfWithData=1&_xfToken=\(_xfToken)&_xfResponseType=json")!).responseJSON { (data) in
            let json:JSON = JSON(data.result.value ?? "")
            if json["visitor"].exists() {
                if json["visitor"]["conversations_unread"].exists() {
                    if let nb = Int(json["visitor"]["conversations_unread"].string ?? "") {
                        if nb > 0 { self.setBadgeConv(nb) } else {
                            self.tabBar.items![3].badgeValue = nil
                        }
                    }
                }
                if json["visitor"]["alerts_unread"].exists() {
                    if let nb = Int(json["visitor"]["alerts_unread"].string ?? "") {
                        if nb > 0 { self.setBadgeAlert(nb) } else {
                            self.tabBar.items![4].badgeValue = nil
                        }
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func setBadgeAlert(_ nb:Int) {
        self.tabBar.items![4].badgeValue = "\(nb)"
    }
    
    public func setBadgeConv(_ nb:Int) {
        self.tabBar.items![3].badgeValue = "\(nb)"
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
    }
    
}
