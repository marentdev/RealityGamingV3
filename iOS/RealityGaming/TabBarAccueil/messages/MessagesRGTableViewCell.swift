//
//  MessagesRGTableViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 08/04/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class MessagesRGTableViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var infos: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var backAvatar: UIView!
    @IBOutlet weak var widthConstrainte: NSLayoutConstraint!
    @IBOutlet weak var contentALL: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        widthConstrainte.constant = screenWidth - (2 * 12)
    }

    
    
}
