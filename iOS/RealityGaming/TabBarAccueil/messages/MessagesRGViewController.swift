//
//  MessagesRGViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 07/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import Alamofire
import SwifterSwift
import SwiftyJSON
import SwiftSoup
import PullToRefreshKit
import NVActivityIndicatorView
class MessagesRGViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var navBar: NavBarCustom!
    @IBOutlet weak var collectionView: UICollectionView!
    
    struct conversation {
        var name:String?
        var id:String?
        var authorId:String?
        var infos:NSAttributedString?
    }
    
    var conversations:[conversation] = []
    var currentIndex:Int = 1
    var loading:NVActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loading = NVActivityIndicatorView(frame: CGRect(x: (self.view.bounds.maxX / 2) - 25, y: (self.view.bounds.maxY / 2) - 25, width: 50, height: 50), type: NVActivityIndicatorType.ballClipRotate, color: UIColor(hex: 0x1E8CE0))
        self.loading?.startAnimating()
        self.view.addSubview(loading!)
        collectionView.register(UINib(nibName: "MessagesRGTableViewCell", bundle: nil), forCellWithReuseIdentifier: "message")
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        }
        self.navBar.here.text = "Vos conversations"
        self.navBar.hideArrowBack()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.currentIndex = 1
        self.collectionView.isHidden = true
        self.loading?.isHidden = false
        self.loading?.startAnimating()
        self.conversations.removeAll()
        self.setupPage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.conversations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let messages = collectionView.dequeueReusableCell(withReuseIdentifier: "message", for: indexPath) as! MessagesRGTableViewCell
        messages.backgroundColor = .clear
        messages.contentALL.maskToBounds = true
        messages.contentALL.cornerRadius = 3
        messages.dropShadow(color: UIColor(hex: 0xDDDDDD), offSet: CGSize(width: 0, height: 2))
        messages.avatar.maskToBounds = true
        messages.avatar.cornerRadius = 3
        messages.title.text = self.conversations[indexPath.row].name
        loadImageFromUrl(url: get_avatar_link(id: self.conversations[indexPath.row].authorId ?? "0"), view: messages.avatar)
        messages.infos.attributedText = self.conversations[indexPath.row].infos
        
        return messages
    }
    
    func setupPage() {
        session.request(URL(string: "https://\(domain_name)/conversations?page=\(currentIndex)")!).validate().responseString { (repdata) in
            do {
                let document = try SwiftSoup.parse(repdata.result.value ?? "")
                let blockConv:Elements = try document.getElementsByClass("structItem--conversation")
                try blockConv.forEach({ (conv) in
                    var stckConv = conversation()
                    stckConv.name = try conv.getElementsByClass("structItem-title").get(0).text()
                    stckConv.id = try conv.getElementsByClass("js-inlineModToggle").get(0).attr("value")
                    stckConv.authorId = try conv.getElementsByClass("avatar").get(0).attr("data-user-id")
                    stckConv.infos = stringFromHtml(string: "<div style=\"font-size:13px;font-family:'Avenir-Light';color:#999999;\">\(try conv.getElementsByClass("structItem-parts").get(0).text().remove(text: [try conv.getElementsByClass("structItem-startDate").get(0).text()]))</div>")
                    self.conversations.append(stckConv)
                })
                if blockConv.array().count < 0 {
                    self.collectionView.switchRefreshFooter(to: .noMoreData)
                }else {
                    self.collectionView.reloadData {
                        if self.currentIndex == 1 {
                            self.collectionView.configRefreshFooter(with: DefaultRefreshFooter.footer()) {
                                self.currentIndex += 1
                                self.setupPage()
                            }
                        }else if self.currentIndex > 1 {
                            self.collectionView.switchRefreshFooter(to: .normal)
                            self.collectionView.scrollToItem(at:IndexPath.init(row: self.conversations.count - 1 - blockConv.array().count, section: 0), at: .bottom, animated: false)
                        }
                    }
                }
                self.collectionView.isHidden = false
                self.loading?.stopAnimating()
                self.loading?.isHidden = false
                let tab = self.tabBarController as! TabBarAccueilViewController
                tab.firecheck()
            }catch Exception.Error(let type, let message) {
                print(type, message)
            } catch {
                print("error")
            }
        }
    }
    
}
