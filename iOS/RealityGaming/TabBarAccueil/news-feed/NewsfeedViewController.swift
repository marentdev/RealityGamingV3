//
//  NewsfeedViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 07/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class NewsfeedViewController: UIViewController {

    @IBOutlet weak var navBar: NavBarCustom!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navBar.here.text = "News feed"
        self.navBar.hideArrowBack()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
