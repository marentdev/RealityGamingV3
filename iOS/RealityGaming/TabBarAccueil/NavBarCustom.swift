//
//  NavBarCustom.swift
//  RealityGaming
//
//  Created by Marentdev on 07/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class NavBarCustom: UIView {
    
    @IBOutlet weak var arrowWidth: NSLayoutConstraint!
    @IBOutlet weak var here: UILabel!
    @IBOutlet weak var `where`: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet var contentView: NavBarCustom!
    @IBOutlet weak var arrowBack: UIButton!
    private var handleAvatar:((UIImageView) -> ())?
    private var handleBack:(() -> ())?
    var imageView:UIImageView = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        updateInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateInit()
    }
    
    private func updateInit() {
        Bundle.main.loadNibNamed("NavBarCustom", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        imageView.frame = self.bounds
        imageView.width = UIScreen.main.bounds.width
        imageView.image = #imageLiteral(resourceName: "gaming-pattern")
        contentView.insertSubview(imageView, at: 0)
        avatar.dropShadow(color: UIColor(hex: 0x0B74C4), offSet: CGSize(width: 0, height: 2))
        avatar.isUserInteractionEnabled = true
        avatar.clipsToBounds = true
        avatar.cornerRadius = 3
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapAvatar))
        avatar.addGestureRecognizer(tap)
        loadImageFromUrl(url: get_avatar_link(id: "712193"), view: avatar)
    }
    
    @objc private func tapAvatar(gesture: UITapGestureRecognizer) {
        if let av = gesture.view as? UIImageView {
            self.handleAvatar?(av)
        }
    }
    
    public func isAvatarTapped(handle: @escaping (UIImageView) -> ()) {
        handleAvatar = handle
    }
    
    @IBAction func backArrow(_ sender: UIButton) {
        self.handleBack?()
    }
    
    public func isArrowBackTapped(handle: @escaping () -> ()) {
        handleBack = handle
    }
    
    public func hideArrowBack() {
        self.arrowWidth.constant = 0
        self.arrowBack.isHidden = true
    }
    
    public func showArrowBack() {
        self.arrowWidth.constant = 30
        self.arrowBack.isHidden = true
    }
}
