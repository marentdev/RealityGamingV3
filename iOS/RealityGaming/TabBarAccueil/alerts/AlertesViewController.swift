//
//  AlertesViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 07/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import Alamofire
import SwifterSwift
import SwiftyJSON
import SwiftSoup
import PullToRefreshKit
import NVActivityIndicatorView
class AlertesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var navBar: NavBarCustom!
    @IBOutlet weak var collectionView: UICollectionView!
    
    enum direction {
        case trophies
        case profile_post
        case post
        case conv_shout
        case thread
        case mp
        case none
    }
    
    struct alerte {
        var dir:direction = .none
        var content:NSAttributedString?
        var id:String?
        var time:String?
        var id_dir:String?
        var style_id:String?
        var id_author:String?
        var author:String?
        var lnk:String?
    }
    
    var alertes:[alerte] = []
    var currentIndex:Int = 1
    var loading:NVActivityIndicatorView?
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = NVActivityIndicatorView(frame: CGRect(x: (self.view.bounds.maxX / 2) - 25, y: (self.view.bounds.maxY / 2) - 25, width: 50, height: 50), type: NVActivityIndicatorType.ballClipRotate, color: UIColor(hex: 0x1E8CE0))
        self.loading?.startAnimating()
        self.view.addSubview(loading!)
        self.navBar.here.text = "Vos alertes"
        collectionView.register(UINib(nibName: "AlertesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "alerts")
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        }
        self.navBar.hideArrowBack()
        getProfile {
            self.setupPage()
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.currentIndex = 1
        self.collectionView.isHidden = true
        self.loading?.isHidden = false
        self.loading?.startAnimating()
        self.alertes.removeAll()
        self.setupPage()
    }
    
    func setupPage() {
        session.request(URL(string: "https://\(domain_name)/compte/alerts?page=\(currentIndex)&_xfResponseType=json&_xfToken=\(_xfToken)")!).validate().responseJSON { (datarep) in
            let json:JSON = JSON(datarep.result.value!)
            if json["status"].string == "ok" {
                if let content = json["html"]["content"].string {
                    do {
                        let document:Document = try SwiftSoup.parse(content)
                        let block_alerts:Elements = try document.getElementsByAttribute("data-alert-id")
                        block_alerts.forEach({ (al) in
                            self.alertes.append(self.parseAlerte(alert: al))
                        })
                        if block_alerts.array().count < 0 {
                            self.collectionView.switchRefreshFooter(to: .noMoreData)
                        }else {
                            self.collectionView.reloadData {
                                if self.currentIndex == 1 {
                                    self.collectionView.configRefreshFooter(with: DefaultRefreshFooter.footer()) {
                                        self.currentIndex += 1
                                        self.setupPage()
                                    }
                                }else if self.currentIndex > 1 {
                                    self.collectionView.switchRefreshFooter(to: .normal)
                                    self.collectionView.scrollToItem(at: IndexPath.init(row: self.alertes.count - 1 - block_alerts.array().count, section: 0), at: .bottom, animated: false)
                                }
                            }
                        }
                        self.collectionView.isHidden = false
                        self.loading?.stopAnimating()
                        self.loading?.isHidden = false
                        let tab = self.tabBarController as! TabBarAccueilViewController
                        tab.firecheck()
                    } catch Exception.Error(let type, let message) {
                        print(type, message)
                    } catch {
                        print("error")
                    }
                }
            }else if json["status"].string == "error" {
                //erreur basic
                print("error")
            }else {
                //erreur incconue
            }
        }
    }
    
    func parseAlerte(alert:Element) -> alerte {
        var result:alerte = alerte()
        do {
            result.id = try alert.attr("data-alert-id")
            result.id_author = try alert.getElementsByClass("contentRow-figure").get(0).child(0).attr("data-user-id")
            result.time = try alert.getElementsByClass("u-dt").get(0).attr("datetime")
            result.author = try alert.getElementsByClass("username").get(0).text()
            let work_br = try alert.getElementsByClass("contentRow-main").get(0).getElementsByTag("a")
            if work_br.array().count > 0 {
                let frag = try work_br.last()?.attr("href").components(separatedBy: "/")
            }else {
                result.id_dir = "0"
                result.dir = .none
            }
            let parsing = self.parseContent(html: try alert.getElementsByClass("contentRow-main").get(0), al: result)
            if parsing.count > 1 {
                result.lnk = parsing[0] as? String
                result.content = parsing[1] as? NSAttributedString
            }
        }catch Exception.Error(let type, let message) {
            print(type, message)
        } catch {
            print("error")
        }
        return result
    }

    func parseContent(html:Element, al:alerte) -> [Any] {
        var result:[Any] = []
        do {
            try html.getElementsByClass("contentRow-minor").remove()
            try html.getElementsByClass("username").remove()
            let href = try html.getElementsByTag("a")
            if href.array().count > 0 {
                let lnk = try href.get(0).text()
                let txt = try html.text()
                var content = ""
                result.append(lnk)
                if lnk.isEmpty {
                    content = "<div style=\"font-size:14px;font-family:'Avenir-Light';color:#2B2B2B;\"><span style=\"color:\(getColorRank(rank: Int(al.style_id ?? "0") ?? 0).hexString);font-family:'Avenir-Black'\">\(al.author ?? "")</span> \(txt)</div>"
                }else {
                    let range:Range = txt.range(of: lnk)!
                    content = "<div style=\"font-size:14px;font-family:'Avenir-Light';color:#2B2B2B;\"><span style=\"color:\(getColorRank(rank: Int(al.style_id ?? "0") ?? 0).hexString);font-family:'Avenir-Black'\">\(al.author ?? "")</span> \(txt.prefix(range.lowerBound.encodedOffset))<span style=\"color:#509FDB;font-family:'Avenir-Heavy'\">\(lnk)</span>\(txt.suffix(txt.count - (range.upperBound.encodedOffset + txt.startIndex.encodedOffset)))</div>"
                }
                result.append(stringFromHtml(string: content) as Any)
                    
            }
        }catch Exception.Error(let type, let message) {
            print(type, message)
        } catch {
            print("error")
        }
        return result
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.alertes.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let alertes = collectionView.dequeueReusableCell(withReuseIdentifier: "alerts", for: indexPath) as! AlertesCollectionViewCell
        alertes.backgroundColor = .clear
        alertes.contentALL.maskToBounds = true
        alertes.contentALL.cornerRadius = 3
        alertes.dropShadow(color: UIColor(hex: 0xDDDDDD), offSet: CGSize(width: 0, height: 2))
        loadImageFromUrl(url: get_avatar_link(id: self.alertes[indexPath.row].id_author ?? "0"), view: alertes.avatar)
        alertes.avatar.maskToBounds = true
        alertes.avatar.cornerRadius = 3
        alertes.content.attributedText = self.alertes[indexPath.row].content
        alertes.date.text = self.alertes[indexPath.row].time ?? ""
        alertes.gliph.image = nil
        alertes.widthgliph.constant = 0
        alertes.content.tag = indexPath.row
        alertes.content.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped))
        alertes.content.addGestureRecognizer(tap)
        return alertes
    }
    
    @objc func labelTapped(gesture: UITapGestureRecognizer) {
        if let label = gesture.view as? UILabel {
            if gesture.didTapAttributedTextInLabel(label: label, inRange: (label.text! as NSString).range(of: self.alertes[label.tag].lnk ?? "")) {
                print("Tapped: \(self.alertes[label.tag].lnk!)")
            }else if gesture.didTapAttributedTextInLabel(label: label, inRange: (label.text! as NSString).range(of: self.alertes[label.tag].author ?? "")) {
                print("Tapped: \(self.alertes[label.tag].author!)")
            }
        }
    }

}
