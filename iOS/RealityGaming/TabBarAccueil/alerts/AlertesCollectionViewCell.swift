//
//  AlertesCollectionViewCell.swift
//  RealityGaming
//
//  Created by Marentdev on 07/03/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class AlertesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var widthConstrainte: NSLayoutConstraint!
    @IBOutlet weak var content:UILabel!
    @IBOutlet weak var contentALL:UIView!
    @IBOutlet weak var date:UILabel!
    @IBOutlet weak var gliph:UIImageView!
    @IBOutlet weak var avatar:UIImageView!
    @IBOutlet weak var widthgliph: NSLayoutConstraint!
    
    var isHeightCalculated: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        widthConstrainte.constant = screenWidth - (2 * 12)

    }


}
