//
//  cellwithicon.swift
//  RealityGaming
//
//  Created by Marentdev on 26/01/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit

class cellwithicon: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
