//
//  MessageViewDelegate.swift
//  MessageView
//
//  Created by Ryan Nystrom on 12/22/17.
//  Copyright © 2017 Ryan Nystrom. All rights reserved.
//

import UIKit

internal protocol MessageViewDelegate: class {
    func sizeDidChange(messageView: MessageViewModern)
    func wantsLayout(messageView: MessageViewModern)
    func selectionDidChange(messageView: MessageViewModern)
}
