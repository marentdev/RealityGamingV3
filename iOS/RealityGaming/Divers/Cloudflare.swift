//
// Created by Mathieu VIGNE on 12/13/17.
// Copyright (c) 2017 Marentdev. All rights reserved.
//

import Foundation
import Alamofire
import SwiftSoup
class Cloudflare_bypass {

    open func getToken(response:DataResponse<String>, closure: @escaping (Int) -> ())
    {
        if response.response!.statusCode == 503 {
            print("a")
            if response.response!.allHeaderFields["Server"] as! String == "cloudflare" {
                print("b")
                do_main(response: response, closure: { (result) in
                    if result == 1 {
                        print("Challenge OKEY ! Bypass by MarentDev 🚀")
                        closure(2)
                    }
                })
            }else {
                closure(-1)
            }
        } else {
            closure(1)
        }
    }

    private func do_main(response:DataResponse<String>, closure: @escaping (Int) -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            let document:Document = try! SwiftSoup.parse(response.result.value!)
            let url:String = "https://\(domain_name)/cdn-cgi/l/chk_jschl"
            var request:URLRequest = URLRequest(url: URL(string: url)!)
            var headers:HTTPHeaders = HTTPHeaders()
            headers["Referer"] = response.response!.url!.absoluteString
            var params:Parameters = ["jschl_vc":try! document.getElementsByAttributeValue("name", "jschl_vc").get(0).getAttributes()?.get(key: "value") as! String,
                                     "pass":try! document.getElementsByAttributeValue("name", "pass").get(0).getAttributes()?.get(key: "value") as! String]
            print("YO")
            if (params["jschl_vc"] as! String).isEmpty || (params["jschl_vc"] as! String).isEmpty {  closure(0) }
            else {
                self.solve_challenge(document: document, closure: { (res) in
                    params["jschl_answer"] = 16  + res
                    DispatchQueue.main.asyncAfter(deadline: .now() + 6, execute: {
                        request.httpMethod = response.request!.httpMethod
                        session.request(request.url!, method: .get, parameters: params).responseString { response2 in
                            print(response.result.value!)
                            session.request("https://\(domain_name)/").responseString { response2 in
                                if let
                                    headerFields = response2.response?.allHeaderFields as? [String: String],
                                    var URL = response2.request?.url
                                {
                                    let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: URL)
                                    print(cookies)
                                }
                            }
                            //closure(1)
                        }
                    })
                })
            }
            
        })
    }
    private func getNum(web:UIWebView) -> Int {
       var res = web.stringByEvaluatingJavaScript(from: "bypassCloud()")!
        print(res)
        if (!res.isEmpty)
        {
            return Int(res)!
        }
        return -1
    }
    private func solve_challenge(document:Document, closure: @escaping (Int) -> ()) {
        var result:Int = -1
        let js:String = try! document.getElementsByTag("script").get(0).html().remove(text: ["\n"])
        let js_clean:String = "\((GetElement(input: js as NSString, paternn: "setTimeout(.*?)4000") as String)));"
        let jstoput:String = """
        <head>
                            <script>function bypassCloud() {
                                    var result = 0;
        \(GetElement(input: js_clean as NSString, paternn: "var s,t,o,p,b,r,e,a,k,i,n,g,f(.*?);"))
        \((GetElement(input: js_clean as NSString, paternn: "f =(.*?)4000") as String).remove(text: ["f.action += location.hash;", "f.submit();", "+ t.length", "}, 4000"]).replacingOccurrences(of: "a.value = ", with: "result = "))
                                    return result;
                            }</script>
        </head>
        <body>
        Marent Challenge Cloudflare ! 🚀
        </body>
"""
        //Create a fake webview for exec the JS and get the result to return
        let web:UIWebView = UIWebView()
        web.loadHTMLString(jstoput, baseURL: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            var res = web.stringByEvaluatingJavaScript(from: "bypassCloud()")!
            if (!res.isEmpty)
            {
                closure(Int(res)!)
            }else {
                closure(-1)
            }
        }
    }
}
