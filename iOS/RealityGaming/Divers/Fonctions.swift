//
// Created by Marentdev on 16/12/2017.
// Copyright (c) 2017 marentdev. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import Alamofire
import SideMenu
import SwiftMessages
import SwiftSoup
import AlamofireImage
import SwiftyJSON
import MessageKit
import Kingfisher
let debug:Bool = false
let domain_name:String = debug ? "xf2.realitygaming.tech" : "realitygaming.fr"
var session:SessionManager = SessionManager.default
var headers = SessionManager.defaultHTTPHeaders
let save = UserDefaults.standard
var _xfToken:String = ""
struct Membre {
    var pseudo:String?
    var id:String?
    var messages:String = ""
    var jaimes:String = ""
    var points:String = ""
    var style:String?
    var avatar:String?
    var title:String?
    var avimg:UIImage = UIImage()
}
var Profile:Membre = Membre()
let net = NetworkReachabilityManager()
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
extension String {
    func localized(bundle: Bundle = .main, tableName: String) -> String {
            return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
                data: data,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil) else { return nil }
        return html
    }
    func remove(text:[String]) -> String{
        var t:String = self
        for i in 0 ..< text.count {
            t = t.replacingOccurrences(of: text[i], with: "")
        }
        return t
    }
    func convertToDate() -> String {
        var t:String = self
        let month:[String] = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
        let m:[String] = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
        for i in 0 ..< month.count {
            t = t.replacingOccurrences(of: month[i], with: m[i])
        }
        return t.replacingOccurrences(of: " ", with: "/")
    }
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(rgb: Int) {
        self.init(
                red: (rgb >> 16) & 0xFF,
                green: (rgb >> 8) & 0xFF,
                blue: rgb & 0xFF
        )
    }
}

extension UIImage {
    public static func fontAwesomeIcon(name: String, textColor: UIColor, size: CGSize, backgroundColor: UIColor = UIColor.clear, borderWidth: CGFloat = 0, borderColor: UIColor = UIColor.clear) -> UIImage {
        var size = size
        if size.width <= 0 { size.width = 1 }
        if size.height <= 0 { size.height = 1 }
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.center
        
        let fontSize = min(size.width / 1.28571429, size.height)
        
        // stroke width expects a whole number percentage of the font size
        let strokeWidth: CGFloat = fontSize == 0 ? 0 : (-100 * borderWidth / fontSize)
        
        let attributedString = NSAttributedString(string: name, attributes: [
            NSAttributedStringKey.font: UIFont(name: "Font Awesome 5 Free", size: fontSize),
            NSAttributedStringKey.foregroundColor: textColor,
            NSAttributedStringKey.backgroundColor: backgroundColor,
            NSAttributedStringKey.paragraphStyle: paragraph,
            NSAttributedStringKey.strokeWidth: strokeWidth,
            NSAttributedStringKey.strokeColor: borderColor
            ])
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        attributedString.draw(in: CGRect(x: 0, y: (size.height - fontSize) / 2, width: size.width, height: fontSize))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

func getLinkAvatarFormPseudo(username:String, imageView:UIImageView) {
    let urlstr:String = username
    session.request(URL(string: "https://\(domain_name)/index.php/members/find?q=\(urlstr.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)&_xfToken=\(_xfToken)&_xfResponseType=json")!).validate().responseJSON { (dataresp) in
        let json = JSON(dataresp.result.value!)
        if (json["results"][0]["iconHtml"] != JSON.null)
        {
            let document:Document = try! SwiftSoup.parse(json["results"][0]["iconHtml"].string!)
            if (json["results"][0]["id"].string! == username) {
                let id:String = (try! document.getElementsByClass("avatar").get(0).getAttributes()?.get(key: "data-user-id"))!
                loadImageFromUrl(url: get_avatar_link(id: id), view: imageView)
            }
            else {
                imageView.image = #imageLiteral(resourceName: "noavatar")
            }
        }else {
            imageView.image = #imageLiteral(resourceName: "noavatar")
        }
    }
}

func Clean_string(text:String) -> String{
    let decodedString = text.htmlAttributedString()?.string
    return decodedString! as String
}
func matches(for regex: String!, in text: String!) -> [String] {

    do {
        let regex = try NSRegularExpression(pattern: regex, options: [])
        let nsString = text as NSString
        let results = regex.matches(in: text, range: NSMakeRange(0, nsString.length))
        return results.map { nsString.substring(with: $0.range)}
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}
func GetElement(input:NSString, paternn:NSString) -> NSString {
    let azerty:NSString = input as NSString
    let cowRegex = try! NSRegularExpression(pattern: paternn as String,
            options: .caseInsensitive)
    if let cowMatch = cowRegex.firstMatch(in: azerty as String, options: [],
            range: NSMakeRange(0, azerty.length)) {
        return (azerty as NSString).substring(with: cowMatch.range) as NSString
    }else{
        return ""
    }
}

func countLabelLines(label: UILabel) -> Int {
    let myText = label.text! as NSString
    let rect = CGSize(width: label.bounds.width, height: CGFloat.greatestFiniteMagnitude)
    let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: label.font], context: nil)
    return Int(ceil(CGFloat(labelSize.height) / label.font.lineHeight))
}
extension String
{
    func stringByReplacingFirstOccurrenceOfString(target: String, withString replaceString: String) -> String {
        if let range = self.range(of: target) {
            return self.replacingCharacters(in: range, with: replaceString)
        }
        return self
    }
}

func returnCSSRank() -> String {
    var str:String = ""
    var str2:String = ""
    let url = Bundle.main.url(forResource: "cssRank", withExtension: "css")
    do {
        str = try String(contentsOf: url!, encoding: String.Encoding.utf8)
    }catch {}
    let url2 = Bundle.main.url(forResource: "font-awesome", withExtension: "css")
    do {
        str2 = try String(contentsOf: url2!, encoding: String.Encoding.utf8)
    }catch {}
    return "<style>\(str)</style><style>\(str2)</style>"
}

let FA_rank:[String:String] = ["style3":"","style19":"\u{f005}","style39":"\u{f19d}","style6":"","style10":"\u{f0a0}","style80":"\u{f0d6}","style83":"\u{f099}","style95":"\u{f099}","style94":"\u{f099}","style66":"\u{f120}","style92":"\u{f06b}","style4":"\u{f0e3}","style84":"\u{f099}","style40":"\u{f16a}","style9":"","style55":"\u{f040}","style88":"\u{f017}","style38":"\u{f006}", "style85": "\u{f132}", "style100": "", "style99": "\u{f2bd}"]

func returnPseudo(pseudo:String, style:String = "style2") -> String {
    var Base:String = "<span class=\"\(style)\"><a style=\"font-family:'FontAwesome';\">famarent</a> \(pseudo)</span>"
    if FA_rank[style] != nil {
        Base = Base.replacingOccurrences(of: "famarent", with: FA_rank[style]!)
    } else {
        Base = Base.replacingOccurrences(of: "famarent", with: "")
    }
    return Base
}

func loadImageFromUrl(url: String, view: UIImageView, isbanner:Bool = false){
    view.kf.indicatorType = .activity
    if ImageCache.default.imageCachedType(forKey: url).cached {
        view.kf.setImage(with: URL(string: url)!, options: [.onlyFromCache])
    }else {
        session.request(URL(string: url)!)
            .validate()
            .downloadProgress(closure: { (progress) in
                print("Download Progress for \(url): \(progress.fractionCompleted)")
            })
            .responseImage { (dataimg) in
                if dataimg.response?.statusCode == 304 || dataimg.response?.statusCode == 200
                {
                    view.image = dataimg.result.value!
                    ImageCache.default.store(dataimg.result.value!, forKey: url)
                }
                else if isbanner {
                    view.image = nil
                    //ImageCache.default.store(#imageLiteral(resourceName: "noavatar"), forKey: url)
                }else {
                    view.image = #imageLiteral(resourceName: "noavatar")
                    ImageCache.default.store(#imageLiteral(resourceName: "noavatar"), forKey: url)
                }
            }
    }
}
func get_avatar_link(id:String) -> String
{
    if id.isEmpty { return "" }
    if id.count <= 3 {
        return "https://\(domain_name)/data/avatars/l/0/\(id).jpg"
    }
    else {
        return "https://\(domain_name)/data/avatars/l/\(id.prefix(id.count - 3))/\(id).jpg"
    }
}

func get_banner_link(id:String) -> String
{
    if id.isEmpty { return "" }
    if id.count <= 3 {
        return "https://\(domain_name)/data/foroagency/profilecustom/banners/o/0/\(id).jpg"
    }
    else {
        return "https://\(domain_name)/data/foroagency/profilecustom/banners/o/\(id.prefix(id.count - 3))/\(id).jpg"
    }
}
func getProfile(completion: @escaping () -> Void) {
    session.request(URL(string: "https://\(domain_name)/compte/visitor-menu")!).responseString { (response) in
        do {
            let html:Document = try SwiftSoup.parse(response.result.value ?? "")
            _xfToken = try html.getElementsByAttributeValue("name", "_xfToken").val()
            print(_xfToken)
            let id:String = try html.getElementsByClass("username").eq(0).attr("data-user-id")
            let username:String = try html.getElementsByClass("username").eq(0).text()
            let likes:String = try html.getElementsByAttributeValue("href", "/compte/likes").eq(0).text()
            let messages:String = try html.getElementsByAttributeValue("href", "/search/member?user_id=\(id)").eq(0).text()
            let points:String = try html.getElementsByAttributeValueMatching("href", "/members/(.*?).\(id)/trophies").eq(0).text()
            let styleid:String = try html.getElementsByAttributeValueMatching("class", "username--(.*?)").eq(0).attr("class").remove(text: ["username--", " username--staff", "username--admin", "username--moderator"])
            let title:String = try html.getElementsByClass("userTitle").eq(0).text()
            let avatar_link:String = get_avatar_link(id: id)
            Profile.id = id
            Profile.pseudo = username
            Profile.avatar = avatar_link
            Profile.jaimes = likes
            Profile.points = points
            Profile.style = styleid
            Profile.messages = messages
            Profile.title = title
        } catch Exception.Error(let type, let message) {
            print(type, message)
        } catch {
            print("error")
        }
        completion()
    }
}

func getProfileFromPseudo(username:String, closure: @escaping (Membre) -> ()) {
    session.request(URL(string: "https://\(domain_name)/index.php/members/find?q=\(username.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)&_xfToken=\(_xfToken)&_xfResponseType=json")!).validate().responseJSON { (dataresp) in
        let json = JSON(dataresp.result.value!)
        if (json["results"][0]["iconHtml"] != JSON.null)
        {
            let document:Document = try! SwiftSoup.parse(json["results"][0]["iconHtml"].string!)
            if (json["results"][0]["id"].string! == username) {
                let id:String = (try! document.getElementsByClass("avatar").get(0).getAttributes()?.get(key: "data-user-id"))!
               session.request(URL(string: "https://\(domain_name)/members/\(id)/?tooltip=true")!).responseString { (response) in
                    //get stats
                }
            }
            else {
                
            }
        }else {
            
        }
    }
}
func isConnected() -> Bool {
    if net?.isReachable ?? false {
        if (net?.isReachableOnEthernetOrWiFi) != nil {
            return true
        } else if (net?.isReachableOnWWAN)! {
            return true
        }
    } else {
        return false
    }
    return false
}
func stringFromHtml(string: String) -> NSAttributedString? {
    do {
        let data = string.data(using: String.Encoding.unicode, allowLossyConversion: true)
        if let d = data {
            let str = try! NSAttributedString(
                data: d,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            return str
        }
    } catch {
    }
    return nil
}

func getColorRank(rank:Int) -> UIColor {
    switch rank {
    case 4:
        return UIColor.black
    case 3:
        return UIColor(hex: 0xFF0000)
    case 80:
        return UIColor(hex: 0x1253b4)
    case 39:
        return UIColor(hex: 0x006d80)
    case 66:
        return UIColor(hex: 0xA70000)
    case 55:
        return UIColor(hex: 0xa101a6)
    case 19:
        return UIColor(hex: 0xFC7F3C)
    case 38:
        return UIColor(hex: 0xf9a600)
    case 9:
        return UIColor(hex: 0xf9a600)
    case 92:
        return UIColor(hex: 0xf9a600)
    case 99:
        return UIColor(hex: 0x4568CA)
    case 2:
        return UIColor(hex: 0x417394)
    case 10:
        return UIColor(hex: 0xB9121B)
    case 100:
        return UIColor(hex: 0x6AB04C)
    case 85:
        return UIColor(hex: 0x054667)
    default:
        return UIColor.black
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = " "
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension BinaryInteger {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

func parse_dir(path:String) -> UIViewController {
    var fragment = path.remove(text: ["https://\(domain_name)"]).components(separatedBy: "/")
    let this = UIStoryboard(name: "Main", bundle: nil)
    fragment.removeFirst(); fragment.removeLast()
    print(fragment)
    if fragment.count > 1 {
        switch fragment[0] {
        case "posts":
            topic_id = fragment[1].contains(".") ? fragment[1].components(separatedBy: ".")[1] : fragment[1]
            is_post = true
            let next:WebViewTopicViewController = this.instantiateViewController(withIdentifier: "topicgo") as! WebViewTopicViewController
            return next
        case "threads":
            topic_id = fragment[1].contains(".") ? fragment[1].components(separatedBy: ".")[1] : fragment[1]
            is_post = false
            let next:WebViewTopicViewController = this.instantiateViewController(withIdentifier: "topicgo") as! WebViewTopicViewController
            return next
        case "profile-posts":
            break
        case "members":
            user_id = fragment[1].contains(".") ? fragment[1].components(separatedBy: ".")[1] : fragment[1]
            let next:ProfileViewController = this.instantiateViewController(withIdentifier: "gotoprrofil") as! ProfileViewController
            return next
        default:
            break
        }
    }
    return UIViewController()
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y:
                                              (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y:
                                                         locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

func randomStringWithLength (len : Int) -> NSString {
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    var randomString : NSMutableString = NSMutableString(capacity: len)
    for var i in 0 ..< len {
        let length = UInt32 (letters.length)
        let rand = arc4random_uniform(length)
        randomString.appendFormat("%C", letters.character(at: Int(rand)))
        i += 1
    }
    return randomString
}
