//
//  SideMenuViewController.swift
//  RealityGaming
//
//  Created by Marentdev on 03/01/2018.
//  Copyright © 2018 marentdev. All rights reserved.
//

import UIKit
import SwiftSoup
import SideMenu
import SwifterSwift
var section_active:[Bool] = [true, false, false]
class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var paneluser: UIView!
    @IBOutlet weak var titleperso: UILabel!
    @IBOutlet weak var stats: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var tableview: UITableView!
    let title_sec:[String] = ["Shoutbox", "Forums", "Mon profil"]
    let title_fa:[String] = ["", "", ""]
    override func viewDidLoad() {
        super.viewDidLoad()
        paneluser.isHidden = true
        avatar.layer.masksToBounds = true
        avatar.layer.cornerRadius = avatar.layer.frame.height / 2
        getProfile {
            self.setupProfile()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func Logout(_ sender: UIButton) {
        sender.isEnabled = false
        session.request(URL(string: "https://\(domain_name)/logout")!, method: .get, parameters: ["t":_xfToken]).responseString { (response) in
            let document:Document = try! SwiftSoup.parse(response.result.value!)
            if try! document.getElementsByClass("p-title-value").eq(0).text().contains("RealityGaming")
            {
                save.set(nil, forKey: "pseudo")
                save.set(nil, forKey: "password")
                save.set(nil, forKey: "touchid")
                let next:StartPageViewController = self.storyboard?.instantiateViewController(withIdentifier: "homepage") as! StartPageViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = next
            }
            else {
                sender.isEnabled = true
                //Error
            }
        }
    }
    
    func setupProfile() {
        if Profile.jaimes == "" { return }
        let attrstats = try! NSAttributedString(
            data: "<center style=\"font-size:14px;\"><span style=\"color:white;font-family:Helvetica;\"><a style=\"font-family:'Font Awesome 5 Free';\">\u{f0e0}</a> \(Profile.messages)</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"color:white;font-family:Helvetica;\"><a style=\"font-family:'Font Awesome 5 Free';\">\u{f164}</a> \(Profile.jaimes)</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"color:white;font-family:Helvetica;\"><a style=\"font-family:'Font Awesome 5 Free';\">\u{f091}</a> \(Profile.points)</span></center>".data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        let attrpseudo = try! NSAttributedString(
            data: "\(returnCSSRank())<center style=\"font-size:14px;\">\(returnPseudo(pseudo: Profile.pseudo!, style: Profile.style!))</center>".data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        titleperso.text = Profile.title
        username.attributedText = attrpseudo
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            loadImageFromUrl(url: Profile.avatar!, view: self.avatar)
        }
        if avatar.image != nil
        {
            Profile.avimg = avatar.image!
        }
        stats.attributedText = attrstats
        paneluser.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.title_sec.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuTableViewCell
        let attrsec = try! NSAttributedString(
            data: "<span style=\"color:white;font-size:17px;font-family:'Helvetica';font-weigth:bold;\"><span style=\"font-size:20px;font-family:'Font Awesome 5 Free';\">\(title_fa[indexPath.row])</span>&nbsp;&nbsp;\(title_sec[indexPath.row])</span>".data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        if section_active[indexPath.row]
        {
            cell.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        }
        else {
            cell.backgroundColor = .clear
        }
        cell.title.attributedText = attrsec
        let view = UIView()
        view.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        cell.selectedBackgroundView = view
        return cell
    }
    @IBAction func ViewConv(_ sender: UIButton) {
        let next:ConversationsViewController = self.storyboard?.instantiateViewController(withIdentifier: "conversations") as! ConversationsViewController
        self.navigationController?.pushViewController(next, animated: true)
    }
    @IBAction func viewAlerts(_ sender: UIButton) {
        let next:AlertsViewController = self.storyboard?.instantiateViewController(withIdentifier: "gotoalertes") as! AlertsViewController
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 && !section_active[indexPath.row] {
            section_active = [true, false, false]
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            self.navigationController?.isNavigationBarHidden = true
            let next:ShoutboxStripeViewController = self.storyboard?.instantiateViewController(withIdentifier: "shoutbox") as! ShoutboxStripeViewController
            self.navigationController?.pushViewController(next, animated: true)
            self.tableview.reloadData()
        }
        else if indexPath.row == 1 && !section_active[indexPath.row] {
            section_active = [false, true, false]
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            self.navigationController?.isNavigationBarHidden = true
            let next:MainForumViewController = self.storyboard?.instantiateViewController(withIdentifier: "mainforum") as! MainForumViewController
            self.navigationController?.pushViewController(next, animated: true)
            self.tableview.reloadData()
        }else if indexPath.row == 2 && !section_active[indexPath.row] {
            section_active = [false, false, true]
            user_id = Profile.id!
            let next:ProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "gotoprrofil") as! ProfileViewController
            self.navigationController?.pushViewController(next, animated: true)
            self.tableview.reloadData()
        }
        else {
            dismiss(animated: true, completion: nil)
        }
    }

}
